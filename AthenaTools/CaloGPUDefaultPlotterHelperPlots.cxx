//
// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#include "CaloGPUDefaultPlotterHelper.h"

//NSF: Quick guide to expand the available plots: search for 'CUSTOMIZATION POINT!'

#include <type_traits>
#include <cstdint>
#include <variant>
#include <any>
#include <iterator>

#include "CLHEP/Units/SystemOfUnits.h"

#include "CaloRecGPU/Helpers.h"

#include <TH1D.h>
#include <TH2D.h>

#include "CxxUtils/checker_macros.h"

ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

//AT LEAST FOR THE TIME BEING!

using namespace CaloRecGPU;

namespace
{

  using hist_1D_type = TH1D;
  using hist_2D_type = TH2D;

  //DIM: `1` or `2`, the dimensions of the plot
  //FILLTYPE: `single` or `combined` [or `other`], the way to fill the plot
#define CALORECGPU_PLOTHISTTYPE(DIM, FILLTYPE)                                   \
  CaloPlotterHelper::plot_type<hist_ ## DIM ## D_type,                           \
  CaloPlotterHelper::plotter_ ## FILLTYPE,                                       \
  CaloPlotterHelper::plotter_H ## DIM ## D>                                      \


#define CALORECGPU_SINGLE_LAMBDA(HISTTYPE)                                       \
  ( [[maybe_unused]] HISTTYPE * plt,                                             \
    [[maybe_unused]] const ConstantDataHolder & cdh,                             \
    [[maybe_unused]] const CaloRecGPU::CellInfoArr & cell_info,                  \
    [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state,                \
    [[maybe_unused]] const CaloRecGPU::ClusterInfoArr & cluster_info,            \
    [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr & cluster_moments)      \


#define CALORECGPU_COMBINED_LAMBDA(HISTTYPE)                                     \
  ( [[maybe_unused]] HISTTYPE * plt,                                             \
    [[maybe_unused]] const ConstantDataHolder & cdh,                             \
    [[maybe_unused]] const CaloRecGPU::CellInfoArr & cell_info,                  \
    [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state_1,              \
    [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state_2,              \
    [[maybe_unused]] const CaloRecGPU::ClusterInfoArr & cluster_info_1,          \
    [[maybe_unused]] const CaloRecGPU::ClusterInfoArr & cluster_info_2,          \
    [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr & cluster_moments_1,    \
    [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr & cluster_moments_2,    \
    [[maybe_unused]] const CaloPlotterHelper::sample_comparisons_holder & sch)   \


  //Note: when C++20 is completely adopted,
  //      what follows might benefit from concepts
  //      to ensure we're truly working with
  //      properties the way we want...
  //      We could already use some static_assert
  //      and so on, but, since there's a more
  //      elegant solution in the making,
  //      and since this would only prevent
  //      misuse in future changes of the code,
  //      it's not that serious. (As we are
  //      currently taking proper care..."ish"...)

  //-------------------------------------

  template <class ... Args>
  struct multi_class_holder;

  template <class T>
  struct multi_class_holder<T>
  {
    using current = T;
    using next = void;
    template <class ... X>
    static constexpr auto concatenate(multi_class_holder<X...>)
    {
      return multi_class_holder<T, X...> {};
    }
  };

  template <class T, class ... Args>
  struct multi_class_holder<T, Args...>
  {
    using current = T;
    using next = multi_class_holder<Args...>;
    template <class ... X>
    static constexpr auto concatenate(multi_class_holder<X...>)
    {
      return multi_class_holder<T, Args..., X...> {};
    }
  };

  template <class T, template <class> class TT>
  using expansion_helper = TT<T>;

  template <class T>
  using storage_type_helper = typename T::storage_t;


  template <class ... Args>
  static constexpr auto get_holder_storage_types(multi_class_holder<Args ...>)
  {
    return multi_class_holder< expansion_helper<Args, storage_type_helper>... > {};
  }

  template <class Holder>
  using holder_storage_types = decltype(get_holder_storage_types(Holder{}));

  template <class ... Args>
  static constexpr auto get_variant_from_holder(multi_class_holder<Args...>)
  {
    return std::variant<Args...> {};
  }

  template <class Holder>
  using variant_from_holder = decltype(get_variant_from_holder(Holder{}));

  //-------------------------------------

  static float float_unhack(const unsigned int bits)
  {
    float res;
    std::memcpy(&res, &bits, sizeof(float));
    //In C++20, we should bit-cast. For now, for our platform, works.
    return res;
  }

  //To deal with potential lack of maybe_unused in pack expansions.
  template <class Arg>
  static constexpr decltype(auto) suppress_warning(Arg && a)
  {
    return std::forward<Arg>(a);
  }

  template <class ... Args>
  static constexpr decltype(auto) get_last(Args && ... args)
  {
    return (suppress_warning(std::forward<Args>(args)), ...);
  }

  //-------------------------------------

  namespace ClusterProperties
  {
    //VA_ARGS should evaulate to a `double` corresponding to the relevant cluster property.
    //Available variables within it: cell_state, cluster_info, cluster_index
#define CALORECGPU_BASIC_CLUSTER_PROPERTIES(FILENAME, PROPERNAME, AXISNAME, UNIT, CUMULATIVE, ...)  \
  struct clusters_ ## FILENAME                                                          \
  {                                                                                     \
    static std::string filename()                                                       \
    {                                                                                   \
      return #FILENAME;                                                                 \
    }                                                                                   \
    static std::string propername()                                                     \
    {                                                                                   \
      return PROPERNAME;                                                                \
    }                                                                                   \
    static std::string axisname()                                                       \
    {                                                                                   \
      return AXISNAME;                                                                  \
    }                                                                                   \
    static std::string unit()                                                           \
    {                                                                                   \
      return UNIT;                                                                      \
    }                                                                                   \
    struct storage_t {};                                                                \
    static auto build_storage(const ConstantDataHolder &,                               \
                              const CaloRecGPU::CellInfoArr &,                          \
                              const CaloRecGPU::CellStateArr &,                         \
                              const CaloRecGPU::ClusterInfoArr &,                       \
                              const CaloRecGPU::ClusterMomentsArr &)                    \
    {                                                                                   \
      return storage_t{};                                                               \
    }                                                                                   \
    static double get_property(const storage_t &,                                       \
                               [[maybe_unused]] const CellStateArr & cell_state,        \
                               [[maybe_unused]] const ClusterInfoArr & cluster_info,    \
                               [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr & cluster_moments, \
                               [[maybe_unused]] const int cluster_index)                \
    {                                                                                   \
      return __VA_ARGS__;                                                               \
    }                                                                                   \
    static constexpr bool do_cumulative()                                               \
    {                                                                                   \
      return CUMULATIVE;                                                                \
    }                                                                                   \
    static constexpr bool is_combined()                                                 \
    {                                                                                   \
      return false;                                                                     \
    }                                                                                   \
  };                                                                                    \


    //VA_ARGS should evaulate to a `double` corresponding to the relevant cluster property.
    //Available variables within it: cell_state_1, cluster_info_1, cluster_index_1,
    //                               cell_state_2, cluster_info_2, cluster_index_2
#define CALORECGPU_CLUSTER_COMPARED_PROPERTIES(FILENAME, PROPERNAME, AXISNAME, UNIT, CUMULATIVE, ...)  \
  struct clusters_ ## FILENAME                                                          \
  {                                                                                     \
    static std::string filename()                                                       \
    {                                                                                   \
      return #FILENAME;                                                                 \
    }                                                                                   \
    static std::string propername()                                                     \
    {                                                                                   \
      return PROPERNAME;                                                                \
    }                                                                                   \
    static std::string axisname()                                                       \
    {                                                                                   \
      return AXISNAME;                                                                  \
    }                                                                                   \
    static std::string unit()                                                           \
    {                                                                                   \
      return UNIT;                                                                      \
    }                                                                                   \
    struct storage_t {};                                                                \
    static auto build_storage(const ConstantDataHolder &,                               \
                              const CaloRecGPU::CellInfoArr &,                          \
                              const CaloRecGPU::CellStateArr &,                         \
                              const CaloRecGPU::CellStateArr &,                         \
                              const CaloRecGPU::ClusterInfoArr &,                       \
                              const CaloRecGPU::ClusterInfoArr &,                       \
                              const CaloRecGPU::ClusterMomentsArr &,                    \
                              const CaloRecGPU::ClusterMomentsArr &,                    \
                              const CaloPlotterHelper::sample_comparisons_holder &)     \
    {                                                                                   \
      return storage_t{};                                                               \
    }                                                                                   \
    static double get_property(const storage_t &,                                                        \
                               [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state_1,           \
                               [[maybe_unused]] const CaloRecGPU::ClusterInfoArr & cluster_info_1,       \
                               [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr & cluster_moments_1, \
                               [[maybe_unused]] const int cluster_index_1,                               \
                               [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state_2,           \
                               [[maybe_unused]] const CaloRecGPU::ClusterInfoArr & cluster_info_2,       \
                               [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr & cluster_moments_2, \
                               [[maybe_unused]] const int cluster_index_2)                               \
    {                                                                                   \
      return __VA_ARGS__;                                                               \
    }                                                                                   \
    static constexpr bool do_cumulative()                                               \
    {                                                                                   \
      return CUMULATIVE;                                                                \
    }                                                                                   \
    static constexpr bool is_combined()                                                 \
    {                                                                                   \
      return true;                                                                      \
    }                                                                                   \
  };                                                                                    \

    CALORECGPU_BASIC_CLUSTER_PROPERTIES(E, "Energy", "E", "[GeV]", false,
                                        cluster_info.clusterEnergy[cluster_index] / CLHEP::GeV)

    CALORECGPU_BASIC_CLUSTER_PROPERTIES(abs_E, "Absolute Energy", "#||{E}", "[GeV]", true,
                                        std::abs(cluster_info.clusterEnergy[cluster_index]) / CLHEP::GeV)

    CALORECGPU_BASIC_CLUSTER_PROPERTIES(Et, "Transverse Energy", "E_{T}", "[GeV]", false,
                                        cluster_info.clusterEt[cluster_index] / CLHEP::GeV)

    CALORECGPU_BASIC_CLUSTER_PROPERTIES(eta, "Pseudo-Rapidity", "#eta_{Cluster}", "", false,
                                        cluster_info.clusterEta[cluster_index])

    CALORECGPU_BASIC_CLUSTER_PROPERTIES(phi, "Azimuthal Angle", "#phi_{Cluster}", "", false,
                                        cluster_info.clusterPhi[cluster_index])


    static double regularize_angle (const double b, const double a = 0.)
    //a. k. a. proxim in Athena code.
    {
      const float diff = b - a;
      const float divi = (fabs(diff) - Helpers::Constants::pi<double>) / (2 * Helpers::Constants::pi<float>);
      return b - ceilf(divi) * ((b > a + Helpers::Constants::pi<double>) - (b < a - Helpers::Constants::pi<float>)) * 2 * Helpers::Constants::pi<float>;
    }

    //Yes, these two are immediately invoked lambdas.
    //Less verbose than copy-pasting the entire classes...

    //This is the wrapped-around version of delta_phi...
    CALORECGPU_CLUSTER_COMPARED_PROPERTIES(delta_phi_in_range, "#Delta #phi", "#Delta #phi", "", false,
                                           regularize_angle( regularize_angle(cluster_info_2.clusterPhi[cluster_index_2]) -
                                                             regularize_angle(cluster_info_1.clusterPhi[cluster_index_1])    )
                                          )

    CALORECGPU_CLUSTER_COMPARED_PROPERTIES(delta_R, "Distance", "#Delta R", "", true,
                                           [&]() -> double
    {
      const double delta_eta = cluster_info_2.clusterEta[cluster_index_2] - cluster_info_1.clusterEta[cluster_index_1];
      const double delta_phi = regularize_angle( regularize_angle(cluster_info_2.clusterPhi[cluster_index_2]) -
                                                 regularize_angle(cluster_info_1.clusterPhi[cluster_index_1])    );
      return std::sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
    } ()
                                          )



    struct clusters_size
    {
      static std::string filename()
      {
        return "size";
      }
      static std::string propername()
      {
        return "Cluster Size";
      }
      static std::string axisname()
      {
        return "#(){# Cluster Cells}";
      }
      static std::string unit()
      {
        return "";
      }
      using storage_t = std::vector<double>;
      static auto build_storage(const ConstantDataHolder &,
                                const CaloRecGPU::CellInfoArr & cell_info,
                                const CaloRecGPU::CellStateArr & cell_state,
                                const CaloRecGPU::ClusterInfoArr & cluster_info,
                                const CaloRecGPU::ClusterMomentsArr & /*cluster_moments*/)
      {
        std::vector<double> ret(cluster_info.number, 0.);
        for (int i = 0; i < NCaloCells; ++i)
          {
            if (!cell_info.is_valid(i))
              {
                continue;
              }
            const ClusterTag tag = cell_state.clusterTag[i];
            int c1 = -1, c2 = -1;
            if (tag.is_part_of_cluster())
              {
                c1 = tag.cluster_index();
                c2 = tag.is_shared_between_clusters() ? tag.secondary_cluster_index() : -1;
              }
            const float weight = 1.0f;
            if (c1 >= 0)
              {
                ret[c1] += weight;
              }
            if (c2 >= 0)
              {
                ret[c2] += weight;
              }
          }
        return ret;
      }
      static double get_property(const storage_t & store,
                                 const CaloRecGPU::CellStateArr &,
                                 const CaloRecGPU::ClusterInfoArr & /*cluster_info*/,
                                 const CaloRecGPU::ClusterMomentsArr & /*cluster_moments*/,
                                 const int cluster_index)
      {
        return store[cluster_index];
      }
      static constexpr bool do_cumulative()
      {
        return true;
      }
      static constexpr bool is_combined()
      {
        return false;
      }
    };

    struct clusters_weighted_size
    {
      static std::string filename()
      {
        return "weighted_size";
      }
      static std::string propername()
      {
        return "Cluster Size";
      }
      static std::string axisname()
      {
        return "#(){Weight of Cluster Cells}";
      }
      static std::string unit()
      {
        return "";
      }
      using storage_t = std::vector<double>;
      static auto build_storage(const ConstantDataHolder &,
                                const CaloRecGPU::CellInfoArr & cell_info,
                                const CaloRecGPU::CellStateArr & cell_state,
                                const CaloRecGPU::ClusterInfoArr & cluster_info,
                                const CaloRecGPU::ClusterMomentsArr & /*cluster_moments*/)
      {
        std::vector<double> ret(cluster_info.number, 0.);
        for (int i = 0; i < NCaloCells; ++i)
          {
            if (!cell_info.is_valid(i))
              {
                continue;
              }
            const ClusterTag tag = cell_state.clusterTag[i];
            int c1 = -1, c2 = -1;
            if (tag.is_part_of_cluster())
              {
                c1 = tag.cluster_index();
                c2 = tag.is_shared_between_clusters() ? tag.secondary_cluster_index() : -1;
              }
            const float rev_weight = float_unhack(tag.secondary_cluster_weight());
            const float weight = 1.0f - rev_weight;

            if (c1 >= 0)
              {
                ret[c1] += weight;
              }
            if (c2 >= 0)
              {
                ret[c2] += rev_weight;
              }
          }
        return ret;
      }
      static double get_property(const storage_t & store,
                                 const CaloRecGPU::CellStateArr &,
                                 const CaloRecGPU::ClusterInfoArr &,
                                 const CaloRecGPU::ClusterMomentsArr &,
                                 const int cluster_index)
      {
        return store[cluster_index];
      }
      static constexpr bool do_cumulative()
      {
        return true;
      }
      static constexpr bool is_combined()
      {
        return false;
      }
    };

    struct clusters_diff_cells
    {
      static std::string filename()
      {
        return "diff_cells";
      }
      static std::string propername()
      {
        return "Differently Assigned Cells per Pair";
      }
      static std::string axisname()
      {
        return "#(){# of Differently Assigned Cells}";
      }
      static std::string unit()
      {
        return "";
      }

      using storage_t = std::vector<double>;

      static auto build_storage(const ConstantDataHolder &,
                                const CaloRecGPU::CellInfoArr & cell_info,
                                const CaloRecGPU::CellStateArr & cell_state_1,
                                const CaloRecGPU::CellStateArr & cell_state_2,
                                const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                                const CaloRecGPU::ClusterInfoArr &,
                                const CaloRecGPU::ClusterMomentsArr &,
                                const CaloRecGPU::ClusterMomentsArr &,
                                const CaloPlotterHelper::sample_comparisons_holder & sch)
      {
        std::vector<double> ret(cluster_info_1.number, 0.);
        for (int i = 0; i < NCaloCells; ++i)
          {
            if (!cell_info.is_valid(i))
              {
                continue;
              }
            const ClusterTag ref_tag = cell_state_1.clusterTag[i];
            const ClusterTag test_tag = cell_state_2.clusterTag[i];
            int ref_c1 = -1, ref_c2 = -1, test_c1 = -1, test_c2 = -1;
            if (ref_tag.is_part_of_cluster())
              {
                ref_c1 = ref_tag.cluster_index();
                ref_c2 = ref_tag.is_shared_between_clusters() ? ref_tag.secondary_cluster_index() : -1;
              }

            if (test_tag.is_part_of_cluster())
              {
                test_c1 = test_tag.cluster_index();
                test_c2 = test_tag.is_shared_between_clusters() ? test_tag.secondary_cluster_index() : -1;
              }

            if (ref_c1 >= 0 && sch.r2t(ref_c1) != test_c1 && sch.r2t(ref_c1) != test_c2)
              {
                ret[ref_c1] += 1;
              }
            if (ref_c2 >= 0 && sch.r2t(ref_c2) != test_c2 && sch.r2t(ref_c2) != test_c1)
              {
                ret[ref_c2] += 1;
              }

            if (test_c1 >= 0 && sch.t2r(test_c1) != ref_c1 && sch.t2r(test_c1) != ref_c2)
              {
                if (sch.t2r(test_c1) >= 0)
                  {
                    ret[sch.t2r(test_c1)] += 1;
                  }
              }
            if (test_c2 >= 0 && sch.t2r(test_c2) != ref_c2 && sch.t2r(test_c2) != ref_c1)
              {
                if (sch.t2r(test_c2) >= 0)
                  {
                    ret[sch.t2r(test_c2)] += 1;
                  }
              }
          }
        return ret;
      }

      static double get_property(const storage_t & store,
                                 const CaloRecGPU::CellStateArr &,
                                 const CaloRecGPU::ClusterInfoArr & /*cluster_info_1*/,
                                 const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_1*/,
                                 const int cluster_index_1,
                                 const CaloRecGPU::CellStateArr &,
                                 const CaloRecGPU::ClusterInfoArr & /*cluster_info_2*/,
                                 const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_2*/,
                                 const int /*cluster_index_2*/)
      {
        return store[cluster_index_1];
      }

      static constexpr bool do_cumulative()
      {
        return true;
      }

      static constexpr bool is_combined()
      {
        return true;
      }
    };

    struct clusters_diff_cells_weighted
    {
      static std::string filename()
      {
        return "diff_cells_weighted";
      }
      static std::string propername()
      {
        return "Differently Assigned Weighted Cells per Pair";
      }
      static std::string axisname()
      {
        return "#(){Weight of Differently Assigned Cells}";
      }
      static std::string unit()
      {
        return "";
      }

      using storage_t = std::vector<double>;

      static auto build_storage(const ConstantDataHolder &,
                                const CaloRecGPU::CellInfoArr & cell_info,
                                const CaloRecGPU::CellStateArr & cell_state_1,
                                const CaloRecGPU::CellStateArr & cell_state_2,
                                const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                                const CaloRecGPU::ClusterInfoArr &,
                                const CaloRecGPU::ClusterMomentsArr &,
                                const CaloRecGPU::ClusterMomentsArr &,
                                const CaloPlotterHelper::sample_comparisons_holder & sch)
      {
        std::vector<double> ret(cluster_info_1.number, 0.);
        for (int i = 0; i < NCaloCells; ++i)
          {
            if (!cell_info.is_valid(i))
              {
                continue;
              }
            const ClusterTag ref_tag = cell_state_1.clusterTag[i];
            const ClusterTag test_tag = cell_state_2.clusterTag[i];
            int ref_c1 = -1, ref_c2 = -1, test_c1 = -1, test_c2 = -1;
            if (ref_tag.is_part_of_cluster())
              {
                ref_c1 = ref_tag.cluster_index();
                ref_c2 = ref_tag.is_shared_between_clusters() ? ref_tag.secondary_cluster_index() : -1;
              }

            if (test_tag.is_part_of_cluster())
              {
                test_c1 = test_tag.cluster_index();
                test_c2 = test_tag.is_shared_between_clusters() ? test_tag.secondary_cluster_index() : -1;
              }

            const float ref_rev_weight = float_unhack(ref_tag.secondary_cluster_weight());
            const float test_rev_weight = float_unhack(test_tag.secondary_cluster_weight());

            const float ref_weight = 1.0f - ref_rev_weight;
            const float test_weight = 1.0f - test_rev_weight;

            if (ref_c1 >= 0 && sch.r2t(ref_c1) != test_c1 && sch.r2t(ref_c1) != test_c2)
              {
                ret[ref_c1] += ref_weight;
              }
            if (ref_c2 >= 0 && sch.r2t(ref_c2) != test_c2 && sch.r2t(ref_c2) != test_c1)
              {
                ret[ref_c2] += ref_rev_weight;
              }

            if (test_c1 >= 0 && sch.t2r(test_c1) != ref_c1 && sch.t2r(test_c1) != ref_c2)
              {
                if (sch.t2r(test_c1) >= 0)
                  {
                    ret[sch.t2r(test_c1)] += test_weight;
                  }
              }
            if (test_c2 >= 0 && sch.t2r(test_c2) != ref_c2 && sch.t2r(test_c2) != ref_c1)
              {
                if (sch.t2r(test_c2) >= 0)
                  {
                    ret[sch.t2r(test_c2)] += test_rev_weight;
                  }
              }
          }
        return ret;
      }

      static double get_property(const storage_t & store,
                                 const CaloRecGPU::CellStateArr &,
                                 const CaloRecGPU::ClusterInfoArr & /*cluster_info_1*/,
                                 const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_1*/,
                                 const int cluster_index_1,
                                 const CaloRecGPU::CellStateArr &,
                                 const CaloRecGPU::ClusterInfoArr & /*cluster_info_2*/,
                                 const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_2*/,
                                 const int /*cluster_index_2*/)
      {
        return store[cluster_index_1];
      }

      static constexpr bool do_cumulative()
      {
        return true;
      }

      static constexpr bool is_combined()
      {
        return true;
      }
    };

    //Now, the moments.
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_time, "Time", "Time", "", false,
                                        cluster_moments.time[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_firstPhi, "First #phi", "First #phi", "", false,
                                        cluster_moments.firstPhi[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_firstEta, "First #eta", "First #eta", "", false,
                                        cluster_moments.firstEta[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_secondR, "Second R", "Second R", "", false,
                                        cluster_moments.secondR[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_secondLambda, "Second #lambda", "Second #lambda", "", false,
                                        cluster_moments.secondLambda[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_deltaPhi, "#Delta #Phi", "#Delta #Phi", "", false,
                                        cluster_moments.deltaPhi[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_deltaTheta, "#Delta #Theta", "#Delta #Theta", "", false,
                                        cluster_moments.deltaTheta[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_deltaAlpha, "#Delta #alpha", "Delta #alpha", "", false,
                                        cluster_moments.deltaAlpha[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_centerX, "Center X", "Center X", "", false,
                                        cluster_moments.centerX[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_centerY, "Center Y", "Center Y", "", false,
                                        cluster_moments.centerY[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_centerZ, "Center Z", "Center Z", "", false,
                                        cluster_moments.centerZ[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_centerMag, "Center Magnitude", "Center Magnitude", "", false,
                                        cluster_moments.centerMag[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_centerLambda, "Center #lambda", "Center #lambda", "", false,
                                        cluster_moments.centerLambda[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_lateral, "Lateral", "Lateral", "", false,
                                        cluster_moments.lateral[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_longitudinal, "Longitudinal", "Longitudinal", "", false,
                                        cluster_moments.longitudinal[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engFracEM, "Fraction of EM Energy", "Fraction of EM Energy", "", false,
                                        cluster_moments.engFracEM[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engFracMax, "Fraction of Energy in the Maximum", "Fraction of Energy in the Maximum", "", false,
                                        cluster_moments.engFracMax[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engFracCore, "Fraction of Energy in the Core", "Fraction of Energy in the Core", "", false,
                                        cluster_moments.engFracCore[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_firstEngDens, "First Energy Density", "First Energy Density", "", false,
                                        cluster_moments.firstEngDens[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_secondEngDens, "Second Energy Density", "Second Energy Density", "", false,
                                        cluster_moments.secondEngDens[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_isolation, "Isolation", "Isolation", "", false,
                                        cluster_moments.isolation[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engBadCells, "Bad Cell Energy", "Bad Cell Energy", "", false,
                                        cluster_moments.engBadCells[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_nBadCells, "Number of Bad Cells", "# of Bad Cells", "", false,
                                        cluster_moments.nBadCells[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_badCellsCorrE, "Corrected Bad Cell Energy", "Corrected Bad Cell Energy", "", false,
                                        cluster_moments.badCellsCorrE[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_nBadCellsCorr, "Corrected Number of Bad Cells", "Corrected # of Bad Cells", "", false,
                                        cluster_moments.nBadCellsCorr[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_badLArQFrac, "Fraction of Bad LArQ", "Bad LArQ Fraction", "", false,
                                        cluster_moments.badLArQFrac[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engPos, "Positive Energy", "Positive Energy", "", false,
                                        cluster_moments.engPos[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_significance, "Significance", "Significance", "", false,
                                        cluster_moments.significance[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_cellSignificance, "Cell With Max Significance", "Cell With Max Significance", "", false,
                                        cluster_moments.cellSignificance[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_cellSigSampling, "Sampling of Cell With Max Significance", "Max Significance Sampling", "", false,
                                        cluster_moments.cellSigSampling[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_avgLArQ, "Average LArQ", "Average LArQ", "", false,
                                        cluster_moments.avgLArQ[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_avgTileQ, "Average TileQ", "Average TileQ", "", false,
                                        cluster_moments.avgTileQ[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engBadHVCells, "Energy of Bad HV Cells", "Energy of Bad HV Cells", "", false,
                                        cluster_moments.engBadHVCells[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_nBadHVCells, "Number of Bad HV Cells", "# of Bad HV Cells", "", false,
                                        cluster_moments.nBadHVCells[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_PTD, "PTD", "PTD", "", false,
                                        cluster_moments.PTD[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_mass, "Mass", "Mass", "", false,
                                        cluster_moments.mass[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_EMProbability, "EM Probability", "EM Probability", "", false,
                                        cluster_moments.EMProbability[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_hadWeight, "Had. Weight", "Had. Weight", "", false,
                                        cluster_moments.hadWeight[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_OOCweight, "OOC Weight", "OOC Weight", "", false,
                                        cluster_moments.OOCweight[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_DMweight, "DM Weight", "DM Weight", "", false,
                                        cluster_moments.DMweight[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_tileConfidenceLevel, "Tile Confidence Level", "Tile Confidence Level", "", false,
                                        cluster_moments.tileConfidenceLevel[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_secondTime, "Second Time", "Second Time", "", false,
                                        cluster_moments.firstPhi[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_numCells, "Number of Cells", "Number of Cells", "", false,
                                        cluster_moments.numCells[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_vertexFraction, "Vertex Fraction", "Vertex Fraction", "", false,
                                        cluster_moments.vertexFraction[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_nVertexFraction, "N. Vertex Fraction", "N. Vertex Fraction", "", false,
                                        cluster_moments.nVertexFraction[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_etaCaloFrame, "#eta Calo Frame", "#eta Calo Frame", "", false,
                                        cluster_moments.etaCaloFrame[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_phiCaloFrame, "#phi Calo Frame", "#phi Calo Frame", "", false,
                                        cluster_moments.phiCaloFrame[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_eta1CaloFrame, "#eta 1 Calo Frame", "#eta 1 Calo Frame", "", false,
                                        cluster_moments.eta1CaloFrame[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_phi1CaloFrame, "#phi 1 Calo Frame", "#phi 1 Calo Frame", "", false,
                                        cluster_moments.phi1CaloFrame[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_eta2CaloFrame, "#eta 2 Calo Frame", "#eta 2 Calo Frame", "", false,
                                        cluster_moments.eta2CaloFrame[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_phi2CaloFrame, "#phi 2 Calo Frame", "#phi 2 Calo Frame", "", false,
                                        cluster_moments.phi2CaloFrame[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibTot, "Total Calib. Energy", "Total Calib. Energy", "", false,
                                        cluster_moments.engCalibTot[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibOutL, "Out Calib. Energy L", "Out Calib. Energy L", "", false,
                                        cluster_moments.engCalibOutL[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibOutM, "Out Calib. Energy M", "Out Calib. Energy M", "", false,
                                        cluster_moments.engCalibOutM[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibOutT, "Out Calib. Energy T", "Out Calib. Energy T", "", false,
                                        cluster_moments.engCalibOutT[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadL, "Dead Calib. Energy L", "Dead Calib. Energy L", "", false,
                                        cluster_moments.engCalibDeadL[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadM, "Dead Calib. Energy M", "Dead Calib. Energy M", "", false,
                                        cluster_moments.engCalibDeadM[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadT, "Dead Calib. Energy T", "Dead Calib. Energy T", "", false,
                                        cluster_moments.engCalibDeadT[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibEMB0, "EMB 0 Calib. Energy", "EMB 0 Calib. Energy", "", false,
                                        cluster_moments.engCalibEMB0[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibEME0, "EME 0 Calib. Energy", "EME 0 Calib. Energy", "", false,
                                        cluster_moments.engCalibEME0[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibTileG3, "Calib. Energy Tile G3", "Calib. Energy Tile G3", "", false,
                                        cluster_moments.engCalibTileG3[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadTot, "Dead Calib. Energy Total", "Dead Calib. Energy Total", "", false,
                                        cluster_moments.engCalibDeadTot[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadEMB0, "Dead Calib. Energy EMB0", "Dead Calib. Energy EMB0", "", false,
                                        cluster_moments.engCalibDeadEMB0[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadTile0, "Dead Calib. Energy Tile 0", "Dead Calib. Energy Tile 0", "", false,
                                        cluster_moments.engCalibDeadTile0[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadTileG3, "Dead Calib. Energy Tile G3", "Dead Calib. Energy Tile G3", "", false,
                                        cluster_moments.engCalibDeadTileG3[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadEME0, "Dead Calib. Energy EME 0", "Dead Calib. Energy EME 0", "", false,
                                        cluster_moments.engCalibDeadEME0[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadHEC0, "Dead Calib. Energy HEC 0", "Dead Calib. Energy HEC 0", "", false,
                                        cluster_moments.engCalibDeadHEC0[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadFCAL, "Dead Calib. Energy FCAL", "Dead Calib. Energy FCAL", "", false,
                                        cluster_moments.engCalibDeadFCAL[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadLeakage, "Dead Calib. Energy Leakage", "Dead Calib. Energy Leakage", "", false,
                                        cluster_moments.engCalibDeadLeakage[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibDeadUnclass, "Dead Calib. Energy Unclassified", "Dead Calib. Energy Unclassified", "", false,
                                        cluster_moments.engCalibDeadUnclass[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibFracEM, "Fraction of EM Calib. Energy", "Fraction of EM Calib. Energy", "", false,
                                        cluster_moments.engCalibFracEM[cluster_index])
    
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibFracHad, "Fraction of Hadronic Calib. Energy", "Fraction of Hadronic Calib. Energy", "", false,
                                        cluster_moments.engCalibFracHad[cluster_index])
                                        
    CALORECGPU_BASIC_CLUSTER_PROPERTIES(moments_engCalibFracRest, "Fraction of Rest Calib. Energy", "Fraction of Rest Calib. Energy", "", false,
                                        cluster_moments.engCalibFracRest[cluster_index])

  }

  //-------------------------------------

  namespace CellTypes
  {

    struct cell_type_global
    {
      static std::string filename()
      {
        return "global";
      }
      static std::string propername()
      {
        return "Global";
      }
      static std::string axisname()
      {
        return "";
      }

      struct storage_t {};

      template <class ... Args>
      static storage_t build_storage(Args && ...)
      {
        return {};
      }

      template <class ... Args>
      static bool filter(const storage_t &,
                         const CaloRecGPU::CellInfoArr & cell_info,
                         Args && ... args)
      {
        const int cell = get_last(std::forward<Args>(args)...);
        return cell_info.is_valid(cell);
      }
      static constexpr bool can_be_single()
      {
        return true;
      }
      static constexpr bool can_be_combined()
      {
        return true;
      }
    };



    //VA_ARGS should evaulate to a `bool` corresponding to whether the cell is part of this type or not.
    //Available variables within it: cell_info, cell_state, cell
#define CALORECGPU_CELL_TYPE_SINGLE(FILENAME, PROPERNAME, AXISNAME, ...)                \
  struct cell_type_ ## FILENAME                                                         \
  {                                                                                     \
    static std::string filename()                                                       \
    {                                                                                   \
      return #FILENAME;                                                                 \
    }                                                                                   \
    static std::string propername()                                                     \
    {                                                                                   \
      return PROPERNAME;                                                                \
    }                                                                                   \
    static std::string axisname()                                                       \
    {                                                                                   \
      return AXISNAME;                                                                  \
    }                                                                                   \
    struct storage_t {};                                                                \
    static auto build_storage(const ConstantDataHolder &,                               \
                              const CaloRecGPU::CellInfoArr &,                          \
                              const CaloRecGPU::CellStateArr &,                         \
                              const CaloRecGPU::ClusterInfoArr &,                       \
                              const CaloRecGPU::ClusterMomentsArr &)                    \
    {                                                                                   \
      return storage_t{};                                                               \
    }                                                                                   \
    static bool filter(const storage_t &,                                               \
                       [[maybe_unused]] const CaloRecGPU::CellInfoArr & cell_info,      \
                       [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state,    \
                       [[maybe_unused]] const int cell)                                 \
    {                                                                                   \
      return __VA_ARGS__;                                                               \
    }                                                                                   \
    static constexpr bool can_be_single()                                               \
    {                                                                                   \
      return true;                                                                      \
    }                                                                                   \
    static constexpr bool can_be_combined()                                             \
    {                                                                                   \
      return false;                                                                     \
    }                                                                                   \
  };                                                                                    \

    //Cells that belong to a cluster
    CALORECGPU_CELL_TYPE_SINGLE(intracluster, "Intra-Cluster", "Intra-Cluster",
                                ClusterTag{cell_state.clusterTag[cell]}.is_part_of_cluster());

    //Cells that don't belong to a cluster.
    CALORECGPU_CELL_TYPE_SINGLE(extracluster, "Extra-Cluster", "Extra-Cluster",
                                !ClusterTag{cell_state.clusterTag[cell]}.is_part_of_cluster());

    //Cells that are shared between clusters.
    CALORECGPU_CELL_TYPE_SINGLE(shared, "Shared", "Shared",
                                ClusterTag{cell_state.clusterTag[cell]}.is_part_of_cluster() &&
                                ClusterTag{cell_state.clusterTag[cell]}.is_shared_between_clusters());


    //VA_ARGS should correspond to a:
    //```static <type> calculate_property(const ConstantDataHolder & cdh,
    //                                    const CaloRecGPU::CellInfoArr & cell_info,
    //                                    const int cell)
    //That returns the relevant property (or combination thereof).
#define CALORECGPU_CELL_TYPE_SAME_GLOBAL(FILENAME, PROPERNAME, AXISNAME, ...)           \
  struct cell_type_ ## FILENAME                                                         \
  {                                                                                     \
    static std::string filename()                                                       \
    {                                                                                   \
      return #FILENAME;                                                                 \
    }                                                                                   \
    static std::string propername()                                                     \
    {                                                                                   \
      return PROPERNAME;                                                                \
    }                                                                                   \
    static std::string axisname()                                                       \
    {                                                                                   \
      return AXISNAME;                                                                  \
    }                                                                                   \
private:                                                                                \
    __VA_ARGS__                                                                         \
public:                                                                                 \
    struct storage_t                                                                    \
    {                                                                                   \
      const ConstantDataHolder * cdh_ptr = nullptr;                                     \
      std::map<decltype(calculate_property(std::declval<ConstantDataHolder>(),          \
                                           std::declval<CaloRecGPU::CellInfoArr>(),     \
                                           std::declval<int>())), int> map;             \
    };                                                                                  \
    template <class ... Args>                                                           \
    static storage_t build_storage(const ConstantDataHolder & cdh,                      \
                                   const CaloRecGPU::CellInfoArr & cell_info,           \
                                   Args && ...)                                         \
    {                                                                                   \
      storage_t ret;                                                                    \
      ret.cdh_ptr = &cdh;                                                               \
      for (int i = 0; i < NCaloCells; ++i)                                              \
        {                                                                               \
          if (!cell_info.is_valid(i))                                                   \
            {                                                                           \
              continue;                                                                 \
            }                                                                           \
          ++ret.map[calculate_property(cdh, cell_info, i)];                             \
        }                                                                               \
      return ret;                                                                       \
    }                                                                                   \
    template <class ... Args>                                                           \
    static bool filter(const storage_t & store,                                         \
                       const CaloRecGPU::CellInfoArr & cell_info,                       \
                       Args && ... args)                                                \
    {                                                                                   \
      const int cell = get_last(std::forward<Args>(args)...);                           \
      if (!cell_info.is_valid(cell))                                                    \
        {                                                                               \
          return false;                                                                 \
        }                                                                               \
      auto it = store.map.find(calculate_property(*store.cdh_ptr, cell_info, cell));    \
      if (it == store.map.end())                                                        \
        {                                                                               \
          return false;                                                                 \
        }                                                                               \
      else                                                                              \
        {                                                                               \
          return it->second > 1;                                                        \
        }                                                                               \
    }                                                                                   \
    static constexpr bool can_be_single()                                               \
    {                                                                                   \
      return true;                                                                      \
    }                                                                                   \
    static constexpr bool can_be_combined()                                             \
    {                                                                                   \
      return true;                                                                      \
    }                                                                                   \
  };                                                                                    \

    CALORECGPU_CELL_TYPE_SAME_GLOBAL(same_E, "Same Energy", "Same Energy",
                                     static double calculate_property(const ConstantDataHolder & /*cdh*/,
                                                                      const CaloRecGPU::CellInfoArr & cell_info,
                                                                      const int cell)
    {
      return cell_info.energy[cell];
    }
                                    )


    CALORECGPU_CELL_TYPE_SAME_GLOBAL(same_SNR, "Same Signal-to-Noise Ratio", "Same Signal-to-Noise",
                                     static float calculate_property(const ConstantDataHolder & cdh,
                                                                     const CaloRecGPU::CellInfoArr & cell_info,
                                                                     const int cell)
    {
      return cell_info.energy[cell] / cdh.m_cell_noise->noise[cell_info.gain[cell]][cell];
    }
                                    )

    CALORECGPU_CELL_TYPE_SAME_GLOBAL(same_abs_SNR, "Same Absolute Signal-to-Noise Ratio", "Same Absolute Signal-to-Noise Ratio",
                                     static float calculate_property(const ConstantDataHolder & cdh,
                                                                     const CaloRecGPU::CellInfoArr & cell_info,
                                                                     const int cell)
    {
      return std::abs(cell_info.energy[cell] / cdh.m_cell_noise->noise[cell_info.gain[cell]][cell]);
    }
                                    )

    CALORECGPU_CELL_TYPE_SAME_GLOBAL(confusable_E, "Confusable-Order (by Energy)", "Confusable",
                                     static std::pair<float, uint8_t> calculate_property(const ConstantDataHolder & /*cdh*/,
                                                                                         const CaloRecGPU::CellInfoArr & cell_info,
                                                                                         const int cell)
    {
      return std::make_pair(cell_info.energy[cell], CaloRecGPU::Helpers::Pearson_hash(cell));
    }
                                    )

    CALORECGPU_CELL_TYPE_SAME_GLOBAL(confusable_SNR, "Confusable-Order (by Signal-to-Noise)", "Confusable",
                                     static std::pair<float, uint8_t> calculate_property(const ConstantDataHolder & cdh,
                                                                                         const CaloRecGPU::CellInfoArr & cell_info,
                                                                                         const int cell)
    {
      return std::make_pair(cell_info.energy[cell] / cdh.m_cell_noise->noise[cell_info.gain[cell]][cell], CaloRecGPU::Helpers::Pearson_hash(cell));
    }
                                    )

    CALORECGPU_CELL_TYPE_SAME_GLOBAL(confusable_abs_SNR, "Confusable-Order (by Absolute Signal-to-Noise)", "Confusable",
                                     static std::pair<float, uint8_t> calculate_property(const ConstantDataHolder & cdh,
                                                                                         const CaloRecGPU::CellInfoArr & cell_info,
                                                                                         const int cell)
    {
      return std::make_pair(std::abs(cell_info.energy[cell] / cdh.m_cell_noise->noise[cell_info.gain[cell]][cell]),
                            CaloRecGPU::Helpers::Pearson_hash(cell));
    }
                                    )


    struct cell_type_same_cluster
    {
      static std::string filename()
      {
        return "same_cluster";
      }
      static std::string propername()
      {
        return "Similarly Assigned";
      }
      static std::string axisname()
      {
        return "Similarly Assigned";
      }

      struct storage_t
      {
        const CaloPlotterHelper::sample_comparisons_holder * sch = nullptr;
        const ConstantDataHolder * cdh = nullptr;
      };

      static storage_t build_storage(const ConstantDataHolder & cdh,
                                     const CaloRecGPU::CellInfoArr &,
                                     const CaloRecGPU::CellStateArr &,
                                     const CaloRecGPU::CellStateArr &,
                                     const CaloRecGPU::ClusterInfoArr &,
                                     const CaloRecGPU::ClusterInfoArr &,
                                      const CaloRecGPU::ClusterMomentsArr &,
                                      const CaloRecGPU::ClusterMomentsArr &,
                                     const CaloPlotterHelper::sample_comparisons_holder & sch)
      {
        return {&sch, &cdh};
      }

      static bool filter(const storage_t & store,
                         const CaloRecGPU::CellInfoArr & cell_info,
                         const CaloRecGPU::CellStateArr & cell_state_1,
                         const CaloRecGPU::CellStateArr & cell_state_2,
                         const int cell)
      {
        if (!cell_info.is_valid(cell))
          {
            return false;
          }
        const ClusterTag ref_tag = cell_state_1.clusterTag[cell];
        const ClusterTag test_tag = cell_state_2.clusterTag[cell];
        int ref_c1 = -2, ref_c2 = -2, test_c1 = -2, test_c2 = -2;
        if (ref_tag.is_part_of_cluster())
          {
            ref_c1 = ref_tag.cluster_index();
            ref_c2 = ref_tag.is_shared_between_clusters() ? ref_tag.secondary_cluster_index() : -2;
          }

        if (test_tag.is_part_of_cluster())
          {
            test_c1 = test_tag.cluster_index();
            test_c2 = test_tag.is_shared_between_clusters() ? test_tag.secondary_cluster_index() : -2;
          }

        const int match_1 = test_c1 < 0 ? -2 : store.sch->t2r(test_c1);
        const int match_2 = test_c2 < 0 ? -2 : store.sch->t2r(test_c2);

        if ((ref_c1 == match_1 && ref_c2 == match_2) ||
            (ref_c1 == match_2 && ref_c2 == match_1)    )
          {
            return true;
          }
        else
          {
            return false;
          }
      }
      static constexpr bool can_be_single()
      {
        return false;
      }
      static constexpr bool can_be_combined()
      {
        return true;
      }
    };

    struct cell_type_diff_cluster
    {
      static std::string filename()
      {
        return "diff_cluster";
      }
      static std::string propername()
      {
        return "Differently Assigned";
      }
      static std::string axisname()
      {
        return "Differently Assigned";
      }

      struct storage_t
      {
        const CaloPlotterHelper::sample_comparisons_holder * sch = nullptr;
        const ConstantDataHolder * cdh = nullptr;
      };

      static storage_t build_storage(const ConstantDataHolder & cdh,
                                     const CaloRecGPU::CellInfoArr &,
                                     const CaloRecGPU::CellStateArr &,
                                     const CaloRecGPU::CellStateArr &,
                                     const CaloRecGPU::ClusterInfoArr &,
                                     const CaloRecGPU::ClusterInfoArr &,
                                     const CaloRecGPU::ClusterMomentsArr &,
                                     const CaloRecGPU::ClusterMomentsArr &,
                                     const CaloPlotterHelper::sample_comparisons_holder & sch)
      {
        return {&sch, &cdh};
      }

      static bool filter(const storage_t & store,
                         const CaloRecGPU::CellInfoArr & cell_info,
                         const CaloRecGPU::CellStateArr & cell_state_1,
                         const CaloRecGPU::CellStateArr & cell_state_2,
                         const int cell)
      {
        if (!cell_info.is_valid(cell))
          {
            return false;
          }

        const ClusterTag ref_tag = cell_state_1.clusterTag[cell];
        const ClusterTag test_tag = cell_state_2.clusterTag[cell];
        int ref_c1 = -2, ref_c2 = -2, test_c1 = -2, test_c2 = -2;
        if (ref_tag.is_part_of_cluster())
          {
            ref_c1 = ref_tag.cluster_index();
            ref_c2 = ref_tag.is_shared_between_clusters() ? ref_tag.secondary_cluster_index() : -2;
          }

        if (test_tag.is_part_of_cluster())
          {
            test_c1 = test_tag.cluster_index();
            test_c2 = test_tag.is_shared_between_clusters() ? test_tag.secondary_cluster_index() : -2;
          }

        const int match_1 = test_c1 < 0 ? -2 : store.sch->t2r(test_c1);
        const int match_2 = test_c2 < 0 ? -2 : store.sch->t2r(test_c2);

        if ((ref_c1 == match_1 && ref_c2 == match_2) ||
            (ref_c1 == match_2 && ref_c2 == match_1)    )
          {
            return false;
          }
        else
          {
            /*
            std::cout << "MISMATCHED: " << cell << " " << ref_c1 << " " << ref_c2 << " " << match_1 << " " << match_2 << " (" << std::hex << ref_tag << " " << test_tag << ") " << std::dec << std::endl;
            int neighs[NMaxNeighbours];
            const int n_neighs = store.cdh->m_geometry->neighbours.get_neighbours(cell, neighs, false, false);
            for (int nn = 0; nn < n_neighs; ++nn)
              {
                const int neigh_cell = neighs[nn];
                const ClusterTag neigh_tag = cell_state_1.clusterTag[neigh_cell];
                const int neigh_idx_1 = neigh_tag.is_part_of_cluster() ? neigh_tag.cluster_index() : -1;
                const int neigh_idx_2 = neigh_tag.is_shared_between_clusters() ? neigh_tag.secondary_cluster_index() : -1;
                std::cout << "            " << neigh_cell << ": " << cell_info.energy[neigh_cell] << " " << neigh_idx_1 << " " << neigh_idx_2 << "\n";
              }
            std::cout << std::endl;
            */
            return true;
          }
      }
      static constexpr bool can_be_single()
      {
        return false;
      }
      static constexpr bool can_be_combined()
      {
        return true;
      }
    };
  }

  namespace CellProperties
  {
    //VA_ARGS should evaulate to a `double` corresponding to the relevant cell property.
    //Available variables within it: cdh, cell_info, cell
#define CALORECGPU_CELL_PROPERTY_GLOBAL(FILENAME, PROPERNAME, AXISNAME, UNIT, CUMULATIVE, ...) \
  struct cell_property_ ## FILENAME                                                            \
  {                                                                                            \
    static std::string filename()                                                              \
    {                                                                                          \
      return #FILENAME;                                                                        \
    }                                                                                          \
    static std::string propername()                                                            \
    {                                                                                          \
      return PROPERNAME;                                                                       \
    }                                                                                          \
    static std::string axisname()                                                              \
    {                                                                                          \
      return AXISNAME;                                                                         \
    }                                                                                          \
    static std::string unit()                                                                  \
    {                                                                                          \
      return UNIT;                                                                             \
    }                                                                                          \
    struct storage_t                                                                           \
    {                                                                                          \
      const ConstantDataHolder * cdh_ptr;                                                      \
      const CellInfoArr * cell_info_ptr;                                                       \
    };                                                                                         \
    template <class ... Args>                                                                  \
    static storage_t build_storage(const ConstantDataHolder & chd,                             \
                                   const CellInfoArr & cell_info,                              \
                                   Args && ...)                                                \
    {                                                                                          \
      return {&chd, &cell_info};                                                               \
    }                                                                                          \
    template <class ... Args>                                                                  \
    static double get_property([[maybe_unused]] const storage_t & store,                       \
                               Args && ... args)                                               \
    {                                                                                          \
      [[maybe_unused]] const ConstantDataHolder & cdh = *store.cdh_ptr;                        \
      [[maybe_unused]] const CellInfoArr & cell_info = *store.cell_info_ptr;                   \
      [[maybe_unused]] const int cell = get_last(std::forward<Args>(args)...);                 \
      return __VA_ARGS__;                                                                      \
    }                                                                                          \
    static constexpr bool can_be_single()                                                      \
    {                                                                                          \
      return true;                                                                             \
    }                                                                                          \
    static constexpr bool can_be_combined()                                                    \
    {                                                                                          \
      return true;                                                                             \
    }                                                                                          \
    static constexpr bool do_cumulative()                                                      \
    {                                                                                          \
      return CUMULATIVE;                                                                       \
    }                                                                                          \
  };                                                                                           \

    //VA_ARGS should evaulate to a `double` corresponding to the relevant cell property.
    //Available variables within it: cdh, cell_info, cell_state, cell
#define CALORECGPU_CELL_PROPERTY_SINGLE(FILENAME, PROPERNAME, AXISNAME, UNIT, CUMULATIVE, ...) \
  struct cell_property_ ## FILENAME                                                            \
  {                                                                                            \
    static std::string filename()                                                              \
    {                                                                                          \
      return #FILENAME;                                                                        \
    }                                                                                          \
    static std::string propername()                                                            \
    {                                                                                          \
      return PROPERNAME;                                                                       \
    }                                                                                          \
    static std::string axisname()                                                              \
    {                                                                                          \
      return AXISNAME;                                                                         \
    }                                                                                          \
    static std::string unit()                                                                  \
    {                                                                                          \
      return UNIT;                                                                             \
    }                                                                                          \
    struct storage_t                                                                           \
    {                                                                                          \
      const ConstantDataHolder * cdh_ptr;                                                      \
      const CellInfoArr * cell_info_ptr;                                                       \
    };                                                                                         \
    static storage_t build_storage(const ConstantDataHolder & chd,                             \
                                   const CaloRecGPU::CellInfoArr & cell_info,                  \
                                   const CaloRecGPU::CellStateArr &,                           \
                                   const CaloRecGPU::ClusterInfoArr &,                         \
                                   const CaloRecGPU::ClusterMomentsArr & )                     \
    {                                                                                          \
      return {&chd, &cell_info};                                                               \
    }                                                                                          \
    static double get_property([[maybe_unused]] const storage_t & store,                       \
                               [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state,   \
                               [[maybe_unused]] const CaloRecGPU::ClusterInfoArr &,            \
                               [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr &,         \
                               [[maybe_unused]] const int cell)                                \
    {                                                                                          \
      [[maybe_unused]] const ConstantDataHolder & cdh = *store.cdh_ptr;                        \
      [[maybe_unused]] const CellInfoArr & cell_info = *store.cell_info_ptr;                   \
      return __VA_ARGS__;                                                                      \
    }                                                                                          \
    static constexpr bool can_be_single()                                                      \
    {                                                                                          \
      return true;                                                                             \
    }                                                                                          \
    static constexpr bool can_be_combined()                                                    \
    {                                                                                          \
      return false;                                                                            \
    }                                                                                          \
    static constexpr bool do_cumulative()                                                      \
    {                                                                                          \
      return CUMULATIVE;                                                                       \
    }                                                                                          \
  };                                                                                           \


    //VA_ARGS should evaulate to a `double` corresponding to the relevant cell property.
    //Available variables within it: cdh, cell_info, cell_state_1, cell_state_2, , cell
#define CALORECGPU_CELL_PROPERTY_COMBINED(FILENAME, PROPERNAME, AXISNAME, UNIT, CUMULATIVE, ...) \
  struct cell_property_ ## FILENAME                                                              \
  {                                                                                              \
    static std::string filename()                                                                \
    {                                                                                            \
      return #FILENAME;                                                                          \
    }                                                                                            \
    static std::string propername()                                                              \
    {                                                                                            \
      return PROPERNAME;                                                                         \
    }                                                                                            \
    static std::string axisname()                                                                \
    {                                                                                            \
      return AXISNAME;                                                                           \
    }                                                                                            \
    static std::string unit()                                                                    \
    {                                                                                            \
      return UNIT;                                                                               \
    }                                                                                            \
    struct storage_t                                                                             \
    {                                                                                            \
      const ConstantDataHolder * cdh_ptr;                                                        \
      const CellInfoArr * cell_info_ptr;                                                         \
    };                                                                                           \
    static storage_t build_storage(const ConstantDataHolder & chd,                               \
                                   const CaloRecGPU::CellInfoArr & cell_info,                    \
                                   const CaloRecGPU::CellStateArr &,                             \
                                   const CaloRecGPU::ClusterInfoArr &,                           \
                                   const CaloRecGPU::ClusterMomentsArr &)                        \
    {                                                                                            \
      return {&chd, &cell_info};                                                                 \
    }                                                                                            \
    static double get_property([[maybe_unused]] const storage_t & store,                         \
                               [[maybe_unused]] const CaloRecGPU::CellStateArr & cell_state,     \
                               [[maybe_unused]] const CaloRecGPU::ClusterInfoArr &,              \
                               [[maybe_unused]] const CaloRecGPU::ClusterMomentsArr &,           \
                               [[maybe_unused]] const int cell)                                  \
    {                                                                                            \
      [[maybe_unused]] const ConstantDataHolder & cdh = *store.cdh_ptr;                          \
      [[maybe_unused]] const CellInfoArr & cell_info = *store.cell_info_ptr;                     \
      return __VA_ARGS__;                                                                        \
    }                                                                                            \
    static constexpr bool can_be_single()                                                        \
    {                                                                                            \
      return false;                                                                              \
    }                                                                                            \
    static constexpr bool can_be_combined()                                                      \
    {                                                                                            \
      return true;                                                                               \
    }                                                                                            \
    static constexpr bool do_cumulative()                                                        \
    {                                                                                            \
      return CUMULATIVE;                                                                         \
    }                                                                                            \
  };                                                                                             \


    CALORECGPU_CELL_PROPERTY_GLOBAL(E, "Energy", "E", "[MeV]", false, cell_info.energy[cell] / CLHEP::MeV)
    CALORECGPU_CELL_PROPERTY_GLOBAL(abs_E, "Absolute Energy", "#||{E}", "[MeV]", true, std::abs(cell_info.energy[cell]) / CLHEP::MeV)

    CALORECGPU_CELL_PROPERTY_GLOBAL(gain, "Gain", "Gain", "", false, cell_info.gain[cell])

    CALORECGPU_CELL_PROPERTY_GLOBAL(noise, "Noise", "Noise", "[MeV]", false,
                                    cdh.m_cell_noise->noise[cell_info.gain[cell]][cell])

    CALORECGPU_CELL_PROPERTY_GLOBAL(SNR, "Signal-to-Noise Ratio", "SNR", "", false,
                                    cell_info.energy[cell] /
                                    cdh.m_cell_noise->noise[cell_info.gain[cell]][cell])

    CALORECGPU_CELL_PROPERTY_GLOBAL(abs_SNR, "Absolute Signal-to-Noise Ratio", "#||{SNR}", "", true,
                                    std::abs(cell_info.energy[cell] /
                                             cdh.m_cell_noise->noise[cell_info.gain[cell]][cell]))

    CALORECGPU_CELL_PROPERTY_GLOBAL(time, "Time", "t_{Cell}", "[#mus]", false, cell_info.time[cell] / CLHEP::us)

    CALORECGPU_CELL_PROPERTY_GLOBAL(index, "Index", "Index", "", false, cell)

    CALORECGPU_CELL_PROPERTY_GLOBAL(Pearson_hash, "Pearson Hash", "Pearson Hash", "", false, CaloRecGPU::Helpers::Pearson_hash(cell))

    CALORECGPU_CELL_PROPERTY_GLOBAL(sampling, "Calorimeter Sampling", "Sampling", "", false, cdh.m_geometry->caloSample[cell])

    CALORECGPU_CELL_PROPERTY_GLOBAL(x, "x Coordinate ", "x", "[cm]", false, cdh.m_geometry->x[cell] / CLHEP::cm )
    CALORECGPU_CELL_PROPERTY_GLOBAL(y, "y Coordinate ", "y", "[cm]", false, cdh.m_geometry->y[cell] / CLHEP::cm )
    CALORECGPU_CELL_PROPERTY_GLOBAL(z, "z Coordinate ", "z", "[cm]", false, cdh.m_geometry->z[cell] / CLHEP::cm )

    CALORECGPU_CELL_PROPERTY_GLOBAL(phi, "#phi Coordinate", "#phi", "", false, cdh.m_geometry->phi[cell])

    CALORECGPU_CELL_PROPERTY_GLOBAL(eta, "#eta Coordinate", "#eta", "", false, cdh.m_geometry->eta[cell])


    CALORECGPU_CELL_PROPERTY_SINGLE(primary_cluster_index, "Primary Cluster Index", "Primary Cluster Index", "", true,
                                    ClusterTag{cell_state.clusterTag[cell]}.cluster_index()
                                   )

    CALORECGPU_CELL_PROPERTY_SINGLE(secondary_cluster_index, "Secondary Cluster Index", "Secondary Cluster Index", "", true,
                                    ClusterTag{cell_state.clusterTag[cell]}.secondary_cluster_index()
                                   )


    //Yes, these two are immediately invoked lambdas.
    //Less verbose than copy-pasting the entire classes...

    CALORECGPU_CELL_PROPERTY_SINGLE(primary_weight, "Primary Weight", "w_{1}", "", true,
                                    [&]() -> double
    {
      const ClusterTag tag = cell_state.clusterTag[cell];
      if (tag.is_part_of_cluster())
        {
          float rev_weight = float_unhack(tag.secondary_cluster_weight());
          return 1.0f - rev_weight;
        }
      else
        {
          return 0.f;
        }
    }()
                                   )

    CALORECGPU_CELL_PROPERTY_SINGLE(secondary_weight, "Secondary Weight", "w_{2}", "", true,
                                    [&]() -> double
    {
      const ClusterTag tag = cell_state.clusterTag[cell];
      if (tag.is_part_of_cluster())
        {
          float rev_weight = float_unhack(tag.secondary_cluster_weight());
          return rev_weight;
        }
      else
        {
          return 0.f;
        }
    }()
                                   )

  }

  //-------------------------------------

  namespace ClusterPlotsDefinitions
  {
    using namespace ClusterProperties;


    using ClusterProperties = multi_class_holder <

                              //NSF: CUSTOMIZATION POINT!
                              //     To add more cluster properties, just create the appropriate classes
                              //     and add them to the list in this multi_class_holder.
                              //     The implementation details provide the ability to make deltas
                              //     and 2d plots (also, from reference or test) from these.
                              //
                              //     FORBIDDEN STRINGS TO APPEAR ON CLUSTER PROPERTIES:
                              //     versus, rel, ref, test, norm, cumul

                              clusters_E,
                              clusters_abs_E,
                              clusters_Et,
                              clusters_eta,
                              clusters_phi,
                              clusters_size,
                              clusters_weighted_size,
                              clusters_delta_R,
                              clusters_delta_phi_in_range,
                              clusters_diff_cells,
                              clusters_diff_cells_weighted,clusters_moments_time,
                              clusters_moments_firstPhi,
                              clusters_moments_firstEta,
                              clusters_moments_secondR,
                              clusters_moments_secondLambda,
                              clusters_moments_deltaPhi,
                              clusters_moments_deltaTheta,
                              clusters_moments_deltaAlpha,
                              clusters_moments_centerX,
                              clusters_moments_centerY,
                              clusters_moments_centerZ,
                              clusters_moments_centerMag,
                              clusters_moments_centerLambda,
                              clusters_moments_lateral,
                              clusters_moments_longitudinal,
                              clusters_moments_engFracEM,
                              clusters_moments_engFracMax,
                              clusters_moments_engFracCore,
                              clusters_moments_firstEngDens,
                              clusters_moments_secondEngDens,
                              clusters_moments_isolation,
                              clusters_moments_engBadCells,
                              clusters_moments_nBadCells,
                              clusters_moments_nBadCellsCorr,
                              clusters_moments_badCellsCorrE,
                              clusters_moments_badLArQFrac,
                              clusters_moments_engPos,
                              clusters_moments_significance,
                              clusters_moments_cellSignificance,
                              clusters_moments_cellSigSampling,
                              clusters_moments_avgLArQ,
                              clusters_moments_avgTileQ,
                              clusters_moments_engBadHVCells,
                              clusters_moments_nBadHVCells,
                              clusters_moments_PTD,
                              clusters_moments_mass,
                              clusters_moments_EMProbability,
                              clusters_moments_hadWeight,
                              clusters_moments_OOCweight,
                              clusters_moments_DMweight,
                              clusters_moments_tileConfidenceLevel,
                              clusters_moments_secondTime,
                              clusters_moments_numCells,
                              clusters_moments_vertexFraction,
                              clusters_moments_nVertexFraction,
                              clusters_moments_etaCaloFrame,
                              clusters_moments_phiCaloFrame,
                              clusters_moments_eta1CaloFrame,
                              clusters_moments_phi1CaloFrame,
                              clusters_moments_eta2CaloFrame,
                              clusters_moments_phi2CaloFrame,
                              clusters_moments_engCalibTot,
                              clusters_moments_engCalibOutL,
                              clusters_moments_engCalibOutM,
                              clusters_moments_engCalibOutT,
                              clusters_moments_engCalibDeadL,
                              clusters_moments_engCalibDeadM,
                              clusters_moments_engCalibDeadT,
                              clusters_moments_engCalibEMB0,
                              clusters_moments_engCalibEME0,
                              clusters_moments_engCalibTileG3,
                              clusters_moments_engCalibDeadTot,
                              clusters_moments_engCalibDeadEMB0,
                              clusters_moments_engCalibDeadTile0,
                              clusters_moments_engCalibDeadTileG3,
                              clusters_moments_engCalibDeadEME0,
                              clusters_moments_engCalibDeadHEC0,
                              clusters_moments_engCalibDeadFCAL,
                              clusters_moments_engCalibDeadLeakage,
                              clusters_moments_engCalibDeadUnclass,
                              clusters_moments_engCalibFracEM,
                              clusters_moments_engCalibFracHad,
                              clusters_moments_engCalibFracRest
                              
                              >;
  }

  namespace CellPlotsDefinitions
  {
    using namespace CellTypes;
    using namespace CellProperties;


    using CellTypes = multi_class_holder <

                      //NSF: CUSTOMIZATION POINT!
                      //     To add more cell types, just create the appropriate classes
                      //     and add them to the list in this multi_class_holder.
                      //     The implementation details provide the ability
                      //     to make all the possible plots from these.
                      //
                      //     FORBIDDEN STRINGS TO APPEAR ON CELL TYPES:
                      //     versus, cells, count, ref, test, norm, cumul

                      cell_type_global,
                      cell_type_intracluster,
                      cell_type_extracluster,
                      cell_type_shared,
                      cell_type_same_E,
                      cell_type_same_SNR,
                      cell_type_same_abs_SNR,
                      cell_type_confusable_E,
                      cell_type_confusable_SNR,
                      cell_type_confusable_abs_SNR,
                      cell_type_same_cluster,
                      cell_type_diff_cluster

                      >;


    using CellProperties = multi_class_holder <

                           //NSF: CUSTOMIZATION POINT!
                           //     To add more cell properties, just create the appropriate classes
                           //     and add them to the list in this multi_class_holder.
                           //     The implementation details provide the ability
                           //     to make all the possible plots from these.
                           //
                           //     FORBIDDEN STRINGS TO APPEAR ON CELL PROPERTIES:
                           //     versus, cells, count, ref, test, norm, cumul

                           cell_property_E,
                           cell_property_abs_E,
                           cell_property_gain,
                           cell_property_noise,
                           cell_property_SNR,
                           cell_property_abs_SNR,
                           cell_property_time,
                           cell_property_index,
                           cell_property_sampling,
                           cell_property_x,
                           cell_property_y,
                           cell_property_z,
                           cell_property_eta,
                           cell_property_Pearson_hash,
                           cell_property_primary_cluster_index,
                           cell_property_secondary_cluster_index,
                           cell_property_primary_weight,
                           cell_property_secondary_weight

                           >;
  }

  //-------------------------------------

  [[maybe_unused]] static bool simple_string_starts_with(const std::string & container, const std::string & contained)
  {
    if (contained.size() > container.size())
      {
        return false;
      }
    for (size_t i = 0; i < contained.size(); ++i)
      {
        if (container[i] != contained[i])
          {
            return false;
          }
      }
    return true;
  }

  [[maybe_unused]] static bool match_with_asterisk (const std::string & to_match, const std::string & pattern_before_asterisk, const bool asterisk_afterward)
  {
    if (pattern_before_asterisk == to_match)
      {
        return true;
      }
    else if (asterisk_afterward)
      {
        return simple_string_starts_with(to_match, pattern_before_asterisk);
      }
    else
      {
        return false;
      }
  }

  [[maybe_unused]] static bool match_with_asterisk (const std::string & to_match, const std::string & pattern)
  {
    if (pattern.size() > 0 && pattern[pattern.size() - 1] == '*')
      {
        return match_with_asterisk(to_match, pattern.substr(0, pattern.size() - 1), true);
      }
    else
      {
        return match_with_asterisk(to_match, pattern, false);
      }
  }

  struct find_results
  {
    size_t found_begin = std::string::npos, found_size = 0;
    bool was_found() const
    {
      return found_begin != std::string::npos;
    }
    bool was_found_exactly(const std::string & pattern) const
    {
      return found_size == pattern.size();
    }
    std::string before(const std::string & str) const
    {
      return str.substr(0, found_begin);
    }
    std::string middle(const std::string & str) const
    {
      if (str.size() == 0)
        {
          return "";
        }
      else if (found_begin < str.size())
        {
          return str.substr(found_begin, found_size);
        }
      else if (str.size() == found_begin)
        {
          return "*";
        }
      else
        {
          return "";
        }
    }
    std::string after(const std::string & str) const
    {
      if (str.size() == 0)
        {
          return "";
        }
      else if (found_begin + found_size < str.size() )
        {
          return str.substr(found_begin + found_size);
        }
      else if (str.size() == found_begin)
        {
          return "*";
        }
      else
        {
          return "";
        }
    }
  };

  ///Finds first occurrence of @p pattern, taking into account asterisks as wildcards.
  ///
  ///match("abc"   , "bc")   -> "a"      | "bc" | ""
  ///match("abcbc" , "bc")   -> "a"      | "bc" | "bc"
  ///match("a"     , "bc")   -> "a"      | ""   | ""
  ///match("*"     , "bc")   -> "*"      | "*"  | "*"
  ///match("b*"    , "bc")   -> ""       | "b"  | "*"
  ///match("bc*"   , "bc")   -> ""       | "bc" | "*"
  ///match("bca*"  , "bc")   -> ""       | "bc" | "a*"
  ///match("ab*"   , "bc")   -> "a"      | "b"  | "*"
  ///match("abcb*" , "bc")   -> "a"      | "bc" | "b*"
  ///match("abcbc*", "bc")   -> "a"      | "bc" | "bc*"
  [[maybe_unused]] static find_results get_earliest_possible_match(const std::string & to_search, const std::string & pattern)
  {
    if (to_search.size() == 0)
      {
        return {std::string::npos * (pattern.size() > 0), 0};
      }

    const bool asterisk_at_end = to_search[to_search.size() - 1] == '*';

    if (to_search.size() < pattern.size() && !asterisk_at_end)
      {
        return {std::string::npos * (pattern.size() > 0), 0};
      }

    const size_t full = to_search.find(pattern);
    if (full < std::string::npos)
      //Matched string completely
      {
        return {full, pattern.size()};
      }

    if (!asterisk_at_end)
      //If this is not a wildcard, no matching can be done!
      {
        return {std::string::npos, 0};
      }

    for (size_t i = std::min(pattern.size(), to_search.size() - 1); i > 0; --i)
      {
        const std::string current = to_search.substr(to_search.size() - i - 1, i);
        const std::string matching = pattern.substr(0, i);
        if (current == matching)
          {
            return {to_search.size() - i - 1, i};
          }
      }

    return {to_search.size(), 0};
  }


  ///Finds last occurrence of @p pattern, taking into account asterisks as wildcards.
  ///
  ///match("abc"   , "bc")   -> "a"      | "bc" | ""
  ///match("abcbc" , "bc")   -> "abc"    | "bc" | ""
  ///match("a"     , "bc")   -> "a"      | ""   | ""
  ///match("*"     , "bc")   -> "*"      | "*"  | "*"
  ///match("b*"    , "bc")   -> "b*"     | "*"  | "*"
  ///match("bc*"   , "bc")   -> "bc*"    | "*"  | "*"
  ///match("bca*"  , "bc")   -> "bca*"   | "*"  | "*"
  ///match("ab*"   , "bc")   -> "ab*"    | "*"  | "*"
  ///match("abcb*" , "bc")   -> "abcb*"  | "*"  | "*"
  ///match("abcbc*", "bc")   -> "abcbc*" | "*"  | "*"
  [[maybe_unused]] static find_results get_latest_possible_match(const std::string & to_search, const std::string & pattern)
  {
    if (to_search.size() == 0)
      {
        return {std::string::npos * (pattern.size() > 0), 0};
      }

    const bool asterisk_at_end = to_search[to_search.size() - 1] == '*';

    if (to_search.size() < pattern.size() && !asterisk_at_end)
      {
        return {std::string::npos * (pattern.size() > 0), 0};
      }

    if (asterisk_at_end)
      {
        return {to_search.size(), 0};
      }

    const size_t full = to_search.rfind(pattern);
    if (full < std::string::npos)
      //Matched string completely
      {
        return {full, pattern.size()};
      }

    return {std::string::npos, 0};
  }



  ///Finds first occurrence of @p pattern, taking into account asterisks as wildcards, but always absorbing a (potentially partial) suffix if available
  ///
  ///match("abc"   , "bc")   -> "a"      | "bc" | ""
  ///match("abcbc" , "bc")   -> "abc"    | "bc" | ""
  ///match("a"     , "bc")   -> "a"      | ""   | ""
  ///match("*"     , "bc")   -> "*"      | "*"  | "*"
  ///match("b*"    , "bc")   -> ""       | "b"  | "*"
  ///match("bc*"   , "bc")   -> ""       | "bc" | "*"
  ///match("bca*"  , "bc")   -> "bca*"   | "*"  | "*"
  ///match("ab*"   , "bc")   -> "a"      | "b"  | "*"
  ///match("abcb*" , "bc")   -> "abc"    | "b"  | "*"
  ///match("abcbc*", "bc")   -> "abc"    | "bc" | "*"
  [[maybe_unused]] static find_results get_latest_possible_match_with_suffix(const std::string & to_search, const std::string & pattern)
  {
    if (to_search.size() == 0)
      {
        return {std::string::npos * (pattern.size() > 0), 0};
      }

    const bool asterisk_at_end = to_search[to_search.size() - 1] == '*';

    if (to_search.size() < pattern.size() && !asterisk_at_end)
      {
        return {std::string::npos * (pattern.size() > 0), 0};
      }

    if (asterisk_at_end)
      {
        for (size_t i = std::min(pattern.size(), to_search.size() - 1); i > 0; --i)
          {
            const std::string current = to_search.substr(to_search.size() - i - 1, i);
            const std::string matching = pattern.substr(0, i);
            if (current == matching)
              {
                return {to_search.size() - i - 1, i};
              }
          }
        return {to_search.size(), 0};
      }

    const size_t full = to_search.rfind(pattern);
    if (full < std::string::npos)
      //Matched string completely
      {
        return {full, pattern.size()};
      }

    return {std::string::npos, 0};
  }

  // * * * * * * *

  template <class T, class ... Classes>
  std::vector<T> match_classes_by_type(const std::string & name, multi_class_holder<Classes...>)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "                            MATCH BY TYPE: " << name << std::endl;
#endif
    std::vector<T> ret;

    auto add_if_match = [&](const auto & c)
    {
      if (name == "@" || match_with_asterisk(c.filename(), name))
        {

#if CALORECGPU_PLOTDESCDEBUG
          std::cout << "Matched: " << c.filename() << std::endl;
#endif
          ret.emplace_back(c);
        }
    };

    (void) ( add_if_match(Classes{}), ... );

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "\n";
#endif

    return ret;
  }

  using build_store_func_both_t = std::any(const ConstantDataHolder &,
                                           const CaloRecGPU::CellInfoArr &);

  using build_store_func_single_t = std::any(const ConstantDataHolder &,
                                             const CaloRecGPU::CellInfoArr &,
                                             const CaloRecGPU::CellStateArr &,
                                             const CaloRecGPU::ClusterInfoArr &,
                                             const CaloRecGPU::ClusterMomentsArr &);

  using build_store_func_combined_t = std::any(const ConstantDataHolder &,
                                               const CaloRecGPU::CellInfoArr &,
                                               const CaloRecGPU::CellStateArr &,
                                               const CaloRecGPU::CellStateArr &,
                                               const CaloRecGPU::ClusterInfoArr &,
                                               const CaloRecGPU::ClusterInfoArr &,
                                               const CaloRecGPU::ClusterMomentsArr &,
                                               const CaloRecGPU::ClusterMomentsArr &,
                                               const CaloPlotterHelper::sample_comparisons_holder &);

  using get_property_func_both_t = double(const std::any &,
                                          const int);

  using get_property_func_single_t = double(const std::any &,
                                            const CaloRecGPU::CellStateArr &,
                                            const CaloRecGPU::ClusterInfoArr &,
                                            const CaloRecGPU::ClusterMomentsArr &,
                                            const int);

  using get_property_func_combined_t = double(const std::any &,
                                              const CaloRecGPU::CellStateArr &,
                                              const CaloRecGPU::ClusterInfoArr &,
                                              const CaloRecGPU::ClusterMomentsArr &,
                                              const int,
                                              const CaloRecGPU::CellStateArr &,
                                              const CaloRecGPU::ClusterInfoArr &,
                                              const CaloRecGPU::ClusterMomentsArr &,
                                              const int);

  struct base_propriety_descriptor_helper
  {
    std::string name, axis, unit, title;

    bool is_single = false;
    bool is_combined = false;

    ///Expresses the possibility of being normalized
    ///up until the moment when the plotter is built,
    ///at which point it represents whether to normalize...
    bool normalize = false;

    ///Expresses the possibility of doing cumulative plots
    ///up until the moment when the plotter is built,
    ///at which point it represents whether to do them...
    bool cumulative = false;

    std::variant< std::function<build_store_func_single_t>,
        std::function<build_store_func_combined_t>,
        std::function<build_store_func_both_t> > build_store;
    std::variant<std::function<get_property_func_single_t>,
        std::function<get_property_func_combined_t>,
        std::function<get_property_func_both_t>> get_property;


    std::function<build_store_func_single_t> encapsulate_store_single() const
    {
      if (!is_combined)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<build_store_func_single_t>>(build_store);
        }
      else
        {
          auto both_store = std::get<std::function<build_store_func_both_t>>(build_store);
          return [both_store](const ConstantDataHolder & cdh,
                              const CaloRecGPU::CellInfoArr & cell_info,
                              const CaloRecGPU::CellStateArr &,
                              const CaloRecGPU::ClusterInfoArr &,
                              const CaloRecGPU::ClusterMomentsArr &)
          {
            return both_store(cdh, cell_info);
          };
        }
    }

    std::function<build_store_func_combined_t> encapsulate_store_combined() const
    {
      if (!is_single)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<build_store_func_combined_t>>(build_store);
        }
      else
        {
          auto both_store = std::get<std::function<build_store_func_both_t>>(build_store);
          return [both_store](const ConstantDataHolder & cdh,
                              const CaloRecGPU::CellInfoArr & cell_info,
                              const CaloRecGPU::CellStateArr &,
                              const CaloRecGPU::CellStateArr &,
                              const CaloRecGPU::ClusterInfoArr &,
                              const CaloRecGPU::ClusterInfoArr &,
                              const CaloRecGPU::ClusterMomentsArr &,
                              const CaloRecGPU::ClusterMomentsArr &,
                              const CaloPlotterHelper::sample_comparisons_holder & )
          {
            return both_store(cdh, cell_info);
          };
        }
    }

    std::function<get_property_func_single_t> encapsulate_get_single() const
    {
      if (!is_combined)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<get_property_func_single_t>>(get_property);
        }
      else
        {
          auto both_get = std::get<std::function<get_property_func_both_t>>(get_property);
          return [both_get](const std::any & store,
                            const CaloRecGPU::CellStateArr &,
                            const CaloRecGPU::ClusterInfoArr &,
                            const CaloRecGPU::ClusterMomentsArr &,
                            const int index)
          {
            return both_get(store, index);
          };
        }
    }

    std::function<get_property_func_combined_t> encapsulate_get_combined() const
    {
      if (!is_single)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<get_property_func_combined_t>>(get_property);
        }
      else
        {
          auto both_get = std::get<std::function<get_property_func_both_t>>(get_property);
          return [both_get](const std::any & store,
                            const CaloRecGPU::CellStateArr &,
                            const CaloRecGPU::ClusterInfoArr &,
                            const CaloRecGPU::ClusterMomentsArr &,
                            const int,
                            const CaloRecGPU::CellStateArr &,
                            const CaloRecGPU::ClusterInfoArr &,
                            const CaloRecGPU::ClusterMomentsArr &,
                            const int index)
          {
            return both_get(store, index);
          };
        }
    }


  };

  ///@warning We assume @p p is a single plot!
  template <class PropDesc>
  static PropDesc make_delta_property(const PropDesc & p)
  {
    PropDesc ret;
    ret.name = std::string("delta_") + p.name;
    ret.axis = p.axis + "^{(2)} - " + p.axis + "^{(1)}";
    ret.unit = p.unit;
    ret.title = p.title + " Differences";
    ret.is_single = false;
    ret.is_combined = true;
    ret.normalize = p.normalize;
    ret.cumulative = false;
    //Deltas shouldn't be cumulative...

    auto build_store = p.encapsulate_store_single();
    auto get_prop = p.encapsulate_get_single();

    ret.build_store = std::function<build_store_func_combined_t>([build_store]
                                                                 (const ConstantDataHolder & cdh,
                                                                  const CaloRecGPU::CellInfoArr & cell_info,
                                                                  const CaloRecGPU::CellStateArr & cell_state_1,
                                                                  const CaloRecGPU::CellStateArr & cell_state_2,
                                                                  const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                                                                  const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                                                                  const CaloRecGPU::ClusterMomentsArr & cluster_moments_1,
                                                                  const CaloRecGPU::ClusterMomentsArr & cluster_moments_2,
                                                                  const CaloPlotterHelper::sample_comparisons_holder &)
    {
      return std::make_any<std::pair<std::any, std::any>>(build_store(cdh, cell_info, cell_state_1, cluster_info_1, cluster_moments_1),
                                                          build_store(cdh, cell_info, cell_state_2, cluster_info_2, cluster_moments_2)  );
    }
                                                                );

    ret.get_property = std::function<get_property_func_combined_t>([get_prop]
                                                                   (const std::any & store,
                                                                    const CaloRecGPU::CellStateArr & cell_state_1,
                                                                    const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                                                                    const CaloRecGPU::ClusterMomentsArr & cluster_moments_1,
                                                                    const int index_1,
                                                                    const CaloRecGPU::CellStateArr & cell_state_2,
                                                                    const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                                                                    const CaloRecGPU::ClusterMomentsArr & cluster_moments_2,
                                                                    const int index_2)
    {
      using to_convert = std::pair<std::any, std::any>;
      const to_convert * store_ptr = std::any_cast<to_convert>(&store);
      return get_prop(store_ptr->second, cell_state_2, cluster_info_2, cluster_moments_2, index_2) -
             get_prop(store_ptr->first, cell_state_1, cluster_info_1, cluster_moments_1, index_1);
    }
                                                                  );

    return ret;
  }

  ///@warning We assume @p p is a single plot!
  template <class PropDesc>
  static PropDesc make_reference_property(const PropDesc & p)
  {
    PropDesc ret;
    ret.name = p.name + "_ref";
    ret.axis = p.axis + "^{(1)}";
    ret.unit = p.unit;
    ret.title = std::string("Reference ") + p.title;
    ret.is_single = false;
    ret.is_combined = true;
    ret.normalize = p.normalize;
    ret.cumulative = p.cumulative;

    auto build_store = p.encapsulate_store_single();
    auto get_prop = p.encapsulate_get_single();

    ret.build_store = std::function<build_store_func_combined_t>([build_store]
                                                                 (const ConstantDataHolder & cdh,
                                                                  const CaloRecGPU::CellInfoArr & cell_info,
                                                                  const CaloRecGPU::CellStateArr & cell_state_1,
                                                                  const CaloRecGPU::CellStateArr &,
                                                                  const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                                                                  const CaloRecGPU::ClusterInfoArr &,
                                                                  const CaloRecGPU::ClusterMomentsArr & cluster_moments_1,
                                                                  const CaloRecGPU::ClusterMomentsArr &,
                                                                  const CaloPlotterHelper::sample_comparisons_holder &)
    {
      return build_store(cdh, cell_info, cell_state_1, cluster_info_1, cluster_moments_1);
    }
                                                                );

    ret.get_property = std::function<get_property_func_combined_t>([get_prop]
                                                                   (const std::any & store,
                                                                    const CaloRecGPU::CellStateArr & cell_state_1,
                                                                    const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                                                                    const CaloRecGPU::ClusterMomentsArr & cluster_moments_1,
                                                                    const int index_1,
                                                                    const CaloRecGPU::CellStateArr &,
                                                                    const CaloRecGPU::ClusterInfoArr &,
                                                                    const CaloRecGPU::ClusterMomentsArr &,
                                                                    const int)
    {
      return get_prop(store, cell_state_1, cluster_info_1, cluster_moments_1, index_1);
    }
                                                                  );

    return ret;
  }

  ///@warning We assume @p p is a single plot!
  template <class PropDesc>
  static PropDesc make_test_property(const PropDesc & p)
  {
    PropDesc ret;
    ret.name = p.name + "_test";
    ret.axis = p.axis + "^{(2)}";
    ret.unit = p.unit;
    ret.title = std::string("Test ") + p.title;
    ret.is_single = false;
    ret.is_combined = true;
    ret.normalize = p.normalize;
    ret.cumulative = p.cumulative;

    auto build_store = p.encapsulate_store_single();
    auto get_prop = p.encapsulate_get_single();

    ret.build_store = std::function<build_store_func_combined_t>([build_store]
                                                                 (const ConstantDataHolder & cdh,
                                                                  const CaloRecGPU::CellInfoArr & cell_info,
                                                                  const CaloRecGPU::CellStateArr &,
                                                                  const CaloRecGPU::CellStateArr & cell_state_2,
                                                                  const CaloRecGPU::ClusterInfoArr &,
                                                                  const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                                                                  const CaloRecGPU::ClusterMomentsArr &,
                                                                  const CaloRecGPU::ClusterMomentsArr & cluster_moments_2,
                                                                  const CaloPlotterHelper::sample_comparisons_holder &)
    {
      return build_store(cdh, cell_info, cell_state_2, cluster_info_2, cluster_moments_2);
    }
                                                                );

    ret.get_property = std::function<get_property_func_combined_t>([get_prop]
                                                                   (const std::any & store,
                                                                    const CaloRecGPU::CellStateArr &,
                                                                    const CaloRecGPU::ClusterInfoArr &,
                                                                    const CaloRecGPU::ClusterMomentsArr &,
                                                                    const int,
                                                                    const CaloRecGPU::CellStateArr & cell_state_2,
                                                                    const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                                                                    const CaloRecGPU::ClusterMomentsArr & cluster_moments_2,
                                                                    const int index_2)
    {
      return get_prop(store, cell_state_2, cluster_info_2, cluster_moments_2, index_2);
    }
                                                                  );

    return ret;
  }

  ///@warning We assume @p p1 and @p p2 are the same kind of (combined, single or both) plots!
  template <class PropDesc>
  static PropDesc make_relative_property(const PropDesc & p1,
                                         const PropDesc & p2)
  {
    PropDesc ret;
    ret.name = p1.name + "_rel_" + p2.name;
    ret.axis = std::string("#(){") + p1.axis + "}/#(){" + p2.axis + "}";

    if (p1.unit == p2.unit)
      {
        ret.unit = "";
      }
    else
      {
        //We might even build some system that allows easier dimensional reasoning,
        //but I expect this to be most relevant for plots relative either to the own unit
        //or to adimensional quantities (cluster sizes).
        if (p1.unit.size() == 0)
          {
            ret.unit = std::string("[#(){") + p2.unit.substr(1, p2.unit.size() - 1) + "}^{-1}]";
          }
        else if (p2.unit.size() == 0)
          {
            ret.unit = p1.unit;
          }
        else
          //The case where both are null strings is handled by the previous branch of the top-level if.
          {
            ret.unit = std::string("[#(){") + p1.unit.substr(1, p1.unit.size() - 1) +
                       "}/#(){" + p2.unit.substr(1, p2.unit.size() - 1) + "}]";
          }
      }

    ret.title = p1.title + " Relative to " + p2.title;
    ret.is_single = p1.is_single && p2.is_single;
    ret.is_combined = p1.is_combined && p2.is_combined;
    ret.normalize = p1.normalize && p2.normalize;
    ret.cumulative = p1.cumulative && p2.cumulative;

    const bool is_single = ret.is_single;
    const bool is_combined = ret.is_combined;

    if (is_single && is_combined)
      {
        auto build_store_1 = std::get<std::function<build_store_func_both_t>>(p1.build_store);
        auto build_store_2 = std::get<std::function<build_store_func_both_t>>(p2.build_store);

        auto rel_build_store = [build_store_1, build_store_2](auto && ... args)
        {
          return std::make_any<std::pair<std::any, std::any>>(build_store_1(std::forward<decltype(args)>(args)...),
                                                              build_store_2(std::forward<decltype(args)>(args)...)  );
        };

        ret.build_store = std::function<build_store_func_both_t>(rel_build_store);
      }
    else if (is_combined)
      {
        auto build_store_1 = p1.encapsulate_store_combined();
        auto build_store_2 = p2.encapsulate_store_combined();

        auto rel_build_store = [build_store_1, build_store_2](auto && ... args)
        {
          return std::make_any<std::pair<std::any, std::any>>(build_store_1(std::forward<decltype(args)>(args)...),
                                                              build_store_2(std::forward<decltype(args)>(args)...)  );
        };

        ret.build_store = std::function<build_store_func_combined_t>(rel_build_store);
      }
    else if (is_single)
      {
        auto build_store_1 = p1.encapsulate_store_single();
        auto build_store_2 = p2.encapsulate_store_single();

        auto rel_build_store = [build_store_1, build_store_2](auto && ... args)
        {
          return std::make_any<std::pair<std::any, std::any>>(build_store_1(std::forward<decltype(args)>(args)...),
                                                              build_store_2(std::forward<decltype(args)>(args)...)  );
        };

        ret.build_store = std::function<build_store_func_single_t>(rel_build_store);
      }

    if (is_single && is_combined)
      {
        auto get_prop_1 = std::get<std::function<get_property_func_both_t>>(p1.get_property);
        auto get_prop_2 = std::get<std::function<get_property_func_both_t>>(p2.get_property);

        auto rel_get_prop = [get_prop_1, get_prop_2](const std::any & store, auto && ... args)
        {
          using to_convert = std::pair<std::any, std::any>;
          const to_convert * store_ptr = std::any_cast<to_convert>(&store);
          auto prop_2 = get_prop_2(store_ptr->second, std::forward<decltype(args)>(args)...);
          return (std::abs(prop_2) > 1e-6 ? get_prop_1(store_ptr->first, std::forward<decltype(args)>(args)...)/prop_2 : 0);
        };

        ret.get_property = std::function<get_property_func_both_t>(rel_get_prop);
      }
    else if (is_combined)
      {
        auto get_prop_1 = p1.encapsulate_get_combined();
        auto get_prop_2 = p2.encapsulate_get_combined();

        auto rel_get_prop = [get_prop_1, get_prop_2](const std::any & store, auto && ... args)
        {
          using to_convert = std::pair<std::any, std::any>;
          const to_convert * store_ptr = std::any_cast<to_convert>(&store);
          auto prop_2 = get_prop_2(store_ptr->second, std::forward<decltype(args)>(args)...);
          return (std::abs(prop_2) > 1e-6 ? get_prop_1(store_ptr->first, std::forward<decltype(args)>(args)...)/prop_2 : 0);
        };

        ret.get_property = std::function<get_property_func_combined_t>(rel_get_prop);
      }
    else if (is_single)
      {
        auto get_prop_1 = p1.encapsulate_get_single();
        auto get_prop_2 = p2.encapsulate_get_single();

        auto rel_get_prop = [get_prop_1, get_prop_2](const std::any & store, auto && ... args)
        {
          using to_convert = std::pair<std::any, std::any>;
          const to_convert * store_ptr = std::any_cast<to_convert>(&store);
          auto prop_2 = get_prop_2(store_ptr->second, std::forward<decltype(args)>(args)...);
          return (std::abs(prop_2) > 1e-6 ? get_prop_1(store_ptr->first, std::forward<decltype(args)>(args)...)/prop_2 : 0);
        };

        ret.get_property = std::function<get_property_func_single_t>(rel_get_prop);
      }

    return ret;
  }

  template <class PropertyDescriptor, class PropertyHolder>
  static std::vector<PropertyDescriptor> get_properties_from_string_no_delta(const PropertyHolder & ph, const std::string & name)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "            IMPL NO DELTA: " << name << std::endl;
#endif

    std::vector<PropertyDescriptor> ret;

    if (name.size() == 0)
      {
        return ret;
      }

    auto find_ref = get_latest_possible_match_with_suffix(name, "_ref");
    auto find_test = get_latest_possible_match_with_suffix(name, "_test");

    const std::string after_ref = find_ref.after(name);
    const std::string after_test = find_ref.after(name);

    if (find_ref.was_found() && (after_ref == "" || after_ref == "*"))
      {
        const auto other_vector = match_classes_by_type<PropertyDescriptor>(find_ref.before(name), ph);

        for (const auto & p : other_vector)
          {
            ret.emplace_back(make_reference_property(p));
          }
      }

    if (find_test.was_found() && (after_test == "" || after_test == "*"))
      {
        const auto other_vector = match_classes_by_type<PropertyDescriptor>(find_test.before(name), ph);

        for (const auto & p : other_vector)
          {
            ret.emplace_back(make_test_property(p));
          }
      }

    if (!find_ref.was_found_exactly("_ref") &&
        !find_test.was_found_exactly("_test")   )
      {
        const auto prop_vector = match_classes_by_type<PropertyDescriptor>(name, ph);
        for (const auto & p : prop_vector)
          {
            ret.emplace_back(p);
          }
      }

    return ret;

  }

  template <class PropertyDescriptor, class PropertyHolder>
  static std::vector<PropertyDescriptor> get_properties_from_string_no_rel(const PropertyHolder & ph, const std::string & name)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "            IMPL NO _REL_: " << name << std::endl;
#endif
    std::vector<PropertyDescriptor> ret;

    if (name.size() == 0)
      {
        return ret;
      }

    const auto find_delta = get_earliest_possible_match(name, "delta_");
    const std::string before_str = find_delta.before(name);

    if (find_delta.was_found() && (before_str == "" || before_str == "*"))
      {
        auto other_vector = get_properties_from_string_no_delta<PropertyDescriptor>(ph, find_delta.after(name));

        for (const auto & p : other_vector)
          {
            if (!p.is_combined && p.is_single)
              {
                ret.emplace_back(make_delta_property(p));
              }
          }
      }
    //'delta' is not a forbidden "keyword":
    //so even in this case we check for properties
    //with delta in the name...

    auto prop_vector = get_properties_from_string_no_delta<PropertyDescriptor>(ph, name);

    for (const auto & p : prop_vector)
      {
        ret.emplace_back(p);
      }

    return ret;
  }

  template <class PropertyDescriptor, class PropertyHolder>
  static std::vector<PropertyDescriptor> get_properties_from_string_no_norm_cumul(const PropertyHolder & ph, const std::string & name)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "            IMPL NO _N/C_: " << name << std::endl;
#endif
    std::vector<PropertyDescriptor> ret;

    if (name.size() == 0)
      {
        return ret;
      }

    const auto find_rel = get_earliest_possible_match(name, "_rel_");

    if (find_rel.was_found())
      {
        const auto vec_1 = get_properties_from_string_no_rel<PropertyDescriptor>(ph, find_rel.before(name));
        const auto vec_2 = get_properties_from_string_no_rel<PropertyDescriptor>(ph, find_rel.after(name));

        for (const auto & p1 : vec_1)
          {
            for (const auto & p2 : vec_2)
              {
                if (p1.is_combined == p2.is_combined && p1.is_single == p2.is_single)
                  {
                    ret.emplace_back(make_relative_property(p1, p2));
                  }
              }
          }
      }

    if (!find_rel.was_found_exactly("_rel_"))
      {
        const auto prop_vector = get_properties_from_string_no_rel<PropertyDescriptor>(ph, name);
        for (const auto & p : prop_vector)
          {
            ret.emplace_back(p);
          }
      }

    return ret;
  }

  template <class PropertyDescriptor, class PropertyHolder>
  static std::vector<PropertyDescriptor> get_properties_from_string(const PropertyHolder & ph, const std::string & name, const bool consider_norm, const bool consider_cumul)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "RAW PROP FROM STR: " << name << " " << consider_norm << " " << consider_cumul << std::endl;
#endif
    std::vector<PropertyDescriptor> properties;

    if (name.size() == 0)
      {
        return properties;
      }

    auto find_norm = get_latest_possible_match_with_suffix(name, "_norm");
    auto find_cumul = get_latest_possible_match_with_suffix(name, "_cumul");

    const std::string after_norm = find_norm.after("_norm");
    const std::string after_cumul = find_cumul.after("_cumul");

    if (consider_norm && find_norm.was_found() && (after_norm == "" || after_norm == "*"))
      {
        const auto other_vector = get_properties_from_string_no_norm_cumul<PropertyDescriptor>(ph, find_norm.before(name));

        for (const auto & p : other_vector)
          {
            properties.emplace_back(p);
            properties.back().normalize = true;
            properties.back().cumulative = false;
          }
      }

    if (consider_cumul && find_cumul.was_found() && (after_cumul == "" || after_cumul == "*"))
      {
        const auto other_vector = get_properties_from_string_no_norm_cumul<PropertyDescriptor>(ph, find_cumul.before(name));

        for (const auto & p : other_vector)
          {
            properties.emplace_back(p);
            properties.back().normalize = false;
            properties.back().cumulative = true;
          }
      }

    if ( (!consider_norm || !find_norm.was_found_exactly("_norm")) &&
         (!consider_cumul || !find_cumul.was_found_exactly("_cumul"))  )
      {
        const auto prop_vector = get_properties_from_string_no_norm_cumul<PropertyDescriptor>(ph, name);
        for (const auto & p : prop_vector)
          {
            properties.emplace_back(p);
            properties.back().normalize = false;
            properties.back().cumulative = false;
          }
      }

    return properties;
  }

  // * * * * * * *

  struct cluster_property_descriptor_helper : public base_propriety_descriptor_helper
  {
    cluster_property_descriptor_helper() = default;

    template <class Property>
    cluster_property_descriptor_helper(const Property &)
    {
      name = Property::filename();
      axis = Property::axisname(),
      unit = Property::unit(),
      title = Property::propername(),
      is_single = !Property::is_combined();
      is_combined = Property::is_combined();
      normalize = true;
      cumulative = Property::do_cumulative();

      auto store_lambda = [](auto && ... args)
      {
        return std::make_any<typename Property::storage_t>(Property::build_storage(std::forward<decltype(args)>(args)...));
      };

      auto property_lambda = [](const std::any & store, auto && ... args)
      {
        const typename Property::storage_t * store_ptr = std::any_cast<typename Property::storage_t>(&store);
        return Property::get_property(*store_ptr, std::forward<decltype(args)>(args)...);
      };

      if constexpr (Property::is_combined())
        {
          build_store = std::function<build_store_func_combined_t>(store_lambda);
          get_property = std::function<get_property_func_combined_t>(property_lambda);
        }
      else
        {
          build_store = std::function<build_store_func_single_t>(store_lambda);
          get_property = std::function<get_property_func_single_t>(property_lambda);
        }
    }

  };

  static std::vector<cluster_property_descriptor_helper> get_cluster_properties_from_string(const std::string & name, const bool consider_norm, const bool consider_cumul)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CLUSTER PROP FROM STR: " << name << std::endl;
#endif
    const auto ret =  get_properties_from_string<cluster_property_descriptor_helper>
                      (ClusterPlotsDefinitions::ClusterProperties{}, name, consider_norm, consider_cumul);
    return ret;
  }

  static void build_cluster_single_plot(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                        const std::string & name,
                                        const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
    if (!f.plot_1D)
      {
        return;
      }

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CLUSTER SINGLE: " << name << std::endl;
#endif
    auto create_plot = [&](const auto & p,
                           auto & plot)
    {
      plot.plot_name = std::string("clusters_") + p.name + (p.normalize ? "_norm" : p.cumulative ? "_cumul" : "");
      plot.axis_x = p.axis + " " + p.unit;
      plot.axis_y = p.normalize ? "Average Number of Clusters Per Event" :
                    p.cumulative ? "Fraction of Clusters With Greater " + p.axis : "Clusters";
      plot.log_y = true;
      if (p.normalize)
        {
          plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
        }
      else if (p.cumulative)
        {
          plot.cumulative = -1;
        }

      ret.emplace_back(plot.clone());
    };

    auto properties = get_cluster_properties_from_string(name, true, true);

    for (const auto & p : properties)
      {
        if (p.is_combined && f.plot_combined)
          {
            CALORECGPU_PLOTHISTTYPE(1, combined) plot;

            auto build_store = std::get<std::function<build_store_func_combined_t>>(p.build_store);
            auto get_prop = std::get<std::function<get_property_func_combined_t>>(p.get_property);

            plot.f = [build_store, get_prop] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
            {
              auto store = build_store(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
              for (int ref_cluster = 0; ref_cluster < cluster_info_1.number; ++ref_cluster)
                {
                  const int test_cluster = sch.r2t(ref_cluster);
                  if (test_cluster >= 0)
                    {
                      plt->Fill(get_prop( store, cell_state_1, cluster_info_1, cluster_moments_1, ref_cluster,
                                          cell_state_2, cluster_info_2, cluster_moments_2, test_cluster ));
                    }
                }
            };

            plot.title = std::string("Matched Clusters ") + p.title + " Distribution";

            create_plot(p, plot);
          }
        else if (!p.is_combined && f.plot_single)
          {
            CALORECGPU_PLOTHISTTYPE(1, single) plot;

            auto build_store = std::get<std::function<build_store_func_single_t>>(p.build_store);
            auto get_prop = std::get<std::function<get_property_func_single_t>>(p.get_property);

            plot.f = [build_store, get_prop] CALORECGPU_SINGLE_LAMBDA(hist_1D_type)
            {
              auto store = build_store(cdh, cell_info, cell_state, cluster_info, cluster_moments);

              for (int cluster = 0; cluster < cluster_info.number; ++cluster)
                {
                  plt->Fill(get_prop(store, cell_state, cluster_info, cluster_moments, cluster));
                }
            };

            plot.title = std::string("Cluster ") + p.title + " Distribution";

            create_plot(p, plot);
          }
      }
  }

  static void build_cluster_versus_plot(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                        const std::string & prop_1,
                                        const std::string & prop_2,
                                        const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
    if (!f.plot_2D)
      {
        return;
      }
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CLUSTER VERSUS: " << prop_1 << " | " << prop_2 << std::endl;
#endif
    auto create_plot = [&](const auto & p1, const auto & p2,
                           auto && plot,
                           auto && f)
    {
      plot.plot_name = std::string("clusters_") + p1.name + "_versus_" + p2.name + (p2.normalize ? "_norm" : "");
      plot.axis_x = p1.axis + " " + p1.unit;
      plot.axis_y = p2.axis + " " + p2.unit;
      plot.axis_z = p2.normalize ? "Average Number of Clusters Per Event" : "Clusters";
      plot.log_z = true;
      plot.title = std::string("Cluster ") + p1.title + " versus " + p2.title;
      plot.f = std::forward<decltype(f)>(f);
      if (p2.normalize)
        {
          plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
        }
      ret.emplace_back(plot.clone());
    };

    auto func_for_single = [&](const auto & p1, const auto & p2)
    {
      auto build_store_1 = std::get<std::function<build_store_func_single_t>>(p1.build_store);
      auto build_store_2 = std::get<std::function<build_store_func_single_t>>(p2.build_store);
      auto get_prop_1 = std::get<std::function<get_property_func_single_t>>(p1.get_property);
      auto get_prop_2 = std::get<std::function<get_property_func_single_t>>(p2.get_property);
      return [build_store_1, build_store_2, get_prop_1, get_prop_2] CALORECGPU_SINGLE_LAMBDA(hist_2D_type)
      {
        auto store_1 = build_store_1(cdh, cell_info, cell_state, cluster_info, cluster_moments);
        auto store_2 = build_store_2(cdh, cell_info, cell_state, cluster_info, cluster_moments);
        for (int i = 0; i < cluster_info.number; ++i)
          {
            plt->Fill(get_prop_1(store_1, cell_state, cluster_info, cluster_moments, i), get_prop_2(store_2, cell_state, cluster_info, cluster_moments, i));
          }
      };
    };

    auto func_for_combined = [&](const auto & p1, const auto & p2)
    {
      auto build_store_1 = std::get<std::function<build_store_func_combined_t>>(p1.build_store);
      auto build_store_2 = std::get<std::function<build_store_func_combined_t>>(p2.build_store);
      auto get_prop_1 = std::get<std::function<get_property_func_combined_t>>(p1.get_property);
      auto get_prop_2 = std::get<std::function<get_property_func_combined_t>>(p2.get_property);
      return [build_store_1, build_store_2, get_prop_1, get_prop_2] CALORECGPU_COMBINED_LAMBDA(hist_2D_type)
      {
        auto store_1 = build_store_1(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
        auto store_2 = build_store_2(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
        for (int ref_cluster = 0; ref_cluster < cluster_info_1.number; ++ref_cluster)
          {
            const int test_cluster = sch.r2t(ref_cluster);
            if (test_cluster >= 0)
              {
                plt->Fill(get_prop_1(store_1, cell_state_1, cluster_info_1, cluster_moments_1, ref_cluster, cell_state_2, cluster_info_2, cluster_moments_2, test_cluster),
                          get_prop_2(store_2, cell_state_1, cluster_info_1, cluster_moments_1, ref_cluster, cell_state_2, cluster_info_2, cluster_moments_2, test_cluster));
              }
          }
      };
    };

    const auto first = get_cluster_properties_from_string(prop_1, false, false);
    const auto second = get_cluster_properties_from_string(prop_2, true, false);

    for (const auto & p1 : first)
      {
        for (const auto & p2 : second)
          {
            if (p1.is_combined && p2.is_combined && f.plot_combined)
              {
                create_plot(p1, p2, CALORECGPU_PLOTHISTTYPE(2, combined) {}, func_for_combined(p1, p2));
              }
            else if (!p1.is_combined && !p2.is_combined && f.plot_single)
              {
                create_plot(p1, p2, CALORECGPU_PLOTHISTTYPE(2, single) {}, func_for_single(p1, p2));
              }
          }
      }
  }

  static void get_cluster_plot_descriptions(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                            const std::string & name,
                                            const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CLUSTER GENERAL: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }

    const auto find_versus = get_earliest_possible_match(name, "_versus_");

    if (find_versus.was_found())
      {
        build_cluster_versus_plot(ret, find_versus.before(name), find_versus.after(name), f);
      }

    if (!find_versus.was_found_exactly("_versus_"))
      {
        build_cluster_single_plot(ret, name, f);
      }
  }

  // * * * * * * *

  static void build_unmatched_ref_plot(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                       const std::string & prop,
                                       const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "UNMATCHED REF: " << prop << std::endl;
#endif
    const auto properties = get_cluster_properties_from_string(prop, true, true);

    for (const auto & p : properties)
      {
        if (p.is_combined || !f.plot_single || !f.plot_1D)
          {
            continue;
          }
        CALORECGPU_PLOTHISTTYPE(1, combined) plot;
        plot.plot_name = std::string("unmatched_ref_") + p.name + (p.normalize ? "_norm" : p.cumulative ? "_cumul" : "");
        plot.axis_x = p.axis + " " + p.unit;
        plot.axis_y = p.normalize ? "Average Number of Clusters Per Event" :
                      p.cumulative ? "Fraction of Unmatched Clusters With Greater " + p.axis : "Clusters";
        plot.title = std::string("Unmatched Reference Clusters ") + p.title + " Distribution";
        plot.log_y = true;
        auto build_store = std::get<std::function<build_store_func_single_t>>(p.build_store);
        auto get_prop = std::get<std::function<get_property_func_single_t>>(p.get_property);
        plot.f = [build_store, get_prop] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
        {
          auto store = build_store(cdh, cell_info, cell_state_1, cluster_info_1, cluster_moments_1);
          for (int ref_cluster = 0; ref_cluster < cluster_info_1.number; ++ref_cluster)
            {
              const int test_cluster = sch.r2t(ref_cluster);
              if (test_cluster < 0)
                {
                  plt->Fill(get_prop(store, cell_state_1, cluster_info_1, cluster_moments_1, ref_cluster));
                }
            }
        };
        if (p.normalize)
          {
            plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
          }
        else if (p.cumulative)
          {
            plot.cumulative = -1;
          }
        ret.emplace_back(plot.clone());
      }
  }

  static void build_unmatched_test_plot(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                        const std::string & prop,
                                        const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "UNMATCHED TEST: " << prop << std::endl;
#endif
    const auto properties = get_cluster_properties_from_string(prop, true, true);

    for (const auto & p : properties)
      {
        if (p.is_combined || !f.plot_single || !f.plot_1D)
          {
            continue;
          }
        CALORECGPU_PLOTHISTTYPE(1, combined) plot;
        plot.plot_name = std::string("unmatched_test_") + p.name + (p.normalize ? "_norm" : p.cumulative ? "_cumul" : "");
        plot.axis_x = p.axis + " " + p.unit;
        plot.axis_y = p.normalize ? "Average Number of Clusters Per Event" :
                      p.cumulative ? "Fraction of Unmatched Clusters With Greater " + p.axis : "Clusters";
        plot.title = std::string("Unmatched Test Clusters ") + p.title + " Distribution";
        plot.log_y = true;
        auto build_store = std::get<std::function<build_store_func_single_t>>(p.build_store);
        auto get_prop = std::get<std::function<get_property_func_single_t>>(p.get_property);
        plot.f = [build_store, get_prop] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
        {
          auto store = build_store(cdh, cell_info, cell_state_2, cluster_info_2, cluster_moments_2);
          for (int test_cluster = 0; test_cluster < cluster_info_2.number; ++test_cluster)
            {
              const int ref_cluster = sch.t2r(test_cluster);
              if (ref_cluster < 0)
                {
                  plt->Fill(get_prop(store, cell_state_2, cluster_info_2, cluster_moments_2, test_cluster));
                }
            }
        };
        if (p.normalize)
          {
            plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
          }
        else if (p.cumulative)
          {
            plot.cumulative = -1;
          }
        ret.emplace_back(plot.clone());
      }
  }

  static void build_unmatched_both_plot(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                        const std::string & prop,
                                        const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "UNMATCHED BOTH: " << prop << std::endl;
#endif
    const auto properties = get_cluster_properties_from_string(prop, true, true);

    for (const auto & p : properties)
      {
        if (p.is_combined || !f.plot_single || !f.plot_1D)
          {
            continue;
          }
        CALORECGPU_PLOTHISTTYPE(1, combined) plot;
        plot.plot_name = std::string("unmatched_both_") + p.name + (p.normalize ? "_norm" : p.cumulative ? "_cumul" : "");
        plot.axis_x = p.axis + " " + p.unit;
        plot.axis_y = p.normalize ? "Average Number of Clusters Per Event" :
                      p.cumulative ? "Fraction of Unmatched Clusters With Greater " + p.axis : "Clusters";
        plot.title = std::string("Unmatched Clusters ") + p.title + " Distribution";
        plot.log_y = true;
        auto build_store = std::get<std::function<build_store_func_single_t>>(p.build_store);
        auto get_prop = std::get<std::function<get_property_func_single_t>>(p.get_property);
        plot.f = [build_store, get_prop] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
        {
          auto store_1 = build_store(cdh, cell_info, cell_state_1, cluster_info_1, cluster_moments_1);
          auto store_2 = build_store(cdh, cell_info, cell_state_2, cluster_info_2, cluster_moments_2);
          for (int ref_cluster = 0; ref_cluster < cluster_info_1.number; ++ref_cluster)
            {
              const int test_cluster = sch.r2t(ref_cluster);
              if (test_cluster < 0)
                {
                  plt->Fill(get_prop(store_1, cell_state_1, cluster_info_1, cluster_moments_1, ref_cluster));
                }
            }
          for (int test_cluster = 0; test_cluster < cluster_info_2.number; ++test_cluster)
            {
              const int ref_cluster = sch.t2r(test_cluster);
              if (ref_cluster < 0)
                {
                  plt->Fill(get_prop(store_2, cell_state_2, cluster_info_2, cluster_moments_2, test_cluster));
                }
            }
        };
        if (p.normalize)
          {
            plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
          }
        else if (p.cumulative)
          {
            plot.cumulative = -1;
          }
        ret.emplace_back(plot.clone());
      }
  }

  static void get_unmatched_plot_descriptions(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                              const std::string & name,
                                              const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "UNMATCHED: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }
    const bool asterisk_afterward = name[name.size() - 1] == '*';

    const auto first_pos = name.find("_");
    std::string first_part;
    if (first_pos >= name.size() - 1)
      {
        if (asterisk_afterward && name.size() > 2)
          {
            const std::string non_asterisk_name = name.substr(0, name.size() - 1);
            //The asterisk in the end!
            if (std::string("ref").find(non_asterisk_name) != std::string::npos)
              {
                first_part = "r*";
              }
            else if (std::string("test").find(non_asterisk_name) != std::string::npos)
              {
                first_part = "t*";
              }
            else if (std::string("both").find(non_asterisk_name) != std::string::npos)
              {
                first_part = "b*";
              }
            else
              {
                return;
              }
          }
        else if (asterisk_afterward)
          //name[0] is either * or a letter,
          //so we can match directly through name.
          {
            first_part = name;
          }
        else
          {
            return;
          }
      }
    else
      {
        first_part = name.substr(0, first_pos);
      }

    if (first_part == "*" || first_part == "r*")
      {
        build_unmatched_ref_plot(ret, "*", f);
      }
    else if (first_part == "ref")
      {
        build_unmatched_ref_plot(ret, name.substr(first_pos + 1), f);
      }

    if (first_part == "*" || first_part == "t*")
      {
        build_unmatched_test_plot(ret, "*", f);
      }
    else if (first_part == "test")
      {
        build_unmatched_test_plot(ret, name.substr(first_pos + 1), f);
      }


    if (first_part == "*" || first_part == "b*")
      {
        build_unmatched_both_plot(ret, "*", f);
      }
    else if (first_part == "both")
      {
        build_unmatched_both_plot(ret, name.substr(first_pos + 1), f);
      }

  }

  // * * * * * * *

  struct cell_property_descriptor_helper : public base_propriety_descriptor_helper
  {
    cell_property_descriptor_helper() = default;

    template <class Property>
    cell_property_descriptor_helper(const Property &)
    {
      name = Property::filename();
      axis = Property::axisname(),
      unit = Property::unit(),
      title = Property::propername(),
      is_single = Property::can_be_single();
      is_combined = Property::can_be_combined();
      normalize = true;
      cumulative = Property::do_cumulative();

      auto store_lambda = [](auto && ... args)
      {
        return std::make_any<typename Property::storage_t>(Property::build_storage(std::forward<decltype(args)>(args)...));
      };

      auto property_lambda = [](const std::any & store, auto && ... args)
      {
        const typename Property::storage_t * store_ptr = std::any_cast<typename Property::storage_t>(&store);
        return Property::get_property(*store_ptr, std::forward<decltype(args)>(args)...);
      };

      if constexpr (Property::can_be_single() && Property::can_be_combined())
        {
          build_store = std::function<build_store_func_both_t>(store_lambda);
          get_property = std::function<get_property_func_both_t>(property_lambda);
        }
      else if constexpr (Property::can_be_combined())
        {
          build_store = std::function<build_store_func_combined_t>(store_lambda);
          get_property = std::function<get_property_func_combined_t>(property_lambda);
        }
      else if constexpr (Property::can_be_single())
        {
          build_store = std::function<build_store_func_single_t>(store_lambda);
          get_property = std::function<get_property_func_single_t>(property_lambda);
        }
    }

  };

  using filter_func_both_t = bool(const std::any &,
                                  const CaloRecGPU::CellInfoArr &,
                                  const int);

  using filter_func_single_t = double(const std::any &,
                                      const CaloRecGPU::CellInfoArr &,
                                      const CaloRecGPU::CellStateArr &,
                                      const int);

  using filter_func_combined_t = double(const std::any &,
                                        const CaloRecGPU::CellInfoArr &,
                                        const CaloRecGPU::CellStateArr &,
                                        const CaloRecGPU::CellStateArr &,
                                        const int);

  struct cell_type_descriptor_helper
  {
    std::string name, axis, title;

    bool is_single = false;
    bool is_combined = false;

    bool normalize = false;
    bool cumulative = false;

    std::variant< std::function<build_store_func_single_t>,
        std::function<build_store_func_combined_t>,
        std::function<build_store_func_both_t> > build_store;

    std::variant<std::function<filter_func_single_t>,
        std::function<filter_func_combined_t>,
        std::function<filter_func_both_t>> filter;


    cell_type_descriptor_helper() = default;

    template <class Type>
    cell_type_descriptor_helper(const Type &)
    {
      name = Type::filename();
      axis = Type::axisname(),
      title = Type::propername(),
      is_single = Type::can_be_single();
      is_combined = Type::can_be_combined();

      auto store_lambda = [](auto && ... args)
      {
        return std::make_any<typename Type::storage_t>(Type::build_storage(std::forward<decltype(args)>(args)...));
      };

      auto filter_lambda = [](const std::any & store, auto && ... args)
      {
        const typename Type::storage_t * store_ptr = std::any_cast<typename Type::storage_t>(&store);
        return Type::filter(*store_ptr, std::forward<decltype(args)>(args)...);
      };

      if constexpr (Type::can_be_single() && Type::can_be_combined())
        {
          build_store = std::function<build_store_func_both_t>(store_lambda);
          filter = std::function<filter_func_both_t>(filter_lambda);
        }
      else if constexpr (Type::can_be_combined())
        {
          build_store = std::function<build_store_func_combined_t>(store_lambda);
          filter = std::function<filter_func_combined_t>(filter_lambda);
        }
      else if (Type::can_be_single())
        {
          build_store = std::function<build_store_func_single_t>(store_lambda);
          filter = std::function<filter_func_single_t>(filter_lambda);
        }
    }

    std::function<build_store_func_single_t> encapsulate_store_single() const
    {
      if (!is_combined)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<build_store_func_single_t>>(build_store);
        }
      else
        {
          auto both_store = std::get<std::function<build_store_func_both_t>>(build_store);
          return [both_store](const ConstantDataHolder & cdh,
                              const CaloRecGPU::CellInfoArr & cell_info,
                              const CaloRecGPU::CellStateArr &,
                              const CaloRecGPU::ClusterInfoArr &,
                              const CaloRecGPU::ClusterMomentsArr &)
          {
            return both_store(cdh, cell_info);
          };
        }
    }

    std::function<build_store_func_combined_t> encapsulate_store_combined() const
    {
      if (!is_single)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<build_store_func_combined_t>>(build_store);
        }
      else
        {
          auto both_store = std::get<std::function<build_store_func_both_t>>(build_store);
          return [both_store](const ConstantDataHolder & cdh,
                              const CaloRecGPU::CellInfoArr & cell_info,
                              const CaloRecGPU::CellStateArr &,
                              const CaloRecGPU::CellStateArr &,
                              const CaloRecGPU::ClusterInfoArr &,
                              const CaloRecGPU::ClusterInfoArr &,
                              const CaloRecGPU::ClusterMomentsArr &,
                              const CaloRecGPU::ClusterMomentsArr &,
                              const CaloPlotterHelper::sample_comparisons_holder & )
          {
            return both_store(cdh, cell_info);
          };
        }
    }

    std::function<filter_func_single_t> encapsulate_filter_single() const
    {
      if (!is_combined)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<filter_func_single_t>>(filter);
        }
      else
        {
          auto both_filter = std::get<std::function<filter_func_both_t>>(filter);
          return [both_filter](const std::any & store,
                               const CaloRecGPU::CellInfoArr & cell_info,
                               const CaloRecGPU::CellStateArr &,
                               const int index)
          {
            return both_filter(store, cell_info, index);
          };
        }
    }

    std::function<filter_func_combined_t> encapsulate_filter_combined() const
    {
      if (!is_single)
        //We assume the conversion below is valid...
        {
          return std::get<std::function<filter_func_combined_t>>(filter);
        }
      else
        {
          auto both_filter = std::get<std::function<filter_func_both_t>>(filter);
          return [both_filter](const std::any & store,
                               const CaloRecGPU::CellInfoArr & cell_info,
                               const CaloRecGPU::CellStateArr &,
                               const CaloRecGPU::CellStateArr &,
                               const int index)
          {
            return both_filter(store, cell_info, index);
          };
        }
    }

  };

  static std::vector<cell_property_descriptor_helper> get_cell_properties_from_string(const std::string & name,
                                                                                      const bool consider_norm,
                                                                                      const bool consider_cumul)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL PROPERTY FROM STR: " << name << std::endl;
#endif
    const auto ret = get_properties_from_string<cell_property_descriptor_helper>
                     (CellPlotsDefinitions::CellProperties{}, name, consider_norm, consider_cumul);
    return ret;
  }

  static std::vector<cell_type_descriptor_helper> get_cell_types_from_string(const std::string & name,
                                                                             const bool consider_norm,
                                                                             const bool consider_cumul)
  {
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL TYPE FROM STR: " << name << std::endl;
#endif

    std::vector<cell_type_descriptor_helper> types;

    if (name.size() == 0)
      {
        return types;
      }

    auto find_norm = get_latest_possible_match_with_suffix(name, "_norm");
    auto find_cumul = get_latest_possible_match_with_suffix(name, "_cumul");

    const std::string after_norm = find_norm.after("_norm");
    const std::string after_cumul = find_cumul.after("_cumul");

    if (consider_norm && find_norm.was_found() && (after_norm == "" || after_norm == "*"))
      {
        const auto other_vector = match_classes_by_type<cell_type_descriptor_helper>
                                  (find_norm.before(name), CellPlotsDefinitions::CellTypes{});
        for (const auto & t : other_vector)
          {
            types.emplace_back(t);
            types.back().normalize = true;
            types.back().cumulative = false;
          }
      }

    if (consider_cumul && find_cumul.was_found() && (after_cumul == "" || after_cumul == "*"))
      {
        const auto other_vector = match_classes_by_type<cell_type_descriptor_helper>
                                  (find_cumul.before(name), CellPlotsDefinitions::CellTypes{});
        for (const auto & t : other_vector)
          {
            types.emplace_back(t);
            types.back().normalize = false;
            types.back().cumulative = true;
          }
      }

    if ((!consider_norm || !find_norm.was_found_exactly("_norm")) &&
        (!consider_cumul || !find_cumul.was_found_exactly("_cumul"))  )
      {
        const auto type_vector = match_classes_by_type<cell_type_descriptor_helper>(name, CellPlotsDefinitions::CellTypes{});

        for (const auto & t : type_vector)
          {
            types.emplace_back(t);
            types.back().normalize = false;
            types.back().cumulative = false;
          }
      }

    return types;
  }

  static void build_cell_count_plots(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                     const std::string & name,
                                     const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
    if (!f.plot_1D)
      {
        return;
      }

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL COUNT: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }

    auto create_plot = [&](const auto & t,
                           auto & plot)
    {
      plot.plot_name = std::string("cells_count_") + t.name + (t.normalize ? "_norm" : t.cumulative ? "_cumul" : "");
      plot.axis_x = std::string("# of ") + t.axis + " Cells";
      plot.axis_y = t.normalize ? "Fraction of Events" :
                    t.cumulative ? "Fraction of Events With Greater Count" : "Events";
      plot.title = t.title + " Cells Counts";
      plot.log_y = true;
      if (t.normalize)
        {
          plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
        }
      else if (t.cumulative)
        {
          plot.cumulative = -1;
        }

      ret.emplace_back(plot.clone());
    };

    auto types = get_cell_types_from_string(name, true, true);

    for (const auto & t : types)
      {
        if (t.is_single && f.plot_single)
          //Types that those that can be both single and not are treated as single.
          {
            CALORECGPU_PLOTHISTTYPE(1, single) plot;
            auto build_store = t.encapsulate_store_single();
            auto filter = t.encapsulate_filter_single();
            plot.f = [build_store, filter] CALORECGPU_SINGLE_LAMBDA(hist_1D_type)
            {
              auto store_type = build_store(cdh, cell_info, cell_state, cluster_info, cluster_moments);
              int count = 0;
              for (int i = 0; i < NCaloCells; ++i)
                {
                  if (!cell_info.is_valid(i))
                    {
                      continue;
                    }
                  if (filter(store_type, cell_info, cell_state, i))
                    {
                      ++count;
                    }
                }
              plt->Fill((double) count);
            };
            create_plot(t, plot);
          }
        else if (t.is_combined && f.plot_combined)
          {
            CALORECGPU_PLOTHISTTYPE(1, combined) plot;
            auto build_store = t.encapsulate_store_combined();
            auto filter = t.encapsulate_filter_combined();
            plot.f = [build_store, filter] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
            {
              auto store_type = build_store(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
              int count = 0;
              for (int i = 0; i < NCaloCells; ++i)
                {
                  if (!cell_info.is_valid(i))
                    {
                      continue;
                    }
                  if (filter(store_type, cell_info, cell_state_1, cell_state_2, i))
                    {
                      ++count;
                    }
                }
              plt->Fill((double) count);
            };
            create_plot(t, plot);
          }
      }
  }

  //NOTE: Here we only consider strictly single plots.
  //      Those that can be both do not differ from tool to tool,
  //      and thus the deltas would be always zero,
  //      which does not provide any useful information...
  static void build_cell_delta_plots(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                     const std::string & name,
                                     const CaloPlotterHelper::GetPlotDescriptionsFilter & f)

  {
    if (!f.plot_1D)
      {
        return;
      }

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL DELTA: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }

    auto create_plot = [&](const auto & t,
                           auto & plot)
    {
      plot.plot_name = std::string("cells_delta_") + t.name + (t.normalize ? "_norm" : "");
      plot.axis_x = std::string("Difference in # of ") + t.axis + " Cells";
      plot.axis_y = t.normalize ? "Fraction of Events" : "Events";
      plot.title = t.title + " Cells Differences";
      plot.log_y = true;
      if (t.normalize)
        {
          plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
        }
      ret.emplace_back(plot.clone());
    };

    auto types = get_cell_types_from_string(name, true, false);

    for (const auto & t : types)
      {
        if (t.is_single && !t.is_combined && f.plot_single)
          {
            CALORECGPU_PLOTHISTTYPE(1, combined) plot;
            auto build_store = std::get<std::function<build_store_func_single_t>>(t.build_store);
            auto filter = std::get<std::function<filter_func_single_t>>(t.filter);
            plot.f = [build_store, filter] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
            {
              auto store_type_1 = build_store(cdh, cell_info, cell_state_1, cluster_info_1, cluster_moments_1);
              auto store_type_2 = build_store(cdh, cell_info, cell_state_2, cluster_info_2, cluster_moments_2);
              int count_1 = 0;
              int count_2 = 0;
              for (int i = 0; i < NCaloCells; ++i)
                {
                  if (!cell_info.is_valid(i))
                    {
                      continue;
                    }
                  if (filter(store_type_1, cell_info, cell_state_1, i))
                    {
                      ++count_1;
                    }
                  if (filter(store_type_2, cell_info, cell_state_2, i))
                    {
                      ++count_2;
                    }
                }
              plt->Fill((double) count_2 - count_1);
            };
            create_plot(t, plot);
          }
      }
  }

  static void build_cell_single_plot(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                     const std::string & name,
                                     const std::vector<cell_type_descriptor_helper> & types,
                                     const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
    if (!f.plot_1D)
      {
        return;
      }

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL SINGLE: " << name << std::endl;
#endif
    auto create_plot = [&](const auto & t,
                           const auto & p,
                           auto & plot)
    {
      plot.plot_name = std::string("cells_") + t.name + "_hist_" + p.name + (p.normalize ? "_norm" : p.cumulative ? "_cumul" : "");
      plot.axis_x = p.axis + " " + p.unit;
      plot.axis_y = p.normalize ? "Average Number of Cells Per Event" :
                    p.cumulative ? "Fraction of Cells With Greater " + p.axis : "Cells";
      plot.title = t.title + " Cells " + p.title + " Distribution";
      plot.log_y = true;
      if (p.normalize)
        {
          plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
        }
      else if (p.cumulative)
        {
          plot.cumulative = -1;
        }

      ret.emplace_back(plot.clone());
    };

    auto properties = get_cell_properties_from_string(name, true, true);

    for (const auto & t : types)
      {
        for (const auto & p : properties)
          {
            if (t.is_single && p.is_single && f.plot_single)
              {
                CALORECGPU_PLOTHISTTYPE(1, single) plot;

                auto build_store_type = t.encapsulate_store_single();
                auto filter = t.encapsulate_filter_single();

                auto build_store_prop = p.encapsulate_store_single();
                auto get_prop = p.encapsulate_get_single();

                plot.f = [build_store_type, build_store_prop, filter, get_prop] CALORECGPU_SINGLE_LAMBDA(hist_1D_type)
                {
                  auto store_type = build_store_type(cdh, cell_info, cell_state, cluster_info, cluster_moments);
                  auto store_prop = build_store_prop(cdh, cell_info, cell_state, cluster_info, cluster_moments);
                  for (int i = 0; i < NCaloCells; ++i)
                    {
                      if (!cell_info.is_valid(i))
                        {
                          continue;
                        }
                      if (filter(store_type, cell_info, cell_state, i))
                        {
                          plt->Fill(get_prop(store_prop, cell_state, cluster_info, cluster_moments, i));
                        }
                    }
                };
                create_plot(t, p, plot);
              }
            else if (t.is_combined && p.is_combined && f.plot_combined)
              {
                CALORECGPU_PLOTHISTTYPE(1, combined) plot;

                auto build_store_type = t.encapsulate_store_combined();
                auto filter = t.encapsulate_filter_combined();

                auto build_store_prop = p.encapsulate_store_combined();
                auto get_prop = p.encapsulate_get_combined();

                plot.f = [build_store_type, build_store_prop, filter, get_prop] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
                {
                  auto store_type = build_store_type(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
                  auto store_prop = build_store_prop(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
                  for (int i = 0; i < NCaloCells; ++i)
                    {
                      if (!cell_info.is_valid(i))
                        {
                          continue;
                        }
                      if (filter(store_type, cell_info, cell_state_1, cell_state_2, i))
                        {
                          plt->Fill(get_prop(store_prop, cell_state_1, cluster_info_1, cluster_moments_1, i, cell_state_2, cluster_info_2, cluster_moments_2, i));
                        }
                    }
                };
                create_plot(t, p, plot);
              }
          }
      }
  }

  static void build_cell_versus_plot(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                     const std::string & prop_1,
                                     const std::string & prop_2,
                                     const std::vector<cell_type_descriptor_helper> & types,
                                     const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
    if (!f.plot_2D)
      {
        return;
      }
#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL VERSUS: " << prop_1 << " | " << prop_2 << std::endl;
#endif
    auto create_plot = [&](const auto & t,
                           const auto & p1,
                           const auto & p2,
                           auto & plot)
    {
      plot.plot_name = std::string("cells_") + t.name + "_hist_" + p1.name + "_versus_" + p2.name + (p2.normalize ? "_norm" : "");
      plot.axis_x = p1.axis + " " + p1.unit;
      plot.axis_y = p2.axis + " " + p2.unit;
      plot.axis_z = p2.normalize ? "Average Number of Cells Per Event" : "Cells";
      plot.log_z = true;
      plot.title = t.title + " Cells " + p1.title + " versus " + p2.title;

      if (p2.normalize)
        {
          plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
        }

      ret.emplace_back(plot.clone());
    };

    auto properties_1 = get_cell_properties_from_string(prop_1, false, false);
    auto properties_2 = get_cell_properties_from_string(prop_2, true, false);

    for (const auto & t : types)
      {
        for (const auto & p1 : properties_1)
          {
            for (const auto & p2 : properties_2)
              {
                //Yes. A three level for.
                //Anyone want to shout "combinatorial explosion"?

                if (t.is_single && p1.is_single && p2.is_single && f.plot_single)
                  {
                    CALORECGPU_PLOTHISTTYPE(2, single) plot;

                    auto build_store_type = t.encapsulate_store_single();
                    auto filter = t.encapsulate_filter_single();

                    auto build_store_prop_1 = p1.encapsulate_store_single();
                    auto get_prop_1 = p1.encapsulate_get_single();
                    auto build_store_prop_2 = p2.encapsulate_store_single();
                    auto get_prop_2 = p2.encapsulate_get_single();

                    plot.f = [build_store_type, build_store_prop_1, build_store_prop_2,
                                                filter, get_prop_1, get_prop_2]
                    CALORECGPU_SINGLE_LAMBDA(hist_2D_type)
                    {
                      auto store_type = build_store_type(cdh, cell_info, cell_state, cluster_info, cluster_moments);
                      auto store_prop_1 = build_store_prop_1(cdh, cell_info, cell_state, cluster_info, cluster_moments);
                      auto store_prop_2 = build_store_prop_2(cdh, cell_info, cell_state, cluster_info, cluster_moments);
                      for (int i = 0; i < NCaloCells; ++i)
                        {
                          if (!cell_info.is_valid(i))
                            {
                              continue;
                            }
                          if (filter(store_type, cell_info, cell_state, i))
                            {
                              plt->Fill( get_prop_1(store_prop_1, cell_state, cluster_info, cluster_moments, i),
                                         get_prop_2(store_prop_2, cell_state, cluster_info, cluster_moments, i)   );
                            }
                        }
                    };

                    create_plot(t, p1, p2, plot);
                  }
                else if (t.is_combined && p1.is_combined && p2.is_combined && f.plot_combined)
                  {
                    CALORECGPU_PLOTHISTTYPE(2, combined) plot;

                    auto build_store_type = t.encapsulate_store_combined();
                    auto filter = t.encapsulate_filter_combined();

                    auto build_store_prop_1 = p1.encapsulate_store_combined();
                    auto get_prop_1 = p1.encapsulate_get_combined();
                    auto build_store_prop_2 = p2.encapsulate_store_combined();
                    auto get_prop_2 = p2.encapsulate_get_combined();

                    plot.f = [build_store_type, build_store_prop_1, build_store_prop_2,
                                                filter, get_prop_1, get_prop_2]
                    CALORECGPU_COMBINED_LAMBDA(hist_2D_type)
                    {
                      auto store_type = build_store_type(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
                      auto store_prop_1 = build_store_prop_1(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
                      auto store_prop_2 = build_store_prop_2(cdh, cell_info, cell_state_1, cell_state_2, cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, sch);
                      for (int i = 0; i < NCaloCells; ++i)
                        {
                          if (!cell_info.is_valid(i))
                            {
                              continue;
                            }
                          if (filter(store_type, cell_info, cell_state_1, cell_state_2, i))
                            {
                              plt->Fill( get_prop_1(store_prop_1, cell_state_1, cluster_info_1, cluster_moments_1, i, cell_state_2, cluster_info_2, cluster_moments_2, i),
                                         get_prop_2(store_prop_2, cell_state_1, cluster_info_1, cluster_moments_1, i, cell_state_2, cluster_info_2, cluster_moments_2, i)   );
                            }
                        }
                    };
                    create_plot(t, p1, p2, plot);
                  }
              }
          }
      }
  }

  static void get_cell_normal_plots(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                    const std::string & name,
                                    const std::vector<cell_type_descriptor_helper> & types,
                                    const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL NORMAL: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }

    const auto find_versus = get_earliest_possible_match(name, "_versus_");

    if (find_versus.was_found())
      {
        build_cell_versus_plot(ret, find_versus.before(name), find_versus.after(name), types, f);
      }

    if (!find_versus.was_found_exactly("_versus_"))
      //This means there was something like "_ve*"...
      {
        build_cell_single_plot(ret, name, types, f);
      }
  }

  static void get_cell_plot_descriptions(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                         const std::string & name,
                                         const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "CELL PLOT: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }
    const bool asterisk_afterward = name[name.size() - 1] == '*';

    const auto first_pos = name.find("_");
    const std::string to_search = name.substr(0, first_pos);
    const std::string rest = (first_pos < name.size() ?
                              name.substr(first_pos + 1) :
                              asterisk_afterward ? "*" : "");
    const std::string count_str = "count", delta_str = "delta";

    bool do_count = true, do_delta = true, do_none = false;

    for (size_t i = 0;
         i < to_search.size() &&
         i < delta_str.size() &&
         i < count_str.size() &&
         (do_count || do_delta);
         ++i)
      {
        if (name[i] == '*')
          {
            do_none = true;
            break;
          }
        if (name[i] != count_str[i])
          {
            do_count = false;
          }
        if (name[i] != delta_str[i])
          {
            do_delta = false;
          }
        if (!do_count && !do_delta)
          {
            do_none = true;
          }
      }

    if (do_count)
      {
        build_cell_count_plots(ret, rest, f);
      }

    if (do_delta)
      {
        build_cell_delta_plots(ret, rest, f);
      }

    if (do_none)
      {
        const auto find_hist = get_earliest_possible_match(name, "_hist_");

        if (find_hist.was_found())
          {
            get_cell_normal_plots(ret, find_hist.after(name),
                                  get_cell_types_from_string(find_hist.before(name), false, false),
                                  f);
          }
      }
  }

  // * * * * * * *

  static void get_num_plot_descriptions(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                        const std::string & name,
                                        const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
    if (!f.plot_1D)
      {
        return;
      }

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "NUM: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }
    const bool asterisk_afterward = name[name.size() - 1] == '*';
    const std::string actual_name = (asterisk_afterward ? name.substr(0, name.size() - 1) : name);

    auto check_for_plot_type_and_update = [&](const std::string & plot_name,
                                              const std::string & plot_axis,
                                              const std::string & plot_title,
                                              auto && f,
                                              const auto & initial_plot,
                                              const bool var_to_check)
    {
      if (var_to_check && match_with_asterisk(plot_name, actual_name, asterisk_afterward))
        {
          std::decay_t<decltype(initial_plot)> plot;
          plot.plot_name = std::string("num_") + plot_name;
          plot.axis_x = plot_axis;
          plot.axis_y = "# of Events";
          plot.axis_z = "";
          plot.title = plot_title;
          plot.log_y = true;
          plot.f = std::forward<decltype(f)>(f);
          ret.emplace_back(plot.clone());
        }
      if (var_to_check && match_with_asterisk(plot_name + "_norm", actual_name, asterisk_afterward))
        {
          std::decay_t<decltype(initial_plot)> plot;
          plot.plot_name = std::string("num_") + plot_name + "_norm";
          plot.axis_x = plot_axis;
          plot.axis_y = "Fraction of Events";
          plot.axis_z = "";
          plot.title = plot_title;
          plot.log_y = true;
          plot.normalize = CaloPlotterHelper::normalization_constants::NormalizationKind::ByNumberOfEvents;
          plot.f = std::forward<decltype(f)>(f);
          ret.emplace_back(plot.clone());
        }
    };

    check_for_plot_type_and_update("clusters",
                                   "Number of Clusters",
                                   "Number of Clusters per Event",
                                   [] CALORECGPU_SINGLE_LAMBDA(hist_1D_type)
    {
      plt->Fill((double) cluster_info.number);
    },
    CALORECGPU_PLOTHISTTYPE(1, single) {},
    f.plot_single);

    check_for_plot_type_and_update("unmatched_ref_clusters",
                                   "Number of Unmatched Reference Clusters",
                                   "Number of Unmatched Reference Clusters per Event",
                                   [] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
    {
      plt->Fill((double) sch.ref_unmatched());
    },
    CALORECGPU_PLOTHISTTYPE(1, combined) {},
    f.plot_combined );

    check_for_plot_type_and_update("unmatched_test_clusters",
                                   "Number of Unmatched Test Clusters",
                                   "Number of Unmatched Test Clusters per Event",
                                   [] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
    {
      plt->Fill((double) sch.test_unmatched());
    },
    CALORECGPU_PLOTHISTTYPE(1, combined) {},
    f.plot_combined );

    check_for_plot_type_and_update("unmatched_any_clusters",
                                   "Number of Unmatched Clusters",
                                   "Number of Unmatched Clusters per Event",
                                   [] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
    {
      plt->Fill(((double) sch.ref_unmatched()) + ((double) sch.test_unmatched()));
    },
    CALORECGPU_PLOTHISTTYPE(1, combined) {},
    f.plot_combined );


    check_for_plot_type_and_update("delta_clusters",
                                   "Number of Test Clusters - Number of Reference Clusters",
                                   "Difference in the Number of Clusters per Event",
                                   [] CALORECGPU_COMBINED_LAMBDA(hist_1D_type)
    {
      plt->Fill(((double) cluster_info_2.number) - ((double) cluster_info_1.number));
    },
    CALORECGPU_PLOTHISTTYPE(1, combined) {},
    f.plot_combined );
  }

  // * * * * * * *

  static void get_default_plot_descriptions(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & ret,
                                            const std::string & name,
                                            const CaloPlotterHelper::GetPlotDescriptionsFilter & f)
  {
    if (!f.plot_default)
      {
        return;
      }

#if CALORECGPU_PLOTDESCDEBUG
    std::cout << "DEFAULT: " << name << std::endl;
#endif
    if (name.size() == 0)
      {
        return;
      }
    const bool asterisk_afterward = name[name.size() - 1] == '*';

    const auto first_pos = name.find("_");
    std::string first_part;
    if (first_pos >= name.size() - 1)
      {
        if (asterisk_afterward && name.size() > 2)
          {
            const std::string non_asterisk_name = name.substr(0, name.size() - 1);
            //The asterisk in the end!
            if (std::string("clusters").find(non_asterisk_name) != std::string::npos)
              {
                first_part = "cl*";
              }
            else if (std::string("unmatched").find(non_asterisk_name) != std::string::npos)
              {
                first_part = "u*";
              }
            else if (std::string("cells").find(non_asterisk_name) != std::string::npos)
              {
                first_part = "ce*";
              }
            else if (std::string("num").find(non_asterisk_name) != std::string::npos)
              {
                first_part = "n*";
              }
            else
              {
                return;
              }
          }
        else if (asterisk_afterward)
          //name[0] is either * or a letter,
          //so we can match directly through name.
          {
            first_part = name;
          }
        else
          {
            return;
          }
      }
    else
      {
        first_part = name.substr(0, first_pos);
      }

    if (first_part == "*" || first_part == "c*" || first_part == "cl*")
      {
        get_cluster_plot_descriptions(ret, "*", f);
      }
    else if (first_part == "clusters")
      {
        get_cluster_plot_descriptions(ret, name.substr(first_pos + 1), f);
      }

    if (first_part == "*" || first_part == "u*")
      {
        get_unmatched_plot_descriptions(ret, "*", f);
      }
    else if (first_part == "unmatched")
      {
        get_unmatched_plot_descriptions(ret, name.substr(first_pos + 1), f);
      }

    if (first_part == "*" || first_part == "c*" || first_part == "ce*")
      {
        get_cell_plot_descriptions(ret, "*", f);
      }
    else if (first_part == "cells")
      {
        get_cell_plot_descriptions(ret, name.substr(first_pos + 1), f);
      }

    if (first_part == "*" || first_part == "n*")
      {
        get_num_plot_descriptions(ret, "*", f);
      }
    else if (first_part == "num")
      {
        get_num_plot_descriptions(ret, name.substr(first_pos + 1), f);
      }

  }

}

std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> CaloPlotterHelper::get_plot_descriptions(const std::string & name,
                                                                                                       const CaloPlotterHelper::GetPlotDescriptionsFilter & f) const
{
  //const bool prev_add = TH1::AddDirectoryStatus();
  //TH1::AddDirectory(false);

  std::vector<std::unique_ptr<base_plotter>> ret;
  if (s_print_messages)
    {
      std::cout << "INFO: Trying to find plot(s) called '" << name << "'. " << std::endl;
    }

#if CALORECGPU_PLOTDESCDEBUG
  std::cout << "------------------------------------------------------------\nGENERAL: " << name << std::endl;
#endif

  const auto asterisk_pos = name.find("*");
  get_default_plot_descriptions(ret, name.substr(0, asterisk_pos + (asterisk_pos < name.size())), f);

  if (!f.plot_extra)
    {
      //TH1::AddDirectory(prev_add);
      return ret;
    }

  const auto it = m_extra_plot_descriptions.find(name);
  if (it != m_extra_plot_descriptions.end())
    {
      ret.emplace_back(it->second->clone());
    }
  else
    {
      //We currently ignore everything after the asterisk.
      //Maybe update this at a certain later point?
      if (asterisk_pos != std::string::npos)
        {
          const std::string substr = name.substr(0, asterisk_pos);
          auto map_it = m_extra_plot_descriptions.lower_bound(substr);
          if (map_it != m_extra_plot_descriptions.end() && map_it->first.find(substr) == std::string::npos)
            {
              ++map_it;
            }
          for (; map_it != m_extra_plot_descriptions.end() && map_it->first.find(substr) != std::string::npos; ++map_it)
            {
              ret.emplace_back(map_it->second->clone());
            }
        }
    }
  if (s_print_messages)
    {
      std::cout << "Found " << ret.size() << "." << std::endl;
    }

  //TH1::AddDirectory(prev_add);
  return ret;
}

std::vector<std::string> CaloPlotterHelper::get_plot_names(const std::string & name, const GetPlotDescriptionsFilter & filter) const
{
  //I KNOW THIS IS NOT EFFICIENT AT ALL.
  //WHEN I HAVE TIME AND OPPORTUNITY,
  //I WILL REWRITE IT!

  const auto plots = get_plot_descriptions(name, filter);

  std::vector<std::string> ret;
  ret.reserve(plots.size());

  for (const auto & p : plots)
    {
      ret.push_back(p->plot_name);
    }

  return ret;
}