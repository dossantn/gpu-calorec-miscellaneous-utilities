//
// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef CALORECGPU_CALOGPUDEFAULTPLOTTER_H
#define CALORECGPU_CALOGPUDEFAULTPLOTTER_H

#include "AthenaBaseComps/AthAlgTool.h"
#include "CaloRecGPU/CaloClusterGPUTransformers.h"
#include "CaloRecGPU/CUDAFriendlyClasses.h"
#include "CaloRecGPU/Helpers.h"
#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <mutex>
#include <tuple>

#include "StoreGate/ReadCondHandleKey.h"
#include "StoreGate/ReadHandleKey.h"

#include "CaloGPUDefaultPlotterOptions.h"

class CaloPlotterHelper;

class CaloCell_ID;

/**
 * @class CaloGPUDefaultPlotter
 * @author Nuno Fernandes <nuno.dos.santos.fernandes@cern.ch>
 * @date 10 November 2022
 * @brief A plotter that outputs a configurable variety of plots
 *        to allow comparisons between CPU and GPU implementations.
 */

class CaloGPUDefaultPlotter :
  public AthAlgTool, virtual public ICaloClusterGPUPlotter
{
 public:

  CaloGPUDefaultPlotter(const std::string & type, const std::string & name, const IInterface * parent);

  virtual StatusCode initialize() override;

  virtual ~CaloGPUDefaultPlotter();


  virtual StatusCode update_plots_start(const EventContext & ctx,
                                        const CaloRecGPU::ConstantDataHolder & constant_data,
                                        const xAOD::CaloClusterContainer * cluster_collection_ptr) const override;

  virtual StatusCode update_plots_end(const EventContext & ctx,
                                      const CaloRecGPU::ConstantDataHolder & constant_data,
                                      const xAOD::CaloClusterContainer * cluster_collection_ptr) const override;

  virtual StatusCode update_plots(const EventContext & ctx,
                                  const CaloRecGPU::ConstantDataHolder & constant_data,
                                  const xAOD::CaloClusterContainer * cluster_collection_ptr,
                                  const CaloClusterCollectionProcessor * tool) const override;

  virtual StatusCode update_plots(const EventContext & ctx,
                                  const CaloRecGPU::ConstantDataHolder & constant_data,
                                  const xAOD::CaloClusterContainer * cluster_collection_ptr,
                                  const CaloRecGPU::EventDataHolder & event_data,
                                  const ICaloClusterGPUInputTransformer * tool) const override;

  virtual StatusCode update_plots(const EventContext & ctx,
                                  const CaloRecGPU::ConstantDataHolder & constant_data,
                                  const xAOD::CaloClusterContainer * cluster_collection_ptr,
                                  const CaloRecGPU::EventDataHolder & event_data,
                                  const CaloClusterGPUProcessor * tool) const override;

  virtual StatusCode update_plots(const EventContext & ctx,
                                  const CaloRecGPU::ConstantDataHolder & constant_data,
                                  const xAOD::CaloClusterContainer * cluster_collection_ptr,
                                  const CaloRecGPU::EventDataHolder & event_data,
                                  const ICaloClusterGPUOutputTransformer * tool) const override;

  virtual StatusCode do_plots() const override;

 private:

  StatusCode add_data_to_plotter(const EventContext & ctx,
                                 const CaloRecGPU::ConstantDataHolder & constant_data,
                                 const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & cell_info,
                                 const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & cell_state,
                                 const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & clusters,
                                 const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & moments,
                                 const std::string & tool_name) const;

  /*! Returns @p true if this tool should be plotted for.
  */
  bool filter_tool_by_name(const std::string & tool_name) const;

  StatusCode convert_to_GPU_data_structures(const EventContext & ctx,
                                            const CaloRecGPU::ConstantDataHolder & constant_data,
                                            const xAOD::CaloClusterContainer * cluster_collection_ptr,
                                            CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & cell_info,
                                            CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & cell_state,
                                            CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & clusters,
                                            CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & moments) const;

  ///Remove invalid clusters, reorder by ET and update the tags accordingly.
  StatusCode compactify_clusters(const EventContext & ctx,
                                 const CaloRecGPU::ConstantDataHolder & constant_data,
                                 const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & cell_info,
                                 CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & cell_state,
                                 CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & clusters,
                                 CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & moments) const;

  //--------------------------------------------------
  //
  //             GENERAL OPTIONS/SETTINGS
  //
  //--------------------------------------------------

  /**
   * @brief The path specifying the folder to which the plots should be saved.
     Default @p ./plots
   */
  Gaudi::Property<std::string> m_savePath{this, "SavePath", "./plots", "Path to where the plots should be saved"};

  /**
   * @brief The prefix of the saved plots. Empty string by default.
   */
  Gaudi::Property<std::string> m_filePrefix{this, "FilePrefix", "", "Prefix of the saved plots"};

  /**
   * @brief The suffix of the saved plots. Empty string by default.
   */
  Gaudi::Property<std::string> m_fileSuffix{this, "FileSuffix", "", "Suffix of the saved plots"};

  /**
   * @brief Cell (terminal) threshold to use for cluster matching.
   */
  Gaudi::Property<float> m_cellThreshold {this, "CellThreshold", 0., "Cell (terminal) threshold (in units of noise Sigma)"};

  /**
   * @brief Neighbor (growing) threshold to use for cluster matching.
   */
  Gaudi::Property<float> m_neighborThreshold {this, "NeighborThreshold", 2., "Neighbor (grow) threshold (in units of noise Sigma)"};

  /**
   * @brief Seed threshold to use for cluster matching.
   */
  Gaudi::Property<float> m_seedThreshold {this, "SeedThreshold", 4., "Seed threshold (in units of noise Sigma)"};

  /**
  * @brief vector of names of the cell containers to use as input.
  */
  Gaudi::Property<SG::ReadHandleKey<CaloCellContainer>> m_cellsKey {this, "CellsName", "", "Name(s) of Cell Containers"};


  //--------------------------------------------------
  //
  //                    PLOT OPTIONS
  //
  //--------------------------------------------------

  /** @brief Tools to plot individually, with optional extra plots just for each tool.
  */
  Gaudi::Property<std::vector<ToolToPlot>> m_toolsToPlot
  {this, "ToolsToPlot", {}, "Tools to be plotted individually"};
  
  /** @brief Pairs of tools to compare, with optional extra plots just for each combination.
  */
  Gaudi::Property<std::vector<CombinationToPlot>> m_pairsToPlot
  {this, "PairsToPlot", {}, "Pairs of tools to be compared and plotted"};
  
  /** @brief Holds the plots that will be done for every individual tool specified.
  */
  Gaudi::Property<std::vector<PlotDefinition>> m_individualPlots
  {this, "IndividualPlots", {}, "Individual plots to be done for all specified tools"};
  
  /** @brief Holds the plots that will be done for every comparison between pairs of tools specified.
  */
  Gaudi::Property<std::vector<PlotDefinition>> m_comparedPlots
  {this, "ComparedPlots", {}, "Compared plots to be done for all specified pairs of tools"};
  
  /** @brief Plot style to be applied to plots for single tools.
  */
  Gaudi::Property<PlotStyleDefinition> m_individualStyle
  {this, "IndividualPlotStyle", {}, "Plot style to be applied to plots for single tools"};
  
  /** @brief Plot style to be applied to plots for tool comparisons.
  */
  Gaudi::Property<PlotStyleDefinition> m_comparedStyle
  {this, "ComparedPlotStyle", {}, "Plot style to be applied to plots for tool comparisons"};
  
  /** @brief Specifies plots being done with several tools together.
  */
  Gaudi::Property<std::vector<JoinedSinglePlots>> m_joinedIndividualPlots
  {this, "JoinedIndividualPlots", {}, "Do several individual plots for several tools together"};
  
  /** @brief Specifies plots being done with several tool comparisons together.
  */
  Gaudi::Property<std::vector<JoinedCombinedPlots>> m_joinedComparedPlots
  {this, "JoinedComparedPlots", {}, "Do several compared plots for several pairs of tools together"};
  
  
  /** @brief Specifies plots being done with several tools together.
  */
  Gaudi::Property<std::vector<JoinedPlotsFromSingle>> m_joinedPlotsForTool
  {this, "JoinedPlotsForATool", {}, "Do several individual plots for a specific tool together"};
  
  /** @brief Specifies plots being done with several tool comparisons together.
  */
  Gaudi::Property<std::vector<JoinedPlotsFromCombined>> m_joinedPlotsForPair
  {this, "JoinedPlotsForAPairOfTools", {}, "Do several compared plots for a specific pairs of tools together"};
  
  
  /** @brief Option for adjusting the parameters for the cluster matching algorithm.
  */
  Gaudi::Property<MatchingOptions> m_matchingOptions
  {this, "ClusterMatchingParameters", {}, "Parameters for the cluster matching algorithm"};
  
  /**
   * @brief Extensions to save the plots as.
   */
  Gaudi::Property<std::vector<std::string>> m_saveExtensions{this, "PlotExtensions", {".png"}, "Extensions to save the plots"};

  /**
   * @brief Options for the corresponding extensions (if needed).
   */
  Gaudi::Property<std::vector<std::string>> m_saveOptions{this, "PlotExtensionOptions", {}, "Options for the corresponding extensions (if needed)"};

  
  //--------------------------------------------------
  //
  //              OTHER MEMBER VARIABLES
  //
  //--------------------------------------------------

  /**
   * @brief Pointer to Calo ID Helper
   */
  const CaloCell_ID * m_calo_id {nullptr};

  /** @brief A PImpl helper for our actual plotter class.
  */
  mutable std::unique_ptr<CaloPlotterHelper> m_pimplHelper ATLAS_THREAD_SAFE;
  //Mutable since accesses will be controlled through a mutex
  //to make all of this thread safe(-ish), even if not as elegant as it could be.

  /** @brief Map of the strings corresponding to all the tools
             that will be relevant for plotting (individually or in comparisons)
             to the index that will be used to identify the tool within the plotter.
  */
  std::map<std::string, int> m_toolsToCheckFor;

  //We technically could make the plotter thread-safe
  //(with a little bit of pain and even more over-complication)
  //but it seems reasonable to expect the plotting to be done
  //under circumstances where time measurements aren't as essential,
  //hence the mutex.


  /** @brief This mutex is used to ensure thread-safety
             (but not multithread-efficiency...)
             of the plotting.
    */
  mutable std::mutex m_mutex;

};

#endif //CALORECGPU_CALOGPUDEFAULTPLOTTER_H