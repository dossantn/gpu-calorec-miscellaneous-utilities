//
// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#include "CaloGPUDefaultPlotterHelper.h"

#include <iostream>
#include <numeric>
#include <algorithm>

#include "CxxUtils/checker_macros.h"

#include "CaloRecGPU/StandaloneDataIO.h"


ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

//FOR THE TIME BEING!

using namespace CaloRecGPU;

//Defined out-of-line to ensure the vtable
//does not end up in other translation units...
CaloPlotterHelper::base_plotter::~base_plotter()
{
}

CaloPlotterHelper::CaloPlotterHelper(const size_t num_tools) :
  m_tool_strings(num_tools, ""),
  m_per_tool_store(num_tools),
  m_tool_connections(num_tools)
{
}

void CaloPlotterHelper::add_tools(const size_t num_new_tools)
{
  m_per_tool_store.resize(m_per_tool_store.size() + num_new_tools);
  m_tool_connections.resize(m_tool_connections.size() + num_new_tools);
}


void CaloPlotterHelper::add_tool_combination(const int tool_1, const int tool_2, const bool match_in_energy)
{
  int current_size = m_per_tool_store.size();
  int new_size = std::max({current_size, tool_1, tool_2});
  if (new_size > current_size)
    {
      add_tools(new_size - current_size);
    }
  m_tool_connections[tool_1].emplace(tool_2 + 1);
  m_tool_connections[tool_2].emplace(-tool_1 - 1);
  m_per_combination_store[std::make_pair(tool_1, tool_2)] = {match_in_energy};
}

void CaloPlotterHelper::add_extension(const std::string & extension, const std::string & option)
{
  m_extensions_options.push_back(std::make_pair(extension, option));
}

void CaloPlotterHelper::set_tool_string(const int tool, const std::string & new_string)
{
  m_tool_strings[tool] = new_string;
}

size_t CaloPlotterHelper::add_to_plots(std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & plot_list,
                                       const std::string & name,
                                       const double x_min, const double x_max, const int x_bins,
                                       const double y_min, const double y_max, const int y_bins,
                                       const std::string prefix, const std::string suffix,
                                       const GetPlotDescriptionsFilter & filter) const
{
  const bool prev_add = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);
  std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> plots = get_plot_descriptions(name, filter);
  if (plots.size() == 0)
    {
      TH1::AddDirectory(prev_add);
      return 0;
    }
  for (auto & plt_ptr : plots)
    {
      plt_ptr->plot_name = post_undrscr(prefix) + plt_ptr->plot_name + pre_undrscr(suffix);
      plt_ptr->x_min = x_min;
      plt_ptr->x_max = x_max;
      plt_ptr->bin_x = x_bins;
      plt_ptr->y_min = y_min;
      plt_ptr->y_max = y_max;
      plt_ptr->bin_y = y_bins;
      plt_ptr->to_save_individual = true;
      plot_list.emplace_back(plt_ptr.release());
    }
  TH1::AddDirectory(prev_add);
  return plots.size();
}

size_t CaloPlotterHelper::add_to_plots(std::map<std::string, std::unique_ptr<CaloPlotterHelper::base_plotter>> & plot_list,
                                       const std::string & name,
                                       const double x_min, const double x_max, const int x_bins,
                                       const double y_min, const double y_max, const int y_bins,
                                       const std::string prefix, const std::string suffix,
                                       const GetPlotDescriptionsFilter & filter) const
{
  const bool prev_add = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);
  std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> plots = get_plot_descriptions(name, filter);
  if (plots.size() == 0)
    {
      TH1::AddDirectory(prev_add);
      return 0;
    }
  for (auto & plt_ptr : plots)
    {
      plt_ptr->plot_name = post_undrscr(prefix) + plt_ptr->plot_name + pre_undrscr(suffix);
      plt_ptr->x_min = x_min;
      plt_ptr->x_max = x_max;
      plt_ptr->bin_x = x_bins;
      plt_ptr->y_min = y_min;
      plt_ptr->y_max = y_max;
      plt_ptr->bin_y = y_bins;
      plt_ptr->to_save_individual = true;
      plot_list[plt_ptr->plot_name] = std::move(plt_ptr);
    }
  TH1::AddDirectory(prev_add);
  return plots.size();
}

size_t CaloPlotterHelper::add_to_plots(const int tool,
                                       const std::string & name,
                                       const double x_min, const double x_max, const int x_bins,
                                       const double y_min, const double y_max, const int y_bins,
                                       const std::string prefix, const std::string suffix,
                                       const GetPlotDescriptionsFilter & filter)
{
  if (size_t(tool) >= m_per_tool_store.size())
    {
      if (s_print_messages)
        {
          std::cout << "WARNING: Trying to add plot(s) matching '" << name << "' to invalid tool (" << tool << ")!" << std::endl;
        }
      return 0;
    }
  GetPlotDescriptionsFilter f2 = filter;
  f2.plot_combined = false;
  return add_to_plots(m_per_tool_store[tool].plots, name, x_min, x_max, x_bins, y_min, y_max, y_bins, prefix, suffix, f2);
}

size_t CaloPlotterHelper::add_to_plots(const std::pair<int, int> & tool_combination,
                                       const std::string & name,
                                       const double x_min, const double x_max, const int x_bins,
                                       const double y_min, const double y_max, const int y_bins,
                                       const std::string prefix, const std::string suffix,
                                       const GetPlotDescriptionsFilter & filter)
{
  if (m_per_combination_store.count(tool_combination) == 0)
    {
      if (s_print_messages)
        {
          std::cout << "WARNING: Trying to add plot(s) matching '" << name << "' to invalid tool combination ("
                    << tool_combination.first << ", " << tool_combination.second << ")!" << std::endl;
        }
      return 0;
    }
  GetPlotDescriptionsFilter f2 = filter;
  f2.plot_single = false;
  return add_to_plots(m_per_combination_store[tool_combination].plots, name,
                      x_min, x_max, x_bins, y_min, y_max, y_bins, prefix, suffix, f2);
}

size_t CaloPlotterHelper::add_to_single_plots(const std::string & name,
                                              const double x_min, const double x_max, const int x_bins,
                                              const double y_min, const double y_max, const int y_bins,
                                              const std::string prefix, const std::string suffix,
                                              const GetPlotDescriptionsFilter & filter)
{
  GetPlotDescriptionsFilter f2 = filter;
  f2.plot_combined = false;
  const bool prev_add = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);

  std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> plots = get_plot_descriptions(name, f2);

  if (plots.size() == 0)
    {
      TH1::AddDirectory(prev_add);
      return 0;
    }
  for (auto & plt_ptr : plots)
    {
      plt_ptr->x_min = x_min;
      plt_ptr->x_max = x_max;
      plt_ptr->bin_x = x_bins;
      plt_ptr->y_min = y_min;
      plt_ptr->y_max = y_max;
      plt_ptr->bin_y = y_bins;
      plt_ptr->to_save_individual = true;
      plt_ptr->plot_name = post_undrscr(prefix) + plt_ptr->plot_name + pre_undrscr(suffix);
      for (size_t i = 0; i < m_per_tool_store.size(); ++i)
        {
          auto clone = plt_ptr->clone();
          clone->plot_name += pre_undrscr(m_tool_strings[i]);
          m_per_tool_store[i].plots.emplace_back(clone.release());
        }
    }

  TH1::AddDirectory(prev_add);
  return plots.size();
}

size_t CaloPlotterHelper::add_to_combined_plots(const std::string & name,
                                                const double x_min, const double x_max, const int x_bins,
                                                const double y_min, const double y_max, const int y_bins,
                                                const std::string prefix, const std::string suffix,
                                                const GetPlotDescriptionsFilter & filter)
{
  GetPlotDescriptionsFilter f2 = filter;
  f2.plot_single = false;
  const bool prev_add = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);

  std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> plots = get_plot_descriptions(name, f2);

  if (plots.size() == 0)
    {
      TH1::AddDirectory(prev_add);
      return 0;
    }
  for (auto & plt_ptr : plots)
    {
      plt_ptr->x_min = x_min;
      plt_ptr->x_max = x_max;
      plt_ptr->bin_x = x_bins;
      plt_ptr->y_min = y_min;
      plt_ptr->y_max = y_max;
      plt_ptr->bin_y = y_bins;
      plt_ptr->to_save_individual = true;
      plt_ptr->plot_name = post_undrscr(prefix) + plt_ptr->plot_name + pre_undrscr(suffix);
      for (auto & k_v_store : m_per_combination_store)
        {
          auto clone = plt_ptr->clone();
          clone->plot_name += pre_undrscr(m_tool_strings[k_v_store.first.first]) + pre_undrscr(m_tool_strings[k_v_store.first.second]);
          k_v_store.second.plots.emplace_back(clone.release());
        }
    }

  TH1::AddDirectory(prev_add);
  return plots.size();
}

template <class Indexer, class StoreVec, class PlotsInfoNotVisibleHere>
static void generic_add_together_plural(StoreVec & store_holder,
                                        std::vector
                                        <
                                        std::pair<std::vector<Indexer>, PlotsInfoNotVisibleHere>
                                        > & together_holder,
                                        const std::vector<Indexer> & index_to_plot,
                                        const std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> & plots,
                                        const std::vector<CaloPlotterHelper::PlotStyle> & styles)
{
  std::vector<CaloPlotterHelper::PlotStyle> actual_styles;

  CaloPlotterHelper::PlotStyle temp_style;

  for (size_t i = 0; i < index_to_plot.size(); ++i)
    {
      if (i < styles.size())
        {
          temp_style = styles[i];
        }
      else
        {
          ++(temp_style.line.color);
          ++(temp_style.fill.color);
          ++(temp_style.marker.color);
        }
      actual_styles.push_back(temp_style);
    }

  for (const auto & plt : plots)
    {
      std::vector<size_t> plot_indices;
      for (const auto & idx : index_to_plot)
        {
          plot_indices.push_back(store_holder[idx].plots.size());
          auto clone = plt->clone();
          clone->to_save_individual = false;
          clone->to_save_joined = true;
          store_holder[idx].plots.emplace_back(clone.release());
        }
      PlotsInfoNotVisibleHere ret;
      ret.to_plot = plot_indices;
      ret.filename = plt->plot_name;
      ret.styles = actual_styles;
      together_holder.emplace_back(index_to_plot, ret);
    }
}


template <class Indexer, class PlotStore, class PlotsInfoNotVisibleHere>
static void generic_add_together_by_plots(CaloPlotterHelper & cph,
                                          PlotStore & store_plots,
                                          std::vector
                                          <
                                          std::pair<std::vector<Indexer>, PlotsInfoNotVisibleHere>
                                          > & together_holder,
                                          const Indexer & single_index,
                                          const std::vector<std::string> & plot_names,
                                          const std::string & title, const std::string & file_name,
                                          const double x_min, const double x_max, const int x_bins,
                                          const double y_min, const double y_max, const int y_bins,
                                          const std::vector<CaloPlotterHelper::PlotStyle> & styles,
                                          const CaloPlotterHelper::GetPlotDescriptionsFilter & filter)
{
  const size_t start_plots = store_plots.size();

  for (const auto & name : plot_names)
    {
      cph.add_to_plots(store_plots, name, x_min, x_max, x_bins, y_min, y_max, y_bins, "", "", filter);
    }

  const size_t end_plots = store_plots.size();

  for (size_t i = start_plots; i < end_plots; ++i)
    {
      store_plots[i]->to_save_individual = false;
      store_plots[i]->to_save_joined = false;
    }

  PlotsInfoNotVisibleHere ret;

  ret.to_plot.resize(end_plots - start_plots);

  CaloPlotterHelper::PlotStyle temp_style;

  for (size_t i = 0; i < ret.to_plot.size(); ++i)
    {
      ret.to_plot[i] = start_plots + i;

      if (i < styles.size())
        {
          temp_style = styles[i];
        }
      else
        {
          ++(temp_style.line.color);
          ++(temp_style.fill.color);
          ++(temp_style.marker.color);
        }
      ret.styles.push_back(temp_style);
    }

  ret.filename = file_name;
  ret.single_plottitle = title;

  std::vector<Indexer> vec;
  vec.push_back(single_index);

  together_holder.emplace_back(vec, ret);

}

void CaloPlotterHelper::add_together_tools(const std::vector<int> & tools, const std::string & name,
                                           const double x_min, const double x_max, const int x_bins,
                                           const double y_min, const double y_max, const int y_bins,
                                           const std::string & prefix, const std::string & suffix,
                                           const std::vector<CaloPlotterHelper::PlotStyle> & styles,
                                           const GetPlotDescriptionsFilter & filter                   )
{
  if (tools.size() < 2)
    {
      std::cout << "WARNING: trying to plot together tools with just one tool!" << std::endl;
      return;
    }
  for (const int t : tools)
    {
      if (size_t(t) >= m_per_tool_store.size())
        {
          if (s_print_messages)
            {
              std::cout << "WARNING: Trying to add together plot(s) involving invalid tool (" << t << ")!" << std::endl;
            }
          return;
        }
    }

  GetPlotDescriptionsFilter real_filter = filter;
  real_filter.plot_combined = false;
  real_filter.plot_2D = false;

  std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> plots;

  add_to_plots(plots, name, x_min, x_max, x_bins, y_min, y_max, y_bins, prefix, suffix, real_filter);

  generic_add_together_plural(m_per_tool_store, m_tools_together, tools, plots, styles);
}


void CaloPlotterHelper::add_together_combinations(const std::vector< std::pair<int, int> > & tool_combinations,
                                                  const std::string & name,
                                                  const double x_min, const double x_max, const int x_bins,
                                                  const double y_min, const double y_max, const int y_bins,
                                                  const std::string & prefix, const std::string & suffix,
                                                  const std::vector<CaloPlotterHelper::PlotStyle> & styles,
                                                  const GetPlotDescriptionsFilter & filter                        )
{
  if (tool_combinations.size() < 2)
    {
      std::cout << "WARNING: trying to plot together combinations with just one combination!" << std::endl;
      return;
    }
  for (const auto & pair : tool_combinations)
    {
      if (m_per_combination_store.count(pair) == 0)
        {
          if (s_print_messages)
            {
              std::cout << "WARNING: Trying to add together plot(s) involving invalid tools (" << pair.first << " , " <<  pair.second << ")!" << std::endl;
            }
          return;
        }
    }

  GetPlotDescriptionsFilter real_filter = filter;
  real_filter.plot_single = false;
  real_filter.plot_2D = false;

  std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> plots;

  add_to_plots(plots, name, x_min, x_max, x_bins, y_min, y_max, y_bins, prefix, suffix, real_filter);

  generic_add_together_plural(m_per_combination_store, m_combinations_together, tool_combinations, plots, styles);

}

void CaloPlotterHelper::add_together_tool_plots(const int tool, const std::vector<std::string> & plots,
                                                const std::string & title, const std::string & file_name,
                                                const double x_min, const double x_max, const int x_bins,
                                                const double y_min, const double y_max, const int y_bins,
                                                const std::vector<CaloPlotterHelper::PlotStyle> & styles,
                                                const GetPlotDescriptionsFilter & filter                    )
{
  if (size_t(tool) >= m_per_tool_store.size())
    {
      if (s_print_messages)
        {
          std::cout << "WARNING: Trying to add together plot(s) involving invalid tool (" << tool << ")!" << std::endl;
        }
      return;
    }

  GetPlotDescriptionsFilter real_filter = filter;
  real_filter.plot_combined = false;
  real_filter.plot_2D = false;

  generic_add_together_by_plots( *this, m_per_tool_store[tool].plots, m_tools_together, tool, plots, title,
                                 file_name, x_min, x_max, x_bins, y_min, y_max, y_bins, styles, real_filter );
}

void CaloPlotterHelper::add_together_combination_plots(const std::pair<int, int> & tool_combination,
                                                       const std::vector<std::string> & plots,
                                                       const std::string & title, const std::string & file_name,
                                                       const double x_min, const double x_max, const int x_bins,
                                                       const double y_min, const double y_max, const int y_bins,
                                                       const std::vector<CaloPlotterHelper::PlotStyle> & styles,
                                                       const GetPlotDescriptionsFilter & filter                    )
{
  if (m_per_combination_store.count(tool_combination) == 0)
    {
      if (s_print_messages)
        {
          std::cout << "WARNING: Trying to add together plot(s) involving invalid tools (" << tool_combination.first << " , " << tool_combination.second << ")!" << std::endl;
        }
      return;
    }

  GetPlotDescriptionsFilter real_filter = filter;
  real_filter.plot_single = false;
  real_filter.plot_2D = false;

  generic_add_together_by_plots( *this, m_per_combination_store[tool_combination].plots, m_combinations_together,
                                 tool_combination, plots, title, file_name,
                                 x_min, x_max, x_bins, y_min, y_max, y_bins, styles, real_filter                 );
}

void CaloPlotterHelper::set_matching_options(const double term_threshold, const double grow_threshold, const double seed_threshold,
                                             const double min_similarity, const double term_weight,    const double grow_weight,
                                             const double seed_weight)
{
  m_term_threshold = term_threshold;
  m_grow_threshold = grow_threshold;
  m_seed_threshold = seed_threshold;

  m_min_similarity = min_similarity;
  m_term_weight = term_weight;
  m_grow_weight = grow_weight;
  m_seed_weight = seed_weight;
}

void CaloPlotterHelper::setup_new_event()
{
  for (auto & store : m_per_tool_store)
    {
      store.added = false;
      //store.cell_state.clear();
      //store.clusters.clear();
      //In principle, we can expect to have enough memory for this.
      //Likely the plots are a bigger memory hog than this is, so...
    }
  m_per_event_message = "";
}

void CaloPlotterHelper::finish_event()
{
  //Nothing.
}

void CaloPlotterHelper::save_plots(const std::string & path, const std::string & prefix, const std::string & suffix)
{
  const auto err1 = StandaloneDataIO::prepare_folder_for_output(path);

  //size_t saved_count = 0;

  const bool prev_add = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);

  if (err1 != StandaloneDataIO::ErrorState::OK)
    {
      return;
    }
  auto do_plot = [&] ATLAS_NOT_THREAD_SAFE (auto & plt, const normalization_constants & nc, const std::string & extra_pref = "", const std::string & extra_suff = "")
  {
    if (!plt)
      {
        return;
      }
    for (const auto & pair : m_extensions_options)
      {
        //++saved_count;
        //This is to ensure the plots do not overwrite each other...
        plt->plot(nc, path, prefix + pre_undrscr(extra_pref), post_undrscr(extra_suff) + post_undrscr(suffix) /*+ std::to_string(saved_count)*/, pair.first, pair.second);
      }
  };

  for (auto & store : m_per_tool_store)
    {
      for (size_t i = 0; i < store.plots.size(); ++i)
        {
          auto & plt = store.plots[i];
          if (plt && plt->to_save_individual)
            {
              do_plot(plt, store.normalization);
            }
        }
    }

  for (auto & kv_store_map : m_per_combination_store)
    {
      auto & store = kv_store_map.second;
      for (auto & plt : store.plots)
        {
          if (plt && plt->to_save_individual)
            {
              do_plot(plt, store.normalization);
            }
        }
    }

  auto add_multiplot_for_single_thing = [&] ATLAS_NOT_THREAD_SAFE (const auto & plot_info, auto & thing_store)
  {
    std::unique_ptr<CaloPlotterHelper::joined_plotter> joined_plot;
    for (size_t i = 0; i < plot_info.to_plot.size(); ++i)
      {
        const size_t this_index = plot_info.to_plot[i];
        if (this_index >= thing_store.plots.size())
          {
            continue;
          }

        const auto & plt = thing_store.plots[this_index];

        if (!plt->to_save_joined)
          {
            continue;
          }

        if (!joined_plot)
          {
            joined_plot = plt->make_joined_plotter();
            joined_plot->plot_name = plot_info.filename;
            joined_plot->title = plot_info.single_plottitle;
          }

        auto clone = plt->clone();

        clone->set_style(plot_info.styles[i]);

        clone->add_to_joined_plotter(joined_plot.get(), thing_store.normalization);
      }
    do_plot(joined_plot, {});
  };

  auto do_for_list = [&] ATLAS_NOT_THREAD_SAFE (const auto & together_list, auto & store_list)
  {
    for (const auto & list_info : together_list)
      {
        const auto & list = list_info.first;
        const auto & info = list_info.second;
        if (list.size() == 1)
          {
            add_multiplot_for_single_thing(info, store_list[list[0]]);
          }
        else
          {
            std::unique_ptr<CaloPlotterHelper::joined_plotter> joined_plot;
            for (size_t i = 0; i < list.size(); ++i)
              {
                const size_t to_plot = info.to_plot[i];
                const auto & store = store_list[list[i]];
                if (to_plot >= store.plots.size())
                  {
                    continue;
                  }
                const auto & plt = store.plots[to_plot];
                if (!plt->to_save_joined)
                  {
                    continue;
                  }
                if (!joined_plot)
                  {
                    joined_plot = plt->make_joined_plotter();
                    joined_plot->plot_name = info.filename;
                  }

                auto clone = plt->clone();

                clone->set_style(info.styles[i]);

                clone->add_to_joined_plotter(joined_plot.get(), store.normalization);
              }
            do_plot(joined_plot, {});
          }
      }
  };

  do_for_list(m_tools_together, m_per_tool_store);
  do_for_list(m_combinations_together, m_per_combination_store);

  TH1::AddDirectory(prev_add);
}


void CaloPlotterHelper::add_event_data(const int tool_number,
                                       const CaloRecGPU::ConstantDataHolder & constant_data,
                                       const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & cell_info,
                                       const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & cell_state,
                                       const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & clusters,
                                       const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & moments)
{
  if (tool_number < 0 || size_t(tool_number) >= m_per_tool_store.size())
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "Trying to add data to invalid tool " << tool_number << std::endl;
        }
      return;
    }
  auto & store = m_per_tool_store[tool_number];

  if (store.added)
    {
      return;
      //Let's not make this any more confusing than it is...
    }

  store.cell_state = cell_state;
  store.clusters = clusters;
  store.moments = moments;
  store.added = true;
  store.normalization.update(constant_data, *cell_info, *cell_state, *clusters, *moments);

  for (auto & plot_ptr : store.plots)
    {
      plot_ptr->add_data(constant_data, *cell_info, *cell_state, *clusters, *moments);
    }

  sample_comparisons_holder sch;

  for (const int other_tool_number_signed : m_tool_connections[tool_number])
    //If this is negative, the other tool is the first element of the pair.
    {
      const int other_tool_number = std::abs(other_tool_number_signed) - 1;
      if (size_t(other_tool_number) >= m_per_tool_store.size())
        {
          if (CaloPlotterHelper::s_print_messages)
            {
              std::cout << "Trying to add data to " << tool_number << " together with invalid tool "
                        << other_tool_number << " (" << other_tool_number_signed << ")" << std::endl;
            }
          continue;
        }
      auto & other_store = m_per_tool_store[other_tool_number];

      if (!other_store.added)
        {
          continue;
        }

      std::pair<int, int> this_pair = (other_tool_number_signed < 0 ?
                                       std::make_pair(other_tool_number, tool_number) :
                                       std::make_pair(tool_number, other_tool_number)   );

      auto & store_1 = (other_tool_number_signed < 0 ? other_store : store);
      auto & store_2 = (other_tool_number_signed < 0 ? store : other_store);
      auto & together_store = m_per_combination_store[this_pair];

      {
        char message_buffer[80];
        snprintf(message_buffer, 80, "%32s -> %-32s: ", m_tool_strings[this_pair.first].c_str(), m_tool_strings[this_pair.second].c_str());
        m_per_event_message += message_buffer;
      }

      match_clusters(sch, constant_data, *cell_info, *store_1.cell_state, *store_2.cell_state,
                     *store_1.clusters, *store_2.clusters, *store_1.moments, *store_2.moments, together_store.match_in_energy);

      together_store.normalization.update(constant_data, *cell_info, *store_1.cell_state, *store_2.cell_state,
                                          *store_1.clusters, *store_2.clusters, *store_1.moments, *store_2.moments);

      for (auto & plot_ptr : together_store.plots)
        {
          plot_ptr->add_data(constant_data, *cell_info, *store_1.cell_state, *store_2.cell_state,
                             *store_1.clusters, *store_2.clusters, *store_1.moments, *store_2.moments, sch);
        }
    }
}

void CaloPlotterHelper::initialize()
{
  const bool prev_add = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);
  for (auto & store : m_per_tool_store)
    {
      for (auto & plot_ptr : store.plots)
        {
          plot_ptr->initialize_data();
        }
    }
  for (auto & kv_store : m_per_combination_store)
    {
      for (auto & plot_ptr : kv_store.second.plots)
        {
          plot_ptr->initialize_data();
        }
    }
  TH1::AddDirectory(prev_add);
}


void CaloPlotterHelper::initialize(const CaloPlotterHelper::PlotStyle & single_style,
                                   const CaloPlotterHelper::PlotStyle & combined_style)
{
  const bool prev_add = TH1::AddDirectoryStatus();
  TH1::AddDirectory(false);
  for (auto & store : m_per_tool_store)
    {
      for (auto & plot_ptr : store.plots)
        {
          plot_ptr->initialize_data();
          plot_ptr->set_style(single_style);
        }
    }
  for (auto & kv_store : m_per_combination_store)
    {
      for (auto & plot_ptr : kv_store.second.plots)
        {
          plot_ptr->initialize_data();
          plot_ptr->set_style(combined_style);
        }
    }
  TH1::AddDirectory(prev_add);
}

void CaloPlotterHelper::initialize(const CaloPlotterHelper::PlotStyle & style)
{
  initialize(style, style);
}

[[maybe_unused]] static float float_unhack(const unsigned int bits)
{
  float res;
  std::memcpy(&res, &bits, sizeof(float));
  //In C++20, we should bit-cast. For now, for our platform, works.
  return res;
}

void CaloPlotterHelper::match_clusters(CaloPlotterHelper::sample_comparisons_holder & sch,
                                       const CaloRecGPU::ConstantDataHolder & constant_data,
                                       const CaloRecGPU::CellInfoArr & cell_info,
                                       const CaloRecGPU::CellStateArr & cell_state_1,
                                       const CaloRecGPU::CellStateArr & cell_state_2,
                                       const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                                       const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                                       const CaloRecGPU::ClusterMomentsArr & /*moments_1*/,
                                       const CaloRecGPU::ClusterMomentsArr & /*moments_2*/,
                                       const bool match_in_energy)
{
  sch.r2t_table.clear();
  sch.r2t_table.resize(cluster_info_1.number, -1);

  sch.t2r_table.clear();
  sch.t2r_table.resize(cluster_info_2.number, -1);

  std::vector<int> similarity_map(cluster_info_1.number * cluster_info_2.number, 0.);

  std::vector<double> ref_normalization(cluster_info_1.number, 0.);
  std::vector<double> test_normalization(cluster_info_2.number, 0.);

  for (int i = 0; i < NCaloCells; ++i)
    {
      const ClusterTag ref_tag = cell_state_1.clusterTag[i];
      const ClusterTag test_tag = cell_state_2.clusterTag[i];

      if (!cell_info.is_valid(i))
        {
          continue;
        }

      double SNR = 0.00001;
      
      if (!cell_info.is_bad(*(constant_data.m_geometry), i))
        {
          const int gain = cell_info.gain[i];

          const double cellNoise = constant_data.m_cell_noise->noise[gain][i];
          if (std::isfinite(cellNoise) && cellNoise > 0.0f)
            {
              SNR = std::abs(cell_info.energy[i] / cellNoise);
            }
        }
        
      const double quantity = /*( match_in_energy ? std::abs(cell_info.energy[i]) :*/ SNR /*)*/;
      const double weight = (quantity + 1e-8) *
                            ( quantity > m_seed_threshold ? (match_in_energy ? 1000 : m_seed_weight) :
                              (
                                      quantity > m_grow_threshold ? (match_in_energy ? 750 : m_grow_weight) :
                                      (
                                              quantity > m_term_threshold ? (match_in_energy ? 500 : m_term_weight) : 0
                                      )
                              )
                            );
      int ref_c1 = -1, ref_c2 = -1, test_c1 = -1, test_c2 = -1;

      if (ref_tag.is_part_of_cluster())
        {
          ref_c1 = ref_tag.cluster_index();
          ref_c2 = ref_tag.is_shared_between_clusters() ? ref_tag.secondary_cluster_index() : ref_c1;
        }

      if (test_tag.is_part_of_cluster())
        {
          test_c1 = test_tag.cluster_index();
          test_c2 = test_tag.is_shared_between_clusters() ? test_tag.secondary_cluster_index() : test_c1;
        }

      float ref_rev_cw = ref_tag.is_shared_between_clusters() ? 0.5f : 1.0f;
      //float_unhack(ref_tag.secondary_cluster_weight());
      float test_rev_cw = test_tag.is_shared_between_clusters() ? 0.5f : 1.0f;
      //float_unhack(test_tag.secondary_cluster_weight());

      float ref_cw = 1.0f - ref_rev_cw;
      float test_cw = 1.0f - test_rev_cw;

      if (ref_c1 >= int(cluster_info_1.number) || ref_c2 >= int(cluster_info_1.number) ||
          test_c1 >= int(cluster_info_2.number) || test_c2 >= int(cluster_info_2.number) )
        {
          if (s_print_messages)
            {
              std::cout << "ERROR: Find Matches: " << i << " " << ref_c1 << " " << ref_c2 << " "
                        << test_c1 << " " << test_c2 << " ("
                        << cluster_info_1.number << " | " << cluster_info_2.number << ")" << std::endl;
            }
          continue;
        }

      if (ref_c1 >= 0 && test_c1 >= 0)
        {
          similarity_map[test_c1 * cluster_info_1.number + ref_c1] += weight * ref_cw * test_cw;
          similarity_map[test_c1 * cluster_info_1.number + ref_c2] += weight * ref_rev_cw * test_cw;
          similarity_map[test_c2 * cluster_info_1.number + ref_c1] += weight * ref_cw * test_rev_cw;
          similarity_map[test_c2 * cluster_info_1.number + ref_c2] += weight * ref_rev_cw * test_rev_cw;
        }
      if (ref_c1 >= 0)
        {
          ref_normalization[ref_c1] += weight * ref_cw;
          ref_normalization[ref_c2] += weight * ref_rev_cw;
        }
      if (test_c1 >= 0)
        {
          test_normalization[test_c1] += weight * test_cw;
          test_normalization[test_c2] += weight * test_rev_cw;
        }
    }

  //In essence, the Gale-Shapley Algorithm

  std::vector<std::vector<int>> sorted_GPU_matches;

  sorted_GPU_matches.reserve(cluster_info_2.number);

  for (int testc = 0; testc < cluster_info_2.number; ++testc)
    {
      std::vector<int> sorter(cluster_info_1.number);
      std::iota(sorter.begin(), sorter.end(), 0);

      std::sort(sorter.begin(), sorter.end(),
                [&](const int a, const int b)
      {
        const double a_weight = similarity_map[testc * cluster_info_1.number + a];
        const double b_weight = similarity_map[testc * cluster_info_1.number + b];
        return a_weight > b_weight;
      }
               );

      size_t wanted_size = 0;

      for (; wanted_size < sorter.size(); ++wanted_size)
        {
          const double match_weight = similarity_map[testc * cluster_info_1.number + sorter[wanted_size]] / test_normalization [testc];
          if (match_weight < m_min_similarity)
            {
              break;
            }
        }

      //Yeah, we could do a binary search for best worst-case complexity,
      //but we are expecting 1~2 similar clusters and the rest garbage,
      //so we're expecting only 1~2 iterations.
      //This actually means all that sorting is way way overkill,
      //but we must make sure in the most general case that this works...

      sorter.resize(wanted_size);

      sorted_GPU_matches.push_back(sorter);
    }

  int num_iter = 0;

  constexpr int max_iter = 32;

  std::vector<double> matched_weights(cluster_info_1.number, -1.);

  std::vector<size_t> skipped_matching(cluster_info_2.number, 0);

  for (int stop_counter = 0; stop_counter < cluster_info_2.number && num_iter < max_iter; ++num_iter)
    {
      stop_counter = 0;
      for (int testc = 0; testc < int(sorted_GPU_matches.size()); ++testc)
        {
          if (skipped_matching[testc] < sorted_GPU_matches[testc].size())
            {
              const int match_c = sorted_GPU_matches[testc][skipped_matching[testc]];
              const double match_weight = similarity_map[testc * cluster_info_1.number + match_c] / ref_normalization[match_c];
              if (match_weight >= m_min_similarity && match_weight > matched_weights[match_c])
                {
                  const int prev_match = sch.r2t_table[match_c];
                  if (prev_match >= 0)
                    {
                      ++skipped_matching[prev_match];
                      --stop_counter;
                    }
                  sch.r2t_table[match_c] = testc;
                  matched_weights[match_c] = match_weight;
                  ++stop_counter;
                }
              else
                {
                  ++skipped_matching[testc];
                }
            }
          else
            {
              ++stop_counter;
            }
        }
    }

  sch.unmatched_ref_list.clear();
  sch.unmatched_test_list.clear();

  for (size_t i = 0; i < sch.r2t_table.size(); ++i)
    {
      const int match = sch.r2t_table[i];
      if (sch.r2t_table[i] < 0)
        {
          sch.unmatched_ref_list.push_back(i);
        }
      else
        {
          sch.t2r_table[match] = i;
        }
    }

  for (size_t i = 0; i < sch.t2r_table.size(); ++i)
    {
      if (sch.t2r_table[i] < 0)
        {
          sch.unmatched_test_list.push_back(i);
        }
    }

  {
    char message_buffer[256];
    snprintf(message_buffer, 256,
             "%2d: %5d / %5d || %5d / %5d || %3d || %5d | %5d || %5d\n",
             num_iter,
             int(sch.r2t_table.size()) - int(sch.unmatched_ref_list.size()), int(sch.r2t_table.size()),
             int(sch.t2r_table.size()) - int(sch.unmatched_test_list.size()), int(sch.t2r_table.size()),
             int(sch.r2t_table.size()) - int(sch.t2r_table.size()),
             int(sch.unmatched_ref_list.size()),
             int(sch.unmatched_test_list.size()),
             int(sch.unmatched_ref_list.size()) - int(sch.unmatched_test_list.size())
            );
    if (s_print_messages)
      {
        std::cout << message_buffer << std::flush;
      }
    m_per_event_message += message_buffer;
  }

}