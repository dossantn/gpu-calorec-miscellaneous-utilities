// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
//

#ifndef TOPOAUTOMATONCLUSTEROPTIMIZER_TEST_DEFINES_H
#define TOPOAUTOMATONCLUSTEROPTIMIZER_TEST_DEFINES_H

#include <vector>
#include <string>
#include <array>
#include <iostream>

void setup_cuda_device()
{
  int devID = 0;
  cudaDeviceProp props;

  /* maybe we want something else here */
  cudaSetDevice(0);

  cudaGetDeviceProperties(&props, devID);
  std::cout << "[CUDA] Device " << devID << " " << props.name <<  " with compute capability " << props.major << "." << props.minor << std::endl;
}


template <class T>
struct top_n_holder
//Simple, no-frills sorted vector.
//(We assume T provides a strong ordering.)
{
 private:
  size_t m_nmax;
  std::vector<T> m_buff;
 public:
  top_n_holder(const size_t sz): m_nmax(sz)
  {
    m_buff.reserve(m_nmax);
  }
  /*Returns `true` if added.*/
  bool try_add(const T & t)
  {
    auto greater_it = std::lower_bound(m_buff.begin(), m_buff.end(), t);
    if (m_buff.size() < m_nmax)
      {
        m_buff.insert(greater_it, t);
      }
    else
      {
        if (greater_it == m_buff.begin())
          {
            return false;
          }
        else
          {
            for (auto it = m_buff.begin(); (it + 1) != greater_it; ++it)
              {
                *it = *(it + 1);
              }
            *(greater_it - 1) = t;
          }
      }
    return true;
  }
  void merge(const top_n_holder & other)
  {
    for (size_t i = other.m_buff.size(); i > 0; --i)
      {
        if (!try_add(other[i - 1]))
          {
            return;
            //Since we are ordered from largest to smallest,
          }
      }
  }
  const T & operator[] (const size_t i) const
  {
    return m_buff[i];
  }
  size_t size() const
  {
    return m_buff.size();
  }
};

struct loop_range
{
  using type = int;
  //It's enough for our purposes,
  //but might need to be bumped up to a int64_t to be sure...

  type start, stop, step;

  loop_range(const type begin = 0, const type end = 0, const type iter = 1):
    start(begin), stop(end), step(iter)
  {
  }

  type get_iteration(const type num) const
  //Returns the iteration number corresponding to the nearest
  //element of the iteration not greater than `num`
  //(that is, if `num` is not part of the numbers
  // that can be reached given `start` and `step`,
  // it's rounded down.)
  {
    return (num - start) / step ;
  }

  type get_number_of_iterations() const
  {
    return (stop - start) / step + 1;
  }

  type get_number_from_iteration(const type num) const
  {
    return start + num * step;
  }

  type get_number_in_the_middle() const
  //Rounded down in case there's an even number of iterations...
  {
    return get_number_from_iteration(get_number_of_iterations() / 2 - (get_number_of_iterations() % 2 == 0));
  }

  type last() const
  {
    return get_number_from_iteration(get_number_of_iterations() - 1);
    //Not necessarily stop...
  }

  type first() const
  {
    return start;
  }

  template <class F, class ... Args>
  void loop (F && f, Args && ... args) const
  {
    for (type i = start, count = 0; i <= stop; ++count, i += step )
      {
        f(count, i, std::forward<Args>(args)...);
      }
  }
};

template <int degrees_of_freedom>
struct optim_results
{
  std::array < int, degrees_of_freedom + 1 > params;
  //First is the number of CPU threads,
  //then are the degrees of freedom.
  size_t time;

#define CALORECGPU_OPTIM_RESULTS_DECL_OPERATOR(OP)                               \
  friend bool operator OP (const optim_results & r1, const optim_results & r2)   \
  {                                                                              \
    if (r1.time != r2.time)                                                      \
      {                                                                          \
        return r2.time OP r1.time;                                               \
        /*We want smaller to compare greater!*/                                  \
      }                                                                          \
    for (int i = 1; i <= degrees_of_freedom; ++i)                                \
      {                                                                          \
        if (r1.params[i] OP r2.params[i])                                        \
          {                                                                      \
            return true;                                                         \
          }                                                                      \
      }                                                                          \
    return r1.params[0] OP r2.params[0];                                         \
  }

  CALORECGPU_OPTIM_RESULTS_DECL_OPERATOR( < )
  CALORECGPU_OPTIM_RESULTS_DECL_OPERATOR( > )
  CALORECGPU_OPTIM_RESULTS_DECL_OPERATOR( <= )
  CALORECGPU_OPTIM_RESULTS_DECL_OPERATOR( >= )
  CALORECGPU_OPTIM_RESULTS_DECL_OPERATOR( == )
  CALORECGPU_OPTIM_RESULTS_DECL_OPERATOR( != )

  template <class Str>
  friend Str & operator<< (Str & s, const optim_results & o_r)
  {
    for (int i = 0; i <= degrees_of_freedom; ++i)
      {
        s << o_r.params[i] << " ";
      }
    s << o_r.time;
    return s;
  }
};


template <size_t num_total_loops, class opt_type, class TestHolder>
top_n_holder<opt_type> optimize_loop_recurse(opt_type p,
                                             TestHolder & test,
                                             const int compress_loops_after,
                                             const int keep_best,
                                             const loop_range & threads,
                                             const loop_range & r1)
{
  top_n_holder<opt_type> res(keep_best);

  auto threader = [&] (const int, const int num_threads, top_n_holder<opt_type> & h)
  {
    p.params[0] = num_threads;
    p.time = test.run(p.params);
    res.try_add(p);
  };

  r1.loop(threader, res);

  return res;
}

template <size_t num_total_loops, class opt_type, class TestHolder, class ... Loops>
top_n_holder<opt_type> optimize_loop_recurse(opt_type p,
                                             TestHolder & test,
                                             const int compress_loops_after,
                                             const int keep_best,
                                             const loop_range & threads,
                                             const loop_range & r1,
                                             const Loops & ... others)
{
  constexpr size_t this_loop_num = num_total_loops - sizeof...(others) - 1;
  if (this_loop_num > compress_loops_after)
    {
      int i = this_loop_num + 1;
      (p.params[++i] = others.get_number_in_the_middle(), ... );

      top_n_holder<opt_type> holder(keep_best);

      auto ranger = [&](const int, const int current_num, top_n_holder<opt_type> & h)
      {
        p.params[this_loop_num + 1] = current_num;
        top_n_holder<opt_type> res = optimize_loop_recurse<num_total_loops>(p, test, compress_loops_after, keep_best, threads, threads);
        h.merge(res);
      };

      r1.loop(ranger, holder);

      top_n_holder<opt_type> res = holder;

      for (size_t i = 0; i < holder.size(); ++i)
        {
          const opt_type this_best = holder[i];

          const top_n_holder<opt_type> this_res = optimize_loop_recurse<num_total_loops>(this_best, test, compress_loops_after, keep_best, threads, others...);

          res.merge(this_res);
        }

      return res;
    }
  else
    {
      top_n_holder<opt_type> holder(keep_best);

      auto ranger = [&](const int, const int current_num, top_n_holder<opt_type> & h)
      {
        p.params[this_loop_num + 1] = current_num;
        top_n_holder<opt_type> res = optimize_loop_recurse<num_total_loops>(p, test, compress_loops_after, keep_best, threads, others...);
        h.merge(res);
      };

      r1.loop(ranger, holder);
      return holder;
    }
}

/*
  @p TestHolder must have a @c run function that receives an @p std::array<int, N + 1>,
  where @c N is the number of degrees of freedom in the parameter space we want to explore.
  The first element of the array is the number of threads with which to run.
  The function must return the time elapsed, in microseconds.
*/

template <class TestHolder, class ... Loops>
top_n_holder<optim_results<sizeof...(Loops)>> optimize(TestHolder & test,
                                                       const int compress_loops_after,
                                                       const int keep_best,
                                                       const loop_range & threads,
                                                       const Loops & ... l)
{
  constexpr int num_total_loops = sizeof...(Loops) + 1;
  using opt_type = optim_results < num_total_loops - 1 >;
  return optimize_loop_recurse<num_total_loops>(opt_type{}, test, compress_loops_after, keep_best, threads, l..., threads);
}


#include <chrono>

struct StandardTimer
{
  using time_type = decltype(std::chrono::high_resolution_clock::now());
  static time_type mark_time()
  {
    return std::chrono::high_resolution_clock::now();
  }
  static size_t time_diff(const time_type & begin, const time_type & end)
  //In microseconds...
  {
    return std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
  }
};

#include "boost/chrono/chrono.hpp"
#include "boost/chrono/thread_clock.hpp"

struct PerThreadTimer
{
  using time_type = decltype(boost::chrono::thread_clock::now());
  static time_type mark_time()
  {
    return boost::chrono::thread_clock::now();
  }
  static size_t time_diff(const time_type & begin, const time_type & end)
  //In microseconds...
  {
    return boost::chrono::duration_cast<boost::chrono::microseconds>(end - begin).count();
  }
};

#include "CaloRecGPU/Helpers.h"

struct wrapped_CUDA_stream
{
  cudaStream_t stream;
  wrapped_CUDA_stream()
  {
    CUDA_ERRCHECK(cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking));
  }
  ~wrapped_CUDA_stream()
  {
    cudaStreamDestroy(stream);
  }
  operator cudaStream_t() const
  {
    return stream;
  }
};

template <class TestImpl, size_t num_params>
class Tester : TestImpl
{
 public:

  using TestImpl::TestImpl;

  size_t run(const std::array < int, num_params + 1 > & params)
  {
    const size_t num_threads = params[0];

    std::vector<typename TestImpl::thread_holder> thread_storage(num_threads);

    size_t ret = std::numeric_limits<size_t>::max();

    for (int rep_num = 0; rep_num < this->num_reps(); ++rep_num)
      {
        this->prepare_for_test(num_threads);

        const size_t max_events = this->num_events();

        std::vector<wrapped_CUDA_stream> stream_vector(num_threads);

        auto start = this->mark_time();

        for (size_t event = 0; event < max_events; ++event)
          {
            const size_t thread_num = event % num_threads;
            this->run_single_test(thread_storage[thread_num], stream_vector[thread_num], event, params);
          }

        CUDA_ERRCHECK(cudaDeviceSynchronize());

        auto end = this->mark_time();

        size_t this_time = this->time_diff(start, end);

        ret = std::min(ret, this_time);

        this->report_time(params, this_time);
      }

    return ret;
  }
};

#include "CaloRecGPU/CUDAFriendlyClasses.h"
#include "CaloRecGPU/DataHolders.h"
#include "CaloRecGPU/StandaloneDataIO.h"
#include "../../src/TopoAutomatonClusteringImpl.h"
#include "../../src/TopoAutomatonSplittingImpl.h"
#include "../../src/TopoAutomatonSplittingImpl.h"
#include "../../src/BasicGPUClusterInfoCalculatorImpl.h"
#include "CLHEP/Units/SystemOfUnits.h"
#include "CaloIdentifier/LArNeighbours.h"

struct StandardTestImpl : StandardTimer
{
 private:

  size_t m_num_reps, m_event_repeat;

  std::vector<CaloRecGPU::Helpers::CUDA_pinned_CPU_object<CaloRecGPU::CellInfoArr>> m_events_arr;
  CaloRecGPU::ConstantDataHolder m_constant_data;

  TACOptionsHolder m_tac_options;
  TASOptionsHolder m_tas_options;

  bool m_cluster_cut_abs;
  float m_cluster_cut_threshold;

  std::ofstream file;

 public:
  StandardTestImpl(const std::string & folder, const int max_events, const int num_reps, const int event_repeat, const std::string & output_file = ""):
    m_num_reps(num_reps),
    m_event_repeat(event_repeat),
    file(output_file != "" ? output_file : "times.txt")
  {
    file << "\n";
    StandaloneDataIO::FolderLoadOptions flo;
    flo.load_cell_info = true;
    flo.load_geometry = true;
    flo.load_noise = true;
    auto load = StandaloneDataIO::load_folder(folder, max_events, flo);
    for (auto & info : load.cell_info)
      {
        m_events_arr.emplace_back(std::move(info.second));
      }
    m_constant_data.m_geometry_dev = (*(load.geometry.begin())).second;
    m_constant_data.m_cell_noise_dev = (*(load.noise.begin())).second;

    m_cluster_cut_abs = true;
    m_cluster_cut_threshold = 0. * CLHEP::MeV;

    m_tac_options.allocate();
    m_tac_options.m_options->seed_threshold = 4;
    m_tac_options.m_options->grow_threshold = 2;
    m_tac_options.m_options->terminal_threshold = 0;
    m_tac_options.m_options->abs_seed = true;
    m_tac_options.m_options->abs_grow = true;
    m_tac_options.m_options->abs_terminal = true;
    m_tac_options.m_options->use_two_gaussian = false;
    m_tac_options.m_options->treat_L1_predicted_as_good = true;
    m_tac_options.m_options->use_time_cut = false;
    m_tac_options.m_options->keep_significant_cells = false;
    m_tac_options.m_options->completely_exclude_cut_seeds = false;
    m_tac_options.m_options->time_threshold = 12.5 * CLHEP::ns;
    m_tac_options.m_options->snr_threshold_for_keeping_cells = 20;
    m_tac_options.m_options->limit_HECIW_and_FCal_neighs = false;
    m_tac_options.m_options->limit_PS_neighs = true;
    m_tac_options.m_options->neighbour_options = LArNeighbours::super3D;
    m_tac_options.m_options->valid_sampling_seed = 0xFFFFFFFFU;
    m_tac_options.m_options->valid_calorimeter_by_sampling = 0xFFFFFFFFU;
    m_tac_options.sendToGPU();

    m_tas_options.allocate();
    m_tas_options.m_options->valid_sampling_primary = 0x2000CCU;
    //EMB2 = 2^2, EMB3 = 2^3, EME2 = 2^6, EME = 2^7, FCAL0 = 2^21
    m_tas_options.m_options->valid_sampling_secondary = 0xC3FF22U;
    //EMB1 = 2^1, EME1 = 2^5, TileBar[0,2] = 2^[12,14],
    //TileExt[0,2] = 2^[15,17], HEC^[0,3] = 2^[8,11],
    //FCAL[1,2] = 2^[22,23]
    m_tas_options.m_options->min_num_cells = 4;
    m_tas_options.m_options->min_maximum_energy = 500 * CLHEP::MeV;
    m_tas_options.m_options->EM_shower_scale = 5.0 * CLHEP::cm;
    m_tas_options.m_options->share_border_cells = true;
    m_tas_options.m_options->use_absolute_energy = false;
    m_tas_options.m_options->treat_L1_predicted_as_good = true;
    m_tas_options.m_options->limit_HECIW_and_FCal_neighs = false;
    m_tas_options.m_options->limit_PS_neighs = true;
    m_tas_options.sendToGPU();
  }

  size_t num_reps() const
  {
    return m_num_reps;
  }

  size_t num_events() const
  {
    return m_events_arr.size() * m_event_repeat;
  }

  void prepare_for_test(const int num_threads)
  {
  }

  CaloRecGPU::CellInfoArr * get_event(CaloRecGPU::EventDataHolder & data_holder, const size_t event_num)
  {
    const size_t real_event_num = event_num % m_events_arr.size();
    return m_events_arr[real_event_num];
  }

  struct thread_holder
  {
    static constexpr size_t temporaries_size = std::max({ sizeof(TopoAutomatonTemporaries),
                                                          sizeof(ClusterInfoCalculatorTemporaries),
                                                          sizeof(TopoAutomatonSplittingTemporaries) });
    CaloRecGPU::EventDataHolder data_holder;
    CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> cell_state_dummy;
    CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> clusters_dummy;
    void * temporary_buffer;
    thread_holder()
    {
      data_holder.allocate();
      cell_state_dummy.allocate();
      clusters_dummy.allocate();
      temporary_buffer = CaloRecGPU::CUDA_Helpers::allocate(temporaries_size);
    }
    thread_holder(const thread_holder &) = delete;
    thread_holder & operator= (const thread_holder &) = delete;
    ~thread_holder()
    {
      CaloRecGPU::CUDA_Helpers::deallocate(temporary_buffer);
    }
  };

  template <class ... Ignore>
  void run_single_test(thread_holder & th, cudaStream_t stream, const size_t event_number, Ignore && ...)
  {
    CaloRecGPU::Helpers::CUDA_kernel_object<TopoAutomatonTemporaries>          tac_temps((TopoAutomatonTemporaries *) th.temporary_buffer);
    CaloRecGPU::Helpers::CUDA_kernel_object<ClusterInfoCalculatorTemporaries>  cic_temps((ClusterInfoCalculatorTemporaries *) th.temporary_buffer);
    CaloRecGPU::Helpers::CUDA_kernel_object<TopoAutomatonSplittingTemporaries> tas_temps((TopoAutomatonSplittingTemporaries *) th.temporary_buffer);

    CaloRecGPU::CellInfoArr * cell_info = get_event(th.data_holder, event_number);

    CUDA_ERRCHECK(cudaMemcpyAsync((CaloRecGPU::CellInfoArr *) th.data_holder.m_cell_info_dev, cell_info, sizeof(CaloRecGPU::CellInfoArr), cudaMemcpyHostToDevice, stream));

    signalToNoise(th.data_holder, tac_temps, m_constant_data, m_tac_options, false, &stream);
    cellPairs(th.data_holder, tac_temps, m_constant_data, m_tac_options, false, &stream);
    clusterGrowing(th.data_holder, tac_temps, m_constant_data, m_tac_options, false, &stream);

    updateSeedCellProperties(th.data_holder, cic_temps, m_constant_data, false, &stream);
    calculateClusterProperties(th.data_holder, cic_temps, m_constant_data, false, m_cluster_cut_abs, m_cluster_cut_threshold, &stream);

    fillNeighbours(th.data_holder, tas_temps, m_constant_data, m_tas_options, false, &stream);
    findLocalMaxima(th.data_holder, tas_temps, m_constant_data, m_tas_options, false, &stream);
    excludeSecondaryMaxima(th.data_holder, tas_temps, m_constant_data, m_tas_options, false, &stream);
    splitClusterGrowing(th.data_holder, tas_temps, m_constant_data, m_tas_options, false, &stream);
    cellWeightingAndFinalization(th.data_holder, tas_temps, m_constant_data, m_tas_options, false, &stream);

    updateSeedCellProperties(th.data_holder, cic_temps, m_constant_data, false, &stream);
    calculateClusterProperties(th.data_holder, cic_temps, m_constant_data, false, true, -1, &stream);
    /*
    CUDA_ERRCHECK(cudaMemcpyAsync((CaloRecGPU::CellStateArr *) th.cell_state_dummy, (CaloRecGPU::CellStateArr *) th.data_holder.m_cell_state_dev, sizeof(CaloRecGPU::CellStateArr), cudaMemcpyDeviceToHost, stream));
    CUDA_ERRCHECK(cudaMemcpyAsync((CaloRecGPU::ClusterInfoArr *) th.clusters_dummy, (CaloRecGPU::ClusterInfoArr *)th.data_holder.m_clusters_dev, sizeof(CaloRecGPU::ClusterInfoArr), cudaMemcpyDeviceToHost, stream));
    */
  }

  template <size_t n>
  void report_time(const std::array<int, n> & p, const size_t t)
  {
    std::cout << p[0] << " " << t << "\n";
    file << p[0] << " " << t << "\n";
  }

  ~StandardTestImpl()
  {
    file << std::endl;
  }
};

using StandardTest = Tester<StandardTestImpl, 1>;

#endif
