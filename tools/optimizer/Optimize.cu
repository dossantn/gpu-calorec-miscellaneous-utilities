// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
//

#define TTAC_CALCULATE_PHI_BY_SIMPLE_AVERAGE 1

#include "TestDefinitions.h"

using namespace CaloRecGPU;

int main(int argc, char ** argv)
{
  std::string folder = ".";
  int max_events = 32,
      num_reps = 5,
      compress_loops_after = 1,
      keep_best = 3,
      event_repeat = 1;
  std::string filename = "";

  if (argc > 1)
    {
      folder = argv[1];
    }
  if (argc > 2)
    {
      max_events = std::atoi(argv[2]);
    }
  if (argc > 3)
    {
      filename = argv[3];
    }
  if (argc > 4)
    {
      num_reps = std::atoi(argv[4]);
    }
  if (argc > 5)
    {
      compress_loops_after = std::atoi(argv[5]);
    }
  if (argc > 6)
    {
      keep_best = std::atoi(argv[6]);
    }
  if (argc > 7)
    {
      event_repeat = std::atoi(argv[7]);
    }

  StandardTest test(folder, max_events, num_reps, event_repeat, filename);


  loop_range threads{1, 64, 1};
  loop_range blocks{1, 1, 1};

  setup_cuda_device();

  const auto results = optimize(test, compress_loops_after, keep_best, threads, blocks);

  std::cout << "-----------------------------------------------------------" << std::endl;

  for (size_t i = results.size(); i > 0; --i)
    {
      std::cout << results[i - 1] << std::endl;
    }

  return 0;
}
