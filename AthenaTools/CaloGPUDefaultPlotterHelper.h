//
// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef CALORECGPU_CALOGPUDEFAULTPLOTTERHELPER_H
#define CALORECGPU_CALOGPUDEFAULTPLOTTERHELPER_H

#include <vector>
#include <utility>
#include <string>
#include <sstream>
#include <iomanip>
#include <set>
#include <functional>
#include <memory>
#include <cstddef>
#include "CaloRecGPU/Helpers.h"
#include "CaloRecGPU/CUDAFriendlyClasses.h"
#include "CaloRecGPU/DataHolders.h"

#include <TROOT.h>    // the TROOT object is the entry point to the ROOT system
#include <TCanvas.h>  // graphics canvas

#include <TFile.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TH2F.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLine.h>
#include <TLegend.h>
#include <TPave.h>
#include <TPad.h>
#include <TMarker.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TLatex.h>

#include "CxxUtils/checker_macros.h"

//POSSIBLE TODO: Make plotting thread-safe by maintaining separate lists of plots and merging them at the end.
//               Painful? Yes. Necessary? Not quite. Useful? Possibly...


/**
 * @class CaloPlotterHelper
 * @author Nuno Fernandes <nuno.dos.santos.fernandes@cern.ch>
 * @date 12 November 2022
 * @brief The actual implementation of the cluster plotter.
 *
 * @warning The plotting is (currently) not thread-safe. To use safely in a multi-threading context,
 *          guard actual usage by a mutex.
 *
 * Capabilities:
 *
 *  - Cluster Properties:
 *     - `clusters_<prop>`
 *     - `clusters_delta_<prop>` (for single properties)
 *     - `clusters_<prop>_ref` (for single properties)
 *     - `clusters_<prop>_test` (for single properties)
 *     - `clusters_[any combination of <prop>, delta and ref/test]_rel_[any combination of <prop>, delta and ref/test]`
 *     - `clusters_[any derived form of <prop>]_norm
 *     - `clusters_[any derived form of <prop>]_cumul (only for quantities that are assuredly positive)
 *     - `clusters_[any derived form of <prop>]_versus_[any derived form of <prop>]`
 *     - `unmatched_{ref/test/both}_[any derived form of <prop> that only depends on one tool]`
 *     - `unmatched_{ref/test/both}_[any derived form of <prop> that only depends on one tool]_norm`
 *     - `unmatched_{ref/test/both}_[any derived form of <prop> that only depends on one tool]_cumul`
 *       (only for quantities that are assuredly positive)
 *
 * Where <prop> can be:
 * ```
 * E                   (s)
 * abs_E               (s)
 * Et                  (s)
 * eta                 (s)
 * phi                 (s)
 * size                (s)
 * weighted_size       (s)
 * delta_R             (c)
 * delta_phi_in_range  (c)
 * diff_cells          (c)
 * diff_cells_weighted (c)
 * ```
 *
 * The 1D plots can have `_norm` appended to be normalized by the average number of clusters per event.
 *
 * Some (size, diff_cells, delta_R) can also have `_cumul` appended,
 * to be integrated from infinity to 0, expressing the "fraction of clusters with greater `<prop>`".
 * (Useful for telling if e. g. more than y % of the clusters differ by less than x cells.)
 *
 * - Cell Properties:
 *     - `cells_count_<type>`
 *     - `cells_count_<type>_norm`
 *     - `cells_count_<type>_cumul`
 *     - `cells_delta_<type>` (for single types)
 *     - `cells_delta_<type>_norm` (for single types)
 *     - `cells_<type>_hist_[any derived from of <prop>]`
 *     - `cells_<type>_hist_[any derived from of <prop>]_norm`
 *     - `cells_<type>_hist_[any derived from of <prop>]_cumul`
 *     - `cells_<type>_hist_[any derived from of <prop>]_versus_[any derived from of <prop>]`
 *
 * Where `<type>` can be:
 * ```
 * global              (b)
 * intracluster        (s)
 * extracluster        (s)
 * shared              (s)
 * same_E              (b)
 * same_SNR            (b)
 * same_abs_SNR        (b)
 * confusable_E        (b)
 * confusable_SNR      (b)
 * confusable_abs_SNR  (b)
 * same_cluster        (c)
 * diff_cluster        (c)
 * ```
 *
 * And where `<prop>` can be:
 * ```
 * E                   (b)
 * abs_E               (b)
 * gain                (b)
 * noise               (b)
 * SNR                 (b)
 * abs_SNR             (b)
 * time                (b)
 * index               (b)
 * Pearson_hash        (b)
 * sampling            (b)
 * x                   (b)
 * y                   (b)
 * z                   (b)
 * phi                 (b)
 * eta                 (b)
 * primary_weight      (s)
 * secondary_weight    (s)
 * ```
 *
 * With `(s)` denoting something that is only valid for a single tool,
 * `(c)` for a comparison between tools and `(b)` for both.
 *
 * The 1D plots can have `_norm` appended to be normalized by the number of events.
 *
 * - Other plots:
 *      - `num_clusters` and `num_clusters_norm` (averaged by the number of events)
 *      - `num_delta_clusters` and `num_delta_clusters_norm` (averaged by the number of events)
 *      - `num_unmatched_ref_clusters` and `num_unmatched_ref_clusters_norm`
 *        (averaged by the number of events)
 *      - `num_unmatched_test_clusters` and `num_unmatched_test_clusters_norm`
 *        (averaged by the number of events)
 *      - `num_unmatched_any_clusters` and `num_unmatched_any_clusters_norm`
 *        (averaged by the number of events)
 */

struct CaloPlotterHelper
{
 public:
  ///x and y dimensions of the canvas on which to plot.
  ///@warning Write to this in a thread-safe way!
  inline static int s_canvas_x ATLAS_THREAD_SAFE = 1200, s_canvas_y ATLAS_THREAD_SAFE = 1000;

  inline static double s_canvas_left_margin ATLAS_THREAD_SAFE = 0.175,
                                            s_canvas_right_margin ATLAS_THREAD_SAFE = 0.175,
                                                                  s_canvas_top_margin ATLAS_THREAD_SAFE = 0.175,
                                                                                      s_canvas_bottom_margin ATLAS_THREAD_SAFE = 0.175;

  ///Write the plot title or not.
  ///@warning Write to this in a thread-safe way!
  inline static bool s_place_title ATLAS_THREAD_SAFE = false;

  ///Place "per bin width" in the y axis.
  ///@warning Write to this in a thread-safe way!
  inline static bool s_yaxis_bins ATLAS_THREAD_SAFE = false;

  ///For writing extra text like "Internal" after the ATLAS label
  ///@warning Write to this in a thread-safe way!
  inline static std::string s_plot_label ATLAS_THREAD_SAFE = "";

  ///Place the ATLAS label (and associated extra text) or not.
  ///@warning Write to this in a thread-safe way!
  inline static bool s_place_ATLAS_label ATLAS_THREAD_SAFE = false;

  ///Additional information below the ATLAS label
  ///@warning Write to this in a thread-safe way!
  inline static std::string s_extra_label_text ATLAS_THREAD_SAFE = "";//"#sqrt{s} = 13 TeV";

  ///If @p true, prints some messages to @p stdout. Not recommended for use within Athena!
  ///@warning Write to this in a thread-safe way!
  inline static bool s_print_messages ATLAS_THREAD_SAFE = false;

 private:

  static std::string pre_undrscr(const std::string & str)
  {
    if (str == "")
      {
        return str;
      }
    else
      {
        return std::string("_") + str;
      }
  }

  static std::string post_undrscr(const std::string & str)
  {
    if (str == "")
      {
        return str;
      }
    else
      {
        return str + "_";
      }
  }

 public:

  /**
   * @class PlotStyle
   * @brief Holds style definitions for our plots.
   */
  struct PlotStyle
  {
    struct ElementStyle
    {
      int color = 1;
      int style = 1;
      float alpha = 1.0f;
      int size = 1;
    };

    ElementStyle line, fill, marker;
  };

  /** @class sample_comparisons_holder
      @brief Stores the comparison between two tools,
             the first being taken as "reference" and the second as "test".
  */

  struct sample_comparisons_holder
  {
    std::vector<int> r2t_table, t2r_table;
    std::vector<int> unmatched_ref_list, unmatched_test_list;

    ///Converts a cluster index from the reference tool (first) to the test tool (second).
    ///Returns `-1` in case the cluster has not been matched.
    int r2t(const int i) const
    {
      return r2t_table[i];
    }
    ///Converts a cluster index from the test tool (second) to the reference tool (first).
    ///Returns `-1` in case the cluster has not been matched.
    int t2r(const int i) const
    {
      return t2r_table[i];
    }
    ///Returns the number of unmatched clusters in the reference (first) tool.
    int ref_unmatched() const
    {
      return unmatched_ref_list.size();
    }
    ///Returns the number of unmatched clusters in the test (second) tool.
    int test_unmatched() const
    {
      return unmatched_test_list.size();
    }
  };

  /** @class normalization_constants
      @brief Holds relevant constants for normalization.
  */
  struct normalization_constants
  {
    size_t num_events = 0;

    size_t num_total_clusters = 0;
    //These are cumulative, that is, over all events.

    size_t num_reference_clusters = 0;
    size_t num_test_clusters = 0;

    enum NormalizationKind
    { None = 0, ByNumberOfEvents = 0x1, ByTotalClusters = 0x2, ByReferenceClusters = 0x4, ByTestClusters = 0x8 };
    //My suggestion is that it be bitflag based, in case we need more...

    void update(const CaloRecGPU::ConstantDataHolder & /*cdh*/,
                const CaloRecGPU::CellInfoArr & /*cell_info*/,
                const CaloRecGPU::CellStateArr & /*cell_state*/,
                const CaloRecGPU::ClusterInfoArr & cluster_info,
                const CaloRecGPU::ClusterMomentsArr & /*cluster_moments*/)
    {
      ++num_events;
      num_total_clusters += cluster_info.number;
    }

    void update(const CaloRecGPU::ConstantDataHolder & /*cdh*/,
                const CaloRecGPU::CellInfoArr & /*cell_info*/,
                const CaloRecGPU::CellStateArr & /*cell_state_1*/,
                const CaloRecGPU::CellStateArr & /*cell_state_*/,
                const CaloRecGPU::ClusterInfoArr & clusters_1,
                const CaloRecGPU::ClusterInfoArr & clusters_2,
                const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_1*/,
                const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_2*/)
    {
      ++num_events;
      num_reference_clusters += clusters_1.number;
      num_test_clusters += clusters_2.number;
      num_total_clusters += clusters_1.number + clusters_2.number;
      //Since it's the reference.
    }

    double get_factor(const NormalizationKind & norm) const
    {
      double ret = 1;
      if (norm & ByNumberOfEvents)
        {
          ret *= num_events;
        }
      if (norm & ByTotalClusters)
        {
          ret *= num_total_clusters;
        }
      if (norm & ByReferenceClusters)
        {
          ret *= num_reference_clusters;
        }
      if (norm & ByTestClusters)
        {
          ret *= num_test_clusters;
        }
      return ret;
    }


  };

  /** @class joined_plotter
      @brief The base class for plotting several things together...
  */
  struct joined_plotter;

  /** @class base_plotter
      @brief The base class for all our plotting endeavours.
  */
  struct base_plotter
  {
    double x_min = 0, x_max = 0, y_min = 0, y_max = 0;
    size_t bin_x = 32, bin_y = 32;
    bool log_x = false, log_y = false, log_z = false;
    std::string axis_x = "", axis_y = "", axis_z = "", title = "", plot_name = "";

    bool to_save_individual = false;
    bool to_save_joined = false;
    //This is actually not used within the class.
    //Not the most elegant, I know.

    virtual void plot ATLAS_NOT_THREAD_SAFE (TCanvas * cv, const normalization_constants & nc, const std::string & name, const std::string & options) = 0;

    virtual void append_value(const double x) = 0;
    virtual void append_value(const double x, const double y) = 0;
    virtual void append_value(const double x, const double y, const double z) = 0;
    virtual void append_value(const double x, const double y, const double z, const double w) = 0;

    virtual void initialize_data() = 0;

    virtual void add_data(const CaloRecGPU::ConstantDataHolder & constant_data,
                          const CaloRecGPU::CellInfoArr & cell_info,
                          const CaloRecGPU::CellStateArr & cell_state,
                          const CaloRecGPU::ClusterInfoArr & cluster_info,
                          const CaloRecGPU::ClusterMomentsArr & /*cluster_moments*/) = 0;

    virtual void add_data(const CaloRecGPU::ConstantDataHolder & constant_data,
                          const CaloRecGPU::CellInfoArr & cell_info,
                          const CaloRecGPU::CellStateArr & cell_state_1,
                          const CaloRecGPU::CellStateArr & cell_state_2,
                          const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                          const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                          const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_1*/,
                          const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_2*/,
                          const sample_comparisons_holder & comparisons) = 0;

    virtual void set_style(const PlotStyle & style) = 0;

    virtual void add_to_joined_plotter(joined_plotter * joined, const normalization_constants & nc) = 0;

    ///This does not add data from the plotter to the joined plotter, only its axes, title, etc.!
    virtual std::unique_ptr<joined_plotter> make_joined_plotter() = 0;

    virtual std::unique_ptr<base_plotter> clone() const = 0;

    std::string build_filename(const std::string & path,
                               const std::string & prefix,
                               const std::string & suffix,
                               const std::string & extension) const
    {
      return path + (path.size() > 0 && path.back() != '/' ? "/" : "") +
             post_undrscr(prefix) +
             plot_name +
             pre_undrscr(suffix) +
             (extension.size() > 0 && extension[0] != '.' ? "." : "") + extension;
    }

    void plot ATLAS_NOT_THREAD_SAFE (const normalization_constants & nc, const std::string & filename, const std::string & options)
    {
      TCanvas cv("cv", "canvas", CaloPlotterHelper::s_canvas_x, CaloPlotterHelper::s_canvas_y);
      this->plot(&cv, nc, filename, options);
    }

    void plot ATLAS_NOT_THREAD_SAFE (TCanvas * cv,
                                     const normalization_constants & nc,
                                     const std::string & path,
                                     const std::string & prefix,
                                     const std::string & suffix,
                                     const std::string & extension,
                                     const std::string & options    )
    {
      plot(cv, nc, build_filename(path, prefix, suffix, extension), options);
    }

    void plot ATLAS_NOT_THREAD_SAFE (const normalization_constants & nc,
                                     const std::string & path,
                                     const std::string & prefix,
                                     const std::string & suffix,
                                     const std::string & extension,
                                     const std::string & options    )
    {
      plot(nc, build_filename(path, prefix, suffix, extension), options);
    }

   private:

    static std::string stringify_pretty_number (const double num)
    {
      std::stringstream sstr;
      sstr << std::setprecision(2) << std::fixed << num;
      return sstr.str();
    }

    static std::string approximate_string(const double num)
    {
      if (num >= 1e-2 && num <= 1e4)
        {
          return stringify_pretty_number(std::ceil(num * 1e3) * 1e-3);
        }
      else
        {
          double log = std::floor(std::log10(num));
          return stringify_pretty_number(std::ceil(num * pow(10, 3 - log)) * 1e-3) + " #times 10^{" + std::to_string(int(log)) + "}";
        }
    }

    template <class Plot>
    static std::string get_bin_label(Plot * gr)
    {
      const double width = gr->GetXaxis()->GetBinWidth(1);
      std::string_view xtext = gr->GetXaxis()->GetTitle();
      const size_t pos_l = xtext.find_last_of('[');
      const size_t pos_r = xtext.find_last_of(']');
      if (pos_l == std::string_view::npos || pos_r == std::string_view::npos || pos_r <= pos_l)
        {
          return approximate_string(width);
        }
      else
        {
          return approximate_string(width) + " " + std::string(xtext.substr(pos_l + 1, pos_r - pos_l - 1));
        }
    }

    static void ATLASLabel ATLAS_NOT_THREAD_SAFE (const Double_t x, const Double_t y)
    {
      TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
      l.SetNDC();
      l.SetTextFont(72);
      l.SetTextColor(1);

      double delx = 0.115 * 696 * gPad->GetWh() / (472 * gPad->GetWw());

      l.DrawLatex(x, y, "ATLAS");
      if (s_plot_label != "")
        {
          TLatex p;
          p.SetNDC();
          p.SetTextFont(42);
          p.SetTextColor(1);
          p.DrawLatex(x + delx, y, s_plot_label.c_str());
          //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
        }
    }

    ///@warning Change this only in a thread-safe way!
    inline static double s_labelstart_x ATLAS_THREAD_SAFE = 0.625;
    ///@warning Change this only in a thread-safe way!
    inline static double s_labelstart_y ATLAS_THREAD_SAFE = 0.625;
    //Well, legend, but...
    ///@warning Change this only in a thread-safe way!
    inline static double s_labelsize_x ATLAS_THREAD_SAFE = 0.25;
    ///@warning Change this only in a thread-safe way!
    inline static double s_labelsize_y ATLAS_THREAD_SAFE = 0.25;

    ///@warning Change this only in a thread-safe way!
    inline static double s_labeloffset_x ATLAS_THREAD_SAFE = -0.225;
    ///@warning Change this only in a thread-safe way!
    inline static double s_labeloffset_y ATLAS_THREAD_SAFE  = s_labelsize_y + 0.01;
    //These control the position of the "ATLAS ..." label
    //in relation to the plot legend.

    ///@warning Change this only in a thread-safe way!
    inline static double s_extraspacefactor_add ATLAS_THREAD_SAFE = 0.;
    ///@warning Change this only in a thread-safe way!
    inline static double s_extraspacefactor_multiply ATLAS_THREAD_SAFE = 2.5;

    ///@warning Change this only in a thread-safe way!
    inline static double s_extralabeloffset_x ATLAS_THREAD_SAFE = 0.075;
    ///@warning Change this only in a thread-safe way!
    inline static double s_extralabeloffset_y ATLAS_THREAD_SAFE = -0.04;

    static void extra_text_label ATLAS_NOT_THREAD_SAFE (Double_t x, Double_t y)
    {
      TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
      l.SetNDC();
      l.SetTextColor(1);
      l.SetTextSize(0.04);

      l.DrawLatex(x, y, s_extra_label_text.c_str());
    }

   public:

    void plot_one ATLAS_NOT_THREAD_SAFE (TCanvas * cv, TH1 * gr, const std::string & file, const std::string & options)
    {
      cv->cd();
      cv->Clear();
      cv->SetLogx(log_x);
      cv->SetLogy(log_y);
      cv->SetLogz(log_z);
      cv->SetLeftMargin(CaloPlotterHelper::s_canvas_left_margin);
      cv->SetRightMargin(CaloPlotterHelper::s_canvas_right_margin);
      cv->SetTopMargin(CaloPlotterHelper::s_canvas_top_margin);
      cv->SetBottomMargin(CaloPlotterHelper::s_canvas_bottom_margin);
      const auto before_stat = gStyle->GetOptStat();
      const auto before_title = gStyle->GetOptTitle();
      if (s_place_ATLAS_label)
        {
          gStyle->SetOptStat(0000);
          gStyle->SetOptTitle(s_place_title);

          if (s_yaxis_bins)
            {
              std::string title = gr->GetYaxis()->GetTitle();
              title += " / " + get_bin_label(gr);
              gr->GetYaxis()->SetTitle(title.c_str());
            }
          gr->SetMaximum(gr->GetMaximum() * s_extraspacefactor_multiply + s_extraspacefactor_add);
          gr->Draw("");
          ATLASLabel(s_labelstart_x + s_labeloffset_x, s_labelstart_y + s_labeloffset_y);
          extra_text_label(s_labelstart_x + s_extralabeloffset_x, s_labelstart_y + s_extralabeloffset_y);
        }
      else
        {
          gStyle->SetOptStat("ou");
          gStyle->SetOptTitle(s_place_title);
          gr->Draw("");
        }
      gr->GetXaxis()->SetTitleOffset(1.5);
      gr->GetYaxis()->SetTitleOffset(1.5);
      cv->SaveAs(file.c_str(), options.c_str());
      gStyle->SetOptStat(before_stat);
      gStyle->SetOptTitle(before_title);
    }

    void plot_one ATLAS_NOT_THREAD_SAFE (TCanvas * cv, TH2 * gr, const std::string & file, const std::string & options)
    {
      cv->cd();
      cv->Clear();
      cv->SetLogx(log_x);
      cv->SetLogy(log_y);
      cv->SetLogz(log_z);
      cv->SetLeftMargin(CaloPlotterHelper::s_canvas_left_margin);
      cv->SetRightMargin(CaloPlotterHelper::s_canvas_right_margin);
      cv->SetTopMargin(CaloPlotterHelper::s_canvas_top_margin);
      cv->SetBottomMargin(CaloPlotterHelper::s_canvas_bottom_margin);
      const auto before_stat = gStyle->GetOptStat();
      const auto before_title = gStyle->GetOptTitle();
      if (s_place_ATLAS_label)
        {
          gStyle->SetOptStat(0000);
        }
      else
        {
          gStyle->SetOptStat(0000);
        }
      gStyle->SetOptTitle(s_place_title);
      gr->Draw("colsz");
      gr->GetXaxis()->SetTitleOffset(1.5);
      gr->GetYaxis()->SetTitleOffset(1.5);
      gr->GetZaxis()->SetTitleOffset(1.5);
      cv->SaveAs(file.c_str(), options.c_str());
      gStyle->SetOptStat(before_stat);
      gStyle->SetOptTitle(before_title);
    }

    void plot_one ATLAS_NOT_THREAD_SAFE (TCanvas * cv, THStack * hs, const std::string & file, const std::string & options)
    {
      cv->cd();
      cv->Clear();
      cv->SetLogx(log_x);
      cv->SetLogy(log_y);
      cv->SetLogz(log_z);
      cv->SetLeftMargin(CaloPlotterHelper::s_canvas_left_margin);
      cv->SetRightMargin(CaloPlotterHelper::s_canvas_right_margin);
      cv->SetTopMargin(CaloPlotterHelper::s_canvas_top_margin);
      cv->SetBottomMargin(CaloPlotterHelper::s_canvas_bottom_margin);
      const auto before_stat = gStyle->GetOptStat();
      const auto before_title = gStyle->GetOptTitle();
      if (s_place_ATLAS_label)
        {
          gStyle->SetOptStat(0000);
        }
      else
        {
          gStyle->SetOptStat(0000);
        }
      gStyle->SetOptTitle(s_place_title);
      std::vector <Style_t> styles(hs->GetNhists());
      TIter iter(hs->GetHists());
      iter.Begin();
      for (int i = 0; iter != iter.End(); iter(), ++i)
        {
          TH1F * hist = (TH1F *) *iter;
          styles[i] = hist->GetLineStyle();
          hist->SetLineStyle(0);
          if (s_place_ATLAS_label)
            {
              if (s_yaxis_bins)
                {
                  std::string title = hist->GetYaxis()->GetTitle();
                  title += " / " + get_bin_label(hist);
                  hist->GetYaxis()->SetTitle(title.c_str());
                }
              hist->SetMaximum(hist->GetMaximum() * s_extraspacefactor_multiply + s_extraspacefactor_add);
            }
        }
      hs->Draw("nostack");
      hs->GetXaxis()->SetTitleOffset(1.5);
      hs->GetYaxis()->SetTitleOffset(1.5);
      if (s_place_ATLAS_label)
        {
          if (s_yaxis_bins)
            {
              std::string title = hs->GetYaxis()->GetTitle();
              title += " / " + get_bin_label(hs);
              hs->GetYaxis()->SetTitle(title.c_str());
            }
          TLegend * leg = gPad->BuildLegend(s_labelstart_x, s_labelstart_y, s_labelstart_x + s_labelsize_x, s_labelstart_y + s_labelsize_y, "", "l");
          if (s_place_ATLAS_label)
            {
              ATLASLabel(s_labelstart_x + s_labeloffset_x, s_labelstart_y + s_labeloffset_y);
              extra_text_label(s_labelstart_x + s_extralabeloffset_x, s_labelstart_y + s_extralabeloffset_y);
            }

          leg->SetBorderSize(0);
          leg->SetFillColor(0);
          leg->SetTextFont(42);
          leg->SetTextSize(0.0275);
        }
      else
        {
          gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
        }
      iter.Begin();
      for (int i = 0; iter != iter.End(); iter(), ++i)
        {
          TH1F * hist = (TH1F *) *iter;
          hist->SetLineStyle(styles[i]);
          styles[i] = hist->GetFillStyle();
          hist->SetFillStyle(0);
        }
      hs->Draw("nostack same");
      iter.Begin();
      for (int i = 0; iter != iter.End(); iter(), ++i)
        {
          TH1F * hist = (TH1F *) *iter;
          hist->SetFillStyle(styles[i]);
        }
      /*
        //This is an attempt for plot overlap to work
        //without transparency/patterns.
        //It kinda does, but it isn't pretty...
      std::vector<std::unique_ptr<TH1F>> temps;
      TIter iter(hs->GetHists());
      for (iter.Begin(); iter != iter.End(); iter() )
        {
      temps.emplace_back(std::make_unique<TH1F>(* ((TH1F *) *iter)));
      temps.back()->SetLineColorAlpha(0, 0.);
      //temps.back()->Rebin(, "", );
      for(int i = 1; i <= temps.back()->GetNbinsX(); ++i)
      {
        double min = temps.back()->GetBinContent(i);
        TIter other = iter;
        for (other.Begin(); other != iter; other())
          {
      TH1F * otherhistptr = (TH1F *) *other;
      min = std::min(min, otherhistptr->GetBinContent(i));
          }
        temps.back()->SetBinContent(i, min);
        //We could just set to 0,
        //but I'll keep like this
        //so we can check what happens
        //if we drop SetLineColorAlpha(0,0.)
      }
      temps.back()->Draw("same");
      }
      */
      gPad->Modified();
      gPad->Update();
      cv->Update();
      cv->SaveAs(file.c_str(), options.c_str());
      gStyle->SetOptStat(before_stat);
      gStyle->SetOptTitle(before_title);
    }

    void plot_one ATLAS_NOT_THREAD_SAFE (TCanvas * cv, TGraphErrors * gr, const std::string & file, const std::string & options)
    {
      cv->cd();
      cv->Clear();
      cv->SetLogx(log_x);
      cv->SetLogy(log_y);
      cv->SetLogz(log_z);
      cv->SetLeftMargin(CaloPlotterHelper::s_canvas_left_margin);
      cv->SetRightMargin(CaloPlotterHelper::s_canvas_right_margin);
      cv->SetTopMargin(CaloPlotterHelper::s_canvas_top_margin);
      cv->SetBottomMargin(CaloPlotterHelper::s_canvas_bottom_margin);
      const auto before_stat = gStyle->GetOptStat();
      const auto before_title = gStyle->GetOptTitle();
      if (s_place_ATLAS_label)
        {
          //gStyle->SetOptStat(0000);
          gStyle->SetOptTitle(s_place_title);

          gr->SetMaximum(gr->GetMaximum() * s_extraspacefactor_multiply + s_extraspacefactor_add);
          gr->Draw("ACP");
          ATLASLabel(s_labelstart_x + s_labeloffset_x, s_labelstart_y + s_labeloffset_y);
          extra_text_label(s_labelstart_x + s_extralabeloffset_x, s_labelstart_y + s_extralabeloffset_y);
        }
      else
        {
          //gStyle->SetOptStat(2200);
          gStyle->SetOptTitle(s_place_title);
          gr->Draw("ACP");
        }
      gr->GetXaxis()->SetTitleOffset(1.5);
      gr->GetYaxis()->SetTitleOffset(1.5);
      cv->SaveAs(file.c_str(), options.c_str());
      gStyle->SetOptStat(before_stat);
      gStyle->SetOptTitle(before_title);
    }

    void plot_one ATLAS_NOT_THREAD_SAFE (TCanvas * cv, TMultiGraph * mg, const std::string & file, const std::string & options)
    {
      cv->cd();
      cv->Clear();
      cv->SetLogx(log_x);
      cv->SetLogy(log_y);
      cv->SetLogz(log_z);
      cv->SetLeftMargin(CaloPlotterHelper::s_canvas_left_margin);
      cv->SetRightMargin(CaloPlotterHelper::s_canvas_right_margin);
      cv->SetTopMargin(CaloPlotterHelper::s_canvas_top_margin);
      cv->SetBottomMargin(CaloPlotterHelper::s_canvas_bottom_margin);
      const auto before_stat = gStyle->GetOptStat();
      const auto before_title = gStyle->GetOptTitle();
      /*
      if (s_place_ATLAS_label)
        {
          gStyle->SetOptStat(0000);
        }
      else
        {
          gStyle->SetOptStat(2200);
        }
      */
      gStyle->SetOptTitle(s_place_title);
      mg->Draw("ACP");
      mg->GetXaxis()->SetTitleOffset(1.5);
      mg->GetYaxis()->SetTitleOffset(1.5);
      if (s_place_ATLAS_label)
        {
          if (s_yaxis_bins)
            {
              std::string title = mg->GetYaxis()->GetTitle();
              title += " / " + get_bin_label(mg);
              mg->GetYaxis()->SetTitle(title.c_str());
            }
          TLegend * leg = gPad->BuildLegend(s_labelstart_x, s_labelstart_y, s_labelstart_x + s_labelsize_x, s_labelstart_y + s_labelsize_y, "", "l");
          if (s_place_ATLAS_label)
            {
              ATLASLabel(s_labelstart_x + s_labeloffset_x, s_labelstart_y + s_labeloffset_y);
              extra_text_label(s_labelstart_x + s_extralabeloffset_x, s_labelstart_y + s_extralabeloffset_y);
            }

          leg->SetBorderSize(0);
          leg->SetFillColor(0);
          leg->SetTextFont(42);
          leg->SetTextSize(0.0275);
        }
      else
        {
          gPad->BuildLegend(0.75, 0.75, 0.95, 0.95, "");
        }
      gPad->Modified();
      gPad->Update();
      cv->Update();
      cv->SaveAs(file.c_str(), options.c_str());
      gStyle->SetOptStat(before_stat);
      gStyle->SetOptTitle(before_title);
    }

    virtual ~base_plotter();
    //Declared out-of-line to ensure the vtable
    //ends up on a single translation unit...

    base_plotter() = default;
    base_plotter(const base_plotter &) = default;
    base_plotter(base_plotter &&) = default;
    base_plotter & operator= (const base_plotter &) = default;
    base_plotter & operator= (base_plotter &&) = default;
  };

  /** @class pointer_with_clone
      @brief Something that allows copying through a @p Clone() member function.
             (Relevant for Root classes.)
  */
  template <class T>
  struct pointer_with_clone
  {

   private:

    template<class X> inline static constexpr
    auto has_setdirectory_checker(X *) -> decltype(&X::SetDirectory);
    template<class X> inline static constexpr
    void has_setdirectory_checker(...);
    template <class X> inline static constexpr
    bool setdirectory_exists = std::is_member_pointer_v<decltype( has_setdirectory_checker<X>(nullptr) )>;

    //A SFINAE'd way to check if a type has the SetPointError member function,
    //to correctly handle TH and derived thereof with our own self-managed memory.

   public:

    std::unique_ptr<T> ptr = nullptr;

    pointer_with_clone(const pointer_with_clone & other):
      ptr(other.ptr ? (T *) other.ptr->Clone() : nullptr)
    {
      if constexpr (setdirectory_exists<T>)
        {
          if (ptr)
            {
              ptr->SetDirectory(nullptr);
            }
        }
    }

    pointer_with_clone(pointer_with_clone && other):
      ptr(other.ptr.release())
    {
    }

    pointer_with_clone(std::unique_ptr<T> && other):
      ptr( std::forward< std::unique_ptr<T> >(other) )
    {
    }

    pointer_with_clone(T * owning_ptr):
      ptr(owning_ptr)
    {
    }

    pointer_with_clone & operator =(std::unique_ptr<T> && other)
    {
      ptr = std::forward< std::unique_ptr<T> >(other);
      return (*this);
    }

    pointer_with_clone & operator =(const pointer_with_clone & other)
    {
      if (&other != this)
        {
          if (other.ptr)
            {
              ptr.reset((T *) other.ptr->Clone());
              if constexpr (setdirectory_exists<T>)
                {
                  ptr->SetDirectory(nullptr);
                }
            }
          else
            {
              ptr.reset(nullptr);
            }
        }
      return (*this);
    }

    pointer_with_clone & operator =(T * owning_ptr)
    {
      ptr.reset(owning_ptr);
      return (*this);
    }

    pointer_with_clone && operator = (pointer_with_clone && other)
    {
      ptr.swap(other.ptr);
      return (*this);
    }

    operator std::unique_ptr<T> && ()
    {
      return std::move(ptr);
    }

  };

  /** @class plotter_object_holder
      @brief Holds the actual plotter object.
  */
  template <class PlotObject>
  struct plotter_object_holder : public base_plotter
  {
    using PlotType = PlotObject;
    pointer_with_clone<PlotObject> obj = nullptr;

    void set_style(const PlotStyle & style) override
    {
      obj.ptr->SetLineColorAlpha(style.line.color, style.line.alpha);
      obj.ptr->SetLineStyle(style.line.style);
      obj.ptr->SetFillColorAlpha(style.fill.color, style.fill.alpha);
      obj.ptr->SetFillStyle(style.fill.style);
      obj.ptr->SetMarkerStyle(style.marker.style);
      obj.ptr->SetMarkerColorAlpha(style.marker.color, style.marker.alpha);
      obj.ptr->SetMarkerSize(style.marker.size);
    }
  };

  template <class PlotterType>
  using single_plotter_fill_t = void(typename PlotterType::PlotType *,
                                     const CaloRecGPU::ConstantDataHolder &,
                                     const CaloRecGPU::CellInfoArr &,
                                     const CaloRecGPU::CellStateArr &,
                                     const CaloRecGPU::ClusterInfoArr &,
                                     const CaloRecGPU::ClusterMomentsArr &);

  /** @class plotter_single
      @brief Makes plots for a single tool.
  */
  template <class PlotterType>
  struct plotter_single : public PlotterType
  {
    std::function<single_plotter_fill_t<PlotterType>> f;

    void add_data(const CaloRecGPU::ConstantDataHolder & constant_data,
                  const CaloRecGPU::CellInfoArr & cell_info,
                  const CaloRecGPU::CellStateArr & cell_state,
                  const CaloRecGPU::ClusterInfoArr & cluster_info,
                  const CaloRecGPU::ClusterMomentsArr & cluster_moments) override
    {
      f(PlotterType::obj.ptr.get(), constant_data, cell_info, cell_state, cluster_info, cluster_moments);
    }

    void add_data(const CaloRecGPU::ConstantDataHolder & /*constant_data*/,
                  const CaloRecGPU::CellInfoArr & /*cell_info*/,
                  const CaloRecGPU::CellStateArr & /*cell_state_1*/,
                  const CaloRecGPU::CellStateArr & /*cell_state_2*/,
                  const CaloRecGPU::ClusterInfoArr & /*cluster_info_1*/,
                  const CaloRecGPU::ClusterInfoArr & /*cluster_info_2*/,
                const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_1*/,
                const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_2*/,
                  const sample_comparisons_holder & /*comparisons*/) override
    {
      //f(PlotterType::obj.get(), constant_data, cell_info, cell_state_1, cluster_info_1, cluster_moments_1);
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Using single plot as combined. (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

  };

  template <class PlotterType>
  using combined_plotter_fill_t = void(typename PlotterType::PlotType *,
                                       const CaloRecGPU::ConstantDataHolder &,
                                       const CaloRecGPU::CellInfoArr &,
                                       const CaloRecGPU::CellStateArr &,
                                       const CaloRecGPU::CellStateArr &,
                                       const CaloRecGPU::ClusterInfoArr &,
                                       const CaloRecGPU::ClusterInfoArr &,
                                       const CaloRecGPU::ClusterMomentsArr &,
                                       const CaloRecGPU::ClusterMomentsArr &,
                                       const sample_comparisons_holder &);

  /** @class plotter_combined
      @brief Makes plots that compare two tools.
  */
  template <class PlotterType>
  struct plotter_combined : public PlotterType
  {
    std::function<combined_plotter_fill_t<PlotterType>> f;

    void add_data(const CaloRecGPU::ConstantDataHolder & /*constant_data*/,
                  const CaloRecGPU::CellInfoArr & /*cell_info*/,
                  const CaloRecGPU::CellStateArr & /*cell_state*/,
                  const CaloRecGPU::ClusterInfoArr & /*cluster_info*/,
                  const CaloRecGPU::ClusterMomentsArr & /*cluster_moments*/) override
    {
      //f(PlotterType::obj.get(), constant_data, cell_info, cell_state, cell_state, cluster_info, cluster_info, cluster_moments, cluster_moments, {});
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Using combined plot as single. (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

    void add_data(const CaloRecGPU::ConstantDataHolder & constant_data,
                  const CaloRecGPU::CellInfoArr & cell_info,
                  const CaloRecGPU::CellStateArr & cell_state_1,
                  const CaloRecGPU::CellStateArr & cell_state_2,
                  const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                  const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                  const CaloRecGPU::ClusterMomentsArr & cluster_moments_1,
                  const CaloRecGPU::ClusterMomentsArr & cluster_moments_2,
                  const sample_comparisons_holder & comparisons) override
    {
      f(PlotterType::obj.ptr.get(), constant_data, cell_info, cell_state_1, cell_state_2,
        cluster_info_1, cluster_info_2, cluster_moments_1, cluster_moments_2, comparisons);
    }

  };

  /** @class plotter_other
      @brief Other kinds of plots that do not depend on event information this way.
             (Used for e. g. time plots.)
  */
  template <class PlotterType>
  struct plotter_other : public PlotterType
  {
    void add_data(const CaloRecGPU::ConstantDataHolder & /*constant_data*/,
                  const CaloRecGPU::CellInfoArr & /*cell_info*/,
                  const CaloRecGPU::CellStateArr & /*cell_state*/,
                  const CaloRecGPU::ClusterInfoArr & /*cluster_info*/,
                  const CaloRecGPU::ClusterMomentsArr & /*cluster_moments*/) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Using other plot as single. (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

    void add_data(const CaloRecGPU::ConstantDataHolder & /*constant_data*/,
                  const CaloRecGPU::CellInfoArr & /*cell_info*/,
                  const CaloRecGPU::CellStateArr & /*cell_state_1*/,
                  const CaloRecGPU::CellStateArr & /*cell_state_2*/,
                  const CaloRecGPU::ClusterInfoArr & /*cluster_info_1*/,
                  const CaloRecGPU::ClusterInfoArr & /*cluster_info_2*/,
                  const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_1*/,
                  const CaloRecGPU::ClusterMomentsArr & /*cluster_moments_2*/,
                  const sample_comparisons_holder & comparisons) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Using other plot as combined. (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

  };


  /** @class joined_plotter_H1D
      @brief Plots 1D histograms together.
  */
  struct joined_plotter_H1D;

  /** @class plotter_H1D
      @brief Plots 1D histograms. Expects the @p PlotterType to hold an appropriate 1D histogram.
             (Could benefit from some compile-time checking, perhaps?)
  */
  template <class PlotterType>
  struct plotter_H1D : public PlotterType
  {
    ///If @p cumulative is 0, normalize by the constant specified by the normalization kind.
    normalization_constants::NormalizationKind normalize = normalization_constants::NormalizationKind::None;

    /** If 0, standard histogram (possibly normalized).
     *  If positive, integrates from 0 to infinity.
     *  If negative, integrates from infinity to 0.
     */
    int cumulative = 0;

    void append_value(const double x) override
    {
      PlotterType::obj.ptr->Fill(x);
    }

    void append_value(const double x, const double w) override
    {
      PlotterType::obj.ptr->Fill(x, w);
      //The second value is used as a weight here
    }

    void append_value(const double, const double, const double) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Giving too many values (3) to add to 1D histogram plotter (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

    void append_value(const double, const double, const double, const double) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Giving too many values (4) to add to 1D histogram plotter (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

    void initialize_data() override
    {
      PlotterType::obj.ptr = std::make_unique<typename PlotterType::PlotType>
                             (PlotterType::plot_name.c_str(),
                              (PlotterType::title + ";" + PlotterType::axis_x + ";" + PlotterType::axis_y).c_str(),
                              PlotterType::bin_x, PlotterType::x_min, PlotterType::x_max
                             );
      PlotterType::obj.ptr->SetDirectory(nullptr);
    }

   private:

    void apply_normalization(const normalization_constants & nc)
    {
      if (cumulative != 0 && PlotterType::obj.ptr->Integral() != 0)
        {
          PlotterType::obj.ptr->Sumw2(false);
          std::unique_ptr<typename PlotterType::PlotType> temp((typename PlotterType::PlotType *) PlotterType::obj.ptr->GetCumulative(cumulative > 0));
          temp->Scale(1. / PlotterType::obj.ptr->Integral());
          PlotterType::obj.ptr.swap(temp);
          PlotterType::obj.ptr->Sumw2(false);
        }
      else
        {
          const double factor = nc.get_factor(normalize);
          if (factor != 0)
            {
              PlotterType::obj.ptr->Scale(1. / factor);
              PlotterType::obj.ptr->Sumw2(false);
            }
        }
    }

   public:


    void plot ATLAS_NOT_THREAD_SAFE (TCanvas * cv, const normalization_constants & nc, const std::string & name, const std::string & options) override
    {
      this->apply_normalization(nc);
      this->plot_one(cv, PlotterType::obj.ptr.get(), name, options);
    }

    virtual std::unique_ptr<base_plotter> clone() const override
    {
      return std::make_unique<plotter_H1D<PlotterType>>(*this);
    }

    std::unique_ptr<joined_plotter> make_joined_plotter() override
    {
      auto ptr = std::make_unique<joined_plotter_H1D>(*this);
      ptr->initialize_data();
      return ptr;
    }

    void add_to_joined_plotter(joined_plotter * joined, const normalization_constants & nc)
    {
      this->apply_normalization(nc);
      joined_plotter_H1D * ptr = dynamic_cast<joined_plotter_H1D *>(joined);
      if (ptr)
        {
          ptr->getHStack().Add(PlotterType::obj.ptr.get());
          //HStack does not own!
        }
    }

  };

  /** @class joined_plotter_dummy
      @brief Does not join plots (used for 2D histograms).
  */
  struct joined_plotter_dummy;

  /** @class plotter_H2D
      @brief Plots 2D histograms. Expects the @p PlotterType to hold an appropriate 2D histogram.
             (Could benefit from some compile-time checking, perhaps?)
  */
  template <class PlotterType>
  struct plotter_H2D : public PlotterType
  {
    normalization_constants::NormalizationKind normalize = normalization_constants::NormalizationKind::None;

    void append_value(const double) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Giving too little values (1) to add to 2D histogram plotter (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

    void append_value(const double x, const double y) override
    {
      PlotterType::obj.ptr->Fill(x, y);
    }

    void append_value(const double x, const double y, const double w) override
    {
      PlotterType::obj.ptr->Fill(x, y, w);
      //The third value is used as a weight here
    }

    void append_value(const double, const double, const double, const double) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Giving too many values (4) to add to 2D histogram plotter (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

    void initialize_data() override
    {
      PlotterType::obj.ptr = std::make_unique<typename PlotterType::PlotType>
                             (PlotterType::plot_name.c_str(),
                              (PlotterType::title + ";" + PlotterType::axis_x + ";" + PlotterType::axis_y + ";" + PlotterType::axis_z).c_str(),
                              PlotterType::bin_x, PlotterType::x_min, PlotterType::x_max, PlotterType::bin_y, PlotterType::y_min, PlotterType::y_max);
      PlotterType::obj.ptr->SetDirectory(nullptr);
    }

   private:

    void apply_normalization(const normalization_constants & nc)
    {
      const double factor = nc.get_factor(normalize);
      if (factor != 0)
        {
          PlotterType::obj.ptr->Scale(1. / factor);
          PlotterType::obj.ptr->Sumw2(false);
        }
    }

   public:

    void plot ATLAS_NOT_THREAD_SAFE (TCanvas * cv, const normalization_constants & nc, const std::string & name, const std::string & options) override
    {
      this->apply_normalization(nc);
      this->plot_one(cv, PlotterType::obj.ptr.get(), name, options);
    }

    virtual std::unique_ptr<base_plotter> clone() const override
    {
      return std::make_unique<plotter_H2D<PlotterType>>(*this);
    }

    std::unique_ptr<joined_plotter> make_joined_plotter() override
    {
      return std::make_unique<joined_plotter_dummy>(*this);
    }

    void add_to_joined_plotter(joined_plotter * /*joined*/, const normalization_constants & /*nc*/) override
    {
      //Do nothing.
      //Nothing can be done with 2D histograms...
    }
  };

  /** @class joined_plotter_graph1D
      @brief Plots 1D graphs together.
  */
  struct joined_plotter_graph1D;

  /** @class plotter_graph1D
      @brief Plots 1D graphs. Expects the @p PlotterType to hold an appropriate 1D graph.
             (Could benefit from some compile-time checking, perhaps?)
  */
  template <class PlotterType>
  struct plotter_graph1D : public PlotterType
  {
   private:

    template<class T> inline static constexpr
    auto has_seterror_checker(T *) -> decltype(&T::SetPointError);
    template<class T> inline static constexpr
    void has_seterror_checker(...);
    template <class T> inline static constexpr
    bool seterror_exists = std::is_member_pointer_v<decltype( has_seterror_checker<T>(nullptr) )>;

    //A SFINAE'd way to check if a type has the SetPointError member function.
    //For allowing usage of both TGraph and TGraphErrors.
    //Since this will require an if constexpr, we need at least C++17...
    //[It would be possible to work around this in earlier versions,
    // even if much more clunkily... but we use C++17 elsewhere in the plotter,
    // such as for parameter pack expansion/fold expressions and so on.]

   public:

    void append_value(const double) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: Giving too little values (1) to add to 1D graph plotter (" << PlotterType::plot_name << ")." << std::endl;
        }
    }

    void append_value(const double x, const double y) override
    {
      PlotterType::obj.ptr->AddPoint(x, y);
    }

    void append_value([[maybe_unused]] const double x,
                      [[maybe_unused]] const double y,
                      [[maybe_unused]] const double ey ) override
    {
      if constexpr(seterror_exists<PlotterType>)
        {
          PlotterType::obj.ptr->AddPoint(x, y);
          PlotterType::obj.ptr->SetPointError(PlotterType::obj.ptr->GetN() - 1, 0, ey);
          //The third value is used as a error in y here
        }
      else
        {
          if (CaloPlotterHelper::s_print_messages)
            {
              std::cout << "WARNING: Giving too many values (3) to add to 1D graph plotter (" << PlotterType::plot_name << ")." << std::endl;
            }
        }
    }

    void append_value([[maybe_unused]] const double x,
                      [[maybe_unused]] const double y,
                      [[maybe_unused]] const double ex,
                      [[maybe_unused]] const double ey ) override
    {
      if constexpr(seterror_exists<PlotterType>)
        {
          PlotterType::obj.ptr->AddPoint(x, y);
          PlotterType::obj.ptr->SetPointError(PlotterType::obj.ptr->GetN() - 1, ex, ey);
          //The third value is used as a error in y here
        }
      else
        {
          if (CaloPlotterHelper::s_print_messages)
            {
              std::cout << "WARNING: Giving too many values (4) to add to 1D graph plotter (" << PlotterType::plot_name << ")." << std::endl;
            }
        }
    }

    void initialize_data() override
    {
      PlotterType::obj.ptr = std::make_unique<typename PlotterType::PlotType>();
      PlotterType::obj.ptr->SetName(PlotterType::plot_name.c_str());
      PlotterType::obj.ptr->SetTitle((PlotterType::title + ";" + PlotterType::xis_x + ";" + PlotterType::axis_y).c_str());
      PlotterType::obj.ptr->SetMinimum(PlotterType::x_min);
      PlotterType::obj.ptr->SetMaximum(PlotterType::x_max);
    }

    void plot ATLAS_NOT_THREAD_SAFE (TCanvas * cv, const normalization_constants & /*nc*/, const std::string & name, const std::string & options) override
    {
      this->plot_one(cv, PlotterType::obj.ptr.get(), name, options);
    }

    virtual std::unique_ptr<base_plotter> clone() const override
    {
      return std::make_unique<plotter_graph1D<PlotterType>>(*this);
    }

    std::unique_ptr<joined_plotter> make_joined_plotter() override
    {
      auto ptr = std::make_unique<joined_plotter_graph1D>(*this);
      ptr->initialize_data();
      return ptr;
    }

    void add_to_joined_plotter(joined_plotter * joined, const normalization_constants & /*nc*/) override
    {
      joined_plotter_graph1D * ptr = dynamic_cast<joined_plotter_graph1D *>(joined);
      if (ptr)
        {
          ptr->getMultiGraph().Add(PlotterType::obj.ptr->Clone());
          //MultiGraph OWNS!
        }
    }
  };

  struct joined_plotter : public base_plotter
  {
    void append_value(const double) override
    {
      //Do nothing.
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: trying to add values to a joined plotter (" << plot_name << ")!" << std::endl;
        }
    }

    void append_value(const double, const double) override
    {
      //Do nothing.
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: trying to add values to a joined plotter(" << plot_name << ")!" << std::endl;
        }
    }

    void append_value(const double, const double, const double) override
    {
      //Do nothing.
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: trying to add values to a joined plotter(" << plot_name << ")!" << std::endl;
        }
    }

    void append_value(const double, const double, const double, const double) override
    {
      //Do nothing.
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: trying to add values to a joined plotter(" << plot_name << ")!" << std::endl;
        }
    }

    void add_data(const CaloRecGPU::ConstantDataHolder &,
                  const CaloRecGPU::CellInfoArr &,
                  const CaloRecGPU::CellStateArr &,
                  const CaloRecGPU::ClusterInfoArr &,
                  const CaloRecGPU::ClusterMomentsArr &) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: trying to add data to a joined plotter(" << plot_name << ")!" << std::endl;
        }
    }

    void add_data(const CaloRecGPU::ConstantDataHolder &,
                  const CaloRecGPU::CellInfoArr &,
                  const CaloRecGPU::CellStateArr &,
                  const CaloRecGPU::CellStateArr &,
                  const CaloRecGPU::ClusterInfoArr &,
                  const CaloRecGPU::ClusterInfoArr &,
                  const CaloRecGPU::ClusterMomentsArr &,
                  const CaloRecGPU::ClusterMomentsArr &,
                  const sample_comparisons_holder &) override
    {
      if (CaloPlotterHelper::s_print_messages)
        {
          std::cout << "WARNING: trying to add data to a joined plotter(" << plot_name << ")!" << std::endl;
        }
    }

    void set_style(const PlotStyle &) override
    {
      //Do nothing: the individual plots hold the styles.
    }

    template <class PlotterType>
    joined_plotter(const PlotterType & plotter): base_plotter(plotter)
    {
    }

  };

  struct joined_plotter_H1D : public joined_plotter
  {
    pointer_with_clone<THStack> obj = nullptr;

    void plot ATLAS_NOT_THREAD_SAFE (TCanvas * cv, const normalization_constants & /*nc*/, const std::string & name, const std::string & options) override
    {
      this->plot_one(cv, obj.ptr.get(), name, options);
    }

    virtual void initialize_data()
    {
      obj.ptr = std::make_unique<THStack>((plot_name + "_stack").c_str(), (title + ";" + axis_x + ";" + axis_y).c_str());
    }

    virtual void add_to_joined_plotter(joined_plotter * joined, const normalization_constants & /*nc*/) override
    {
      joined_plotter_H1D * ptr = dynamic_cast<joined_plotter_H1D *>(joined);
      if (ptr)
        {
          for (TIter iter = TIter(obj.ptr->GetHists()).Begin(); iter != iter.End(); iter())
            {
              ptr->getHStack().Add((TH1 *) *iter);
              //HStack does not own!
            }
        }
    }

    virtual std::unique_ptr<base_plotter> clone() const override
    {
      return std::make_unique<joined_plotter_H1D>(*this);
    }

    const THStack & getHStack() const
    {
      return *(obj.ptr);
    }

    THStack & getHStack()
    {
      return *(obj.ptr);
    }

    template <class PlotterType>
    joined_plotter_H1D(const plotter_H1D<PlotterType> & plotter): joined_plotter(plotter)
    {
    }

    virtual std::unique_ptr<joined_plotter> make_joined_plotter() override
    {
      return std::make_unique<joined_plotter_H1D>(*this);
    }
  };

  struct joined_plotter_dummy : public joined_plotter
  {
    void plot ATLAS_NOT_THREAD_SAFE (TCanvas * /*cv*/, const normalization_constants & /*nc*/, const std::string & /*name*/, const std::string & /*options*/) override
    //This one technically is thread safe, but OK...
    {
    }

    virtual void initialize_data()
    {
    }

    virtual void add_to_joined_plotter(joined_plotter * /*joined*/, const normalization_constants & /*nc*/) override
    {
    }

    virtual std::unique_ptr<base_plotter> clone() const override
    {
      return std::make_unique<joined_plotter_dummy>(*this);
    }

    template <class PlotterType>
    joined_plotter_dummy(const PlotterType & plotter): joined_plotter(plotter)
    {
    }

    virtual std::unique_ptr<joined_plotter> make_joined_plotter() override
    {
      return std::make_unique<joined_plotter_dummy>(*this);
    }
  };

  struct joined_plotter_graph1D : public joined_plotter
  {
    pointer_with_clone<TMultiGraph> obj = nullptr;

    void plot ATLAS_NOT_THREAD_SAFE (TCanvas * cv, const normalization_constants & /*nc*/, const std::string & name, const std::string & options) override
    {
      this->plot_one(cv, obj.ptr.get(), name, options);
    }

    virtual void initialize_data()
    {
      obj.ptr = std::make_unique<TMultiGraph>((plot_name + "_multi").c_str(), (title + ";" + axis_x + ";" + axis_y).c_str());
    }

    virtual void add_to_joined_plotter(joined_plotter * joined, const normalization_constants & /*nc*/) override
    {
      joined_plotter_graph1D * ptr = dynamic_cast<joined_plotter_graph1D *>(joined);
      if (ptr)
        {
          ptr->getMultiGraph().Add(obj.ptr.get());
        }
    }

    virtual std::unique_ptr<base_plotter> clone() const override
    {
      return std::make_unique<joined_plotter_graph1D>(*this);
    }

    const TMultiGraph & getMultiGraph() const
    {
      return *(obj.ptr);
    }

    TMultiGraph & getMultiGraph()
    {
      return *(obj.ptr);
    }

    template <class PlotterType>
    joined_plotter_graph1D(const plotter_graph1D<PlotterType> & plotter): joined_plotter(plotter)
    {
    }

    virtual std::unique_ptr<joined_plotter> make_joined_plotter() override
    {
      return std::make_unique<joined_plotter_graph1D>(*this);
    }
  };

 private:

  template <class T, template <class> class ... Types> struct plot_types_helper;

  template <class T>
  struct plot_types_helper<T>
  {
    using type = T;
  };

  template <class T, template <class> class Type>
  struct plot_types_helper<T, Type>
  {
    using type = Type<T>;
  };

  template <class T, template <class> class Type, template <class> class ... Others>
  struct plot_types_helper<T, Type, Others...>
  {
    using type = typename plot_types_helper<Type<T>, Others...>::type;
  };

 public:

  template <class HistType, template <class> class FillType, template <class> class PlotType>
  using plot_type = typename plot_types_helper<plotter_object_holder<HistType>, FillType, PlotType>::type;

 private:

  ///A map of other plot definitions beyond the ones programmatically constructed by default..
  std::map<std::string, std::unique_ptr<base_plotter>> m_extra_plot_descriptions ATLAS_THREAD_SAFE;
  //We need this to be ordered so one can use a lexicographical compare
  //to allow a limited form of globbing in the end.
  //(Should futurely be expanded to a more general form?)

 public:

  void add_generic_plot_description(const base_plotter & plot)
  {
    m_extra_plot_descriptions[plot.plot_name] = plot.clone();
  }

  void add_generic_plot_description(const base_plotter * plot)
  {
    m_extra_plot_descriptions[plot->plot_name] = plot->clone();
  }

  void add_generic_plot_description(const std::unique_ptr<base_plotter> & plot)
  {
    m_extra_plot_descriptions[plot->plot_name] = plot->clone();
  }

  struct GetPlotDescriptionsFilter
  {
    bool plot_1D = true;
    bool plot_2D = true;
    bool plot_single = true;
    bool plot_combined = true;

    bool plot_default = true;
    bool plot_extra = true;

    //For extra plots,
    //we can't make the 1D, 2D, single and combined distinction...
  };

 private:

  inline constexpr static GetPlotDescriptionsFilter s_default_filter = {true, true, true, true, true, true};
  //Note: GCC/Clang (and others?) bug:
  //"default member initializer for 'CaloPlotterHelper::GetPlotDescriptionsFilter::<member>' required before the end of its enclosing class"
  //prevents us to simply use the default initializer as the default value.

 public:

  /** Gets all plot descriptions that match the name, with an optional
   *  `*` allowing for partial matching in the end of the name.
   */
  std::vector<std::unique_ptr<CaloPlotterHelper::base_plotter>> get_plot_descriptions(const std::string & name,
                                                                                      const GetPlotDescriptionsFilter & filter = s_default_filter) const;

  /** Gets the plot names corresponding to all plot description that match the @p name,
   *  with an optional @p filter.
   */
  std::vector<std::string> get_plot_names(const std::string & name, const GetPlotDescriptionsFilter & filter = s_default_filter) const;

  /** Adds the plot(s) identified by @p name to the @p plot_list,
   *  setting the ranges, bins, prefixes and suffixes according to the arguments.
   *  Returns the number of plots that have been added.
   */
  size_t add_to_plots(std::vector<std::unique_ptr<base_plotter>> & plot_list,
                      const std::string & name,
                      const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                      const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                      const std::string prefix = "", const std::string suffix = "",
                      const GetPlotDescriptionsFilter & filter = s_default_filter) const;

  /**Adds the plot(s) identified by @p name to the @p plot_list,
     setting the ranges, bins, prefixes and suffixes according to the arguments.
     Returns the number of plots that have been added.
   */
  size_t add_to_plots(std::map<std::string, std::unique_ptr<base_plotter>> & plot_list,
                      const std::string & name,
                      const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                      const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                      const std::string prefix = "", const std::string suffix = "",
                      const GetPlotDescriptionsFilter & filter = s_default_filter) const;

 private:

  struct per_tool_storage
  {
    CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> cell_state;
    CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> clusters;
    CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> moments;
    bool added = false;
    std::vector<std::unique_ptr<base_plotter>> plots;
    normalization_constants normalization;

    per_tool_storage() = default;
    per_tool_storage(per_tool_storage &&) = default;
    per_tool_storage & operator= (per_tool_storage &&) = default;
  };

  struct per_combination_storage
  {
    std::vector<std::unique_ptr<base_plotter>> plots;
    normalization_constants normalization;
    bool match_in_energy = false;

    per_combination_storage() = default;
    per_combination_storage(per_combination_storage &&) = default;
    per_combination_storage & operator= (per_combination_storage &&) = default;

    per_combination_storage(const bool match):
      match_in_energy(match)
    {
    }
  };

  ///Holds a string that will identify each tool in filenames.
  std::vector<std::string> m_tool_strings;

  ///Holds the relevant per-tool information.
  std::vector<per_tool_storage> m_per_tool_store;

  ///Holds the relevant per-combination information.
  std::map<std::pair<int, int>, per_combination_storage> m_per_combination_store;

  /** Maps every tool to the tools to which it will be compared
   *  (and vice-versa). The tools in which it is a second element
   *  are signalled by setting their indices to negative.
   *  Tools here are used as one-indexed to simplify logic.
   */
  std::vector<std::set<int>> m_tool_connections;

  ///Any relevant output one might wish to do for every event.
  std::string m_per_event_message;

  struct together_plots_information
  {
    ///Indices that specify the plot to be done at each of the tools/combinations
    ///(or the plots to be done at the single tool/combination).
    std::vector<size_t> to_plot;

    ///Filename for the plots.
    std::string filename;

    ///The styles to apply for each tool or combination or plot.
    ///(We assume the vector has the same length
    /// as the number of tools/combinations/plots to plot.)
    std::vector<PlotStyle> styles;

    ///Title to use when multiple plots for the same tool or combination are plotted.
    std::string single_plottitle;
  };

  ///Tools to plot together, and the prefixes and suffixes to use for these plots.
  ///If only a tool is provided, the plots in the @p together_plots_information
  ///are all plotted together with the filename and plot title specified there.
  std::vector < std::pair<std::vector<int>, together_plots_information> > m_tools_together;

  ///Combinations to plot together, and the prefixes and suffixes to use for these plots.
  ///If only a combination is provided, the plots in the @p together_plots_information
  ///are all plotted together with the filename and plot title specified there.
  std::vector < std::pair< std::vector<std::pair<int, int>>, together_plots_information > > m_combinations_together;

  ///Extensions and options for saving the plots.
  std::vector<std::pair<std::string, std::string>> m_extensions_options;


 public:

  /** Adds @p num_new_tools more tools to be plotted (i. e. increases the maximum possible tool index by @p num_new_tools).
  */
  void add_tools(const size_t num_new_tools);

  /** Adds the combinations between @p tool_1 (taken as reference) and @p tool_2 (taken as test) to be plotted.
      If any of the tools is outside of the range of the currently existing tools, we increase it to fit.
      If @p match_in_energy is `true`, clusters are matched based on cell energy rather than signal-to-noise ratio,
      which should work better for post-splitter clusters.
  */
  void add_tool_combination(const int tool_1, const int tool_2, const bool match_in_energy = false);


  /** Sets the corresponding string to identify the tools in the plot file names.
  */
  void set_tool_string(const int tool, const std::string & new_string);

  CaloPlotterHelper(const size_t num_tools);

  /**Adds the plot(s) identified by @p name to the plots to be done with @p tool,
     setting the ranges, bins, prefixes and suffixes according to the arguments.
     Returns the number of plots that have been added.

     @warning We assume the tool exists!
   */
  size_t add_to_plots(const int tool,
                      const std::string & name,
                      const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                      const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                      const std::string prefix = "", const std::string suffix = "",
                      const GetPlotDescriptionsFilter & filter = s_default_filter);


  /**Adds the plot(s) identified by @p name to the plots to be done with @p tool_combination,
     setting the ranges, bins, prefixes and suffixes according to the arguments.
     Returns the number of plots that have been added.

     @warning We assume the tool combination exists!
   */
  size_t add_to_plots(const std::pair<int, int> & tool_combination,
                      const std::string & name,
                      const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                      const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                      const std::string prefix = "", const std::string suffix = "",
                      const GetPlotDescriptionsFilter & filter = s_default_filter);

  /**Adds the plot(s) identified by @p name to the plots to be done for all individual tools,
     setting the ranges, bins, prefixes and suffixes according to the arguments.
     Returns the number of plots that have been added to each tool.
   */
  size_t add_to_single_plots(const std::string & name,
                             const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                             const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                             const std::string prefix = "", const std::string suffix = "",
                             const GetPlotDescriptionsFilter & filter = s_default_filter);

  /**Adds the plot(s) identified by @p name to the plots to be done for all tool combinations,
     setting the ranges, bins, prefixes and suffixes according to the arguments.
     Returns the number of plots that have been added to each combination.
   */
  size_t add_to_combined_plots(const std::string & name,
                               const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                               const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                               const std::string prefix = "", const std::string suffix = "",
                               const GetPlotDescriptionsFilter & filter = s_default_filter);

  /** Add a set of @p tools to have each of the plots corresponding to @p name plotted together, saving the file
      with an optional @p prefix and @p suffix and assigning the corresponding @p styles to each tool.
      All @p tools must be valid and the @p styles will be filled by incrementing the colours from the end
      if the vector is shorter than the list of @p tools.
  */
  void add_together_tools(const std::vector<int> & tools, const std::string & name,
                          const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                          const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                          const std::string & prefix = "", const std::string & suffix = "_joined",
                          const std::vector<PlotStyle> & styles = {},
                          const GetPlotDescriptionsFilter & filter = s_default_filter               );

  /** Add a set of @p plots to be plotted together for a single @p tool, with a specific @p title,
      saving the file with a specific @p file_name, and assigning the corresponding @p styles to each plot.
      The @p tool must be valid and the @p styles will be filled by incrementing the colours from the end
      if the vector is shorter than the list of @p plots.
  */
  void add_together_tool_plots(const int tool, const std::vector<std::string> & plots,
                               const std::string & title, const std::string & file_name,
                               const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                               const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                               const std::vector<PlotStyle> & styles = {},
                               const GetPlotDescriptionsFilter & filter = s_default_filter            );

  /** Add a set of @p tool_combinations to have each of the plots corresponding to @p name plotted together, saving the file
      with an optional @p prefix and @p suffix and assigning the corresponding @p styles to each tool.
      All @p tool_combinations must be valid and the @p styles will be filled by incrementing the colours from the end
      if the vector is shorter than the list of @p tools.
  */
  void add_together_combinations(const std::vector< std::pair<int, int> > & tool_combinations,
                                 const std::string & name,
                                 const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                                 const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                                 const std::string & prefix = "", const std::string & suffix = "_joined",
                                 const std::vector<PlotStyle> & styles = {},
                                 const GetPlotDescriptionsFilter & filter = s_default_filter                 );

  /** Add a set of @p plots to be plotted together for a single @p tool_combination, with a specific @p title,
      saving the file with a specific @p file_name, and assigning the corresponding @p styles to each plot.
      The @p tool_combination must be valid and the @p styles will be filled by incrementing the colours from the end
      if the vector is shorter than the list of @p plots.
  */
  void add_together_combination_plots(const std::pair<int, int> & tool_combination,
                                      const std::vector<std::string> & plots,
                                      const std::string & title, const std::string & file_name,
                                      const double x_min = 0, const double x_max = 0, const int x_bins = 1,
                                      const double y_min = 0, const double y_max = 0, const int y_bins = 1,
                                      const std::vector<PlotStyle> & styles = {},
                                      const GetPlotDescriptionsFilter & filter = s_default_filter                    );

  /** Adds a new extension under which the plots should be saved, with an optional parameter
      that corresponds to the options to be passed to `TCanvas::SaveAs`
      (useful for e. g. the `"EmbedFonts"` option for saving as `.pdf`).
  */
  void add_extension(const std::string & extension, const std::string & option = "");


  /// Returns a vector of pairs of extensions and the options to apply for each of them.
  const std::vector<std::pair<std::string, std::string>> & get_extensions() const
  {
    return m_extensions_options;
  }

  /// Returns a vector of pairs of extensions and the options to apply for each of them.
  std::vector<std::pair<std::string, std::string>> & get_extensions()
  {
    return m_extensions_options;
  }

  const std::string & get_per_event_message() const
  {
    return m_per_event_message;
  }

  void setup_new_event();

  void finish_event();

  void add_event_data(const int tool_number,
                      const CaloRecGPU::ConstantDataHolder & constant_data,
                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & cell_info,
                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & cell_state,
                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & clusters,
                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & moments);

  void save_plots(const std::string & path, const std::string & prefix, const std::string & suffix);

  /** Initialize all plotters to be ready to receive data.
  */
  void initialize();

  /** Initialize all plotters to be ready to receive data and set them to the @p style.
  */
  void initialize(const PlotStyle & style);

  /** Initialize all plotters to be ready to receive data,
   *  set the single plots to @p single_style and the combined to @ p combined_style.
   */
  void initialize(const PlotStyle & single_style, const PlotStyle & combined_style);

 private:

  double m_term_threshold = 0.;
  double m_grow_threshold = 2.;
  double m_seed_threshold = 4.;

  double m_min_similarity = 0.5;
  double m_term_weight = 1.;
  double m_grow_weight = 250.;
  double m_seed_weight = 5000.;


 public:

  void set_matching_options(const double term_threshold,       const double grow_threshold,   const double seed_threshold,
                            const double min_similarity = 0.75, const double term_weight = 1., const double grow_weight = 250.,
                            const double seed_weight = 5000.);

  ///Matches the clusters and returns the string that identifies the comparisons.
  ///Uses the modified version of Gale-Shapley algorithm.
  void match_clusters(sample_comparisons_holder & sch,
                      const CaloRecGPU::ConstantDataHolder & constant_data,
                      const CaloRecGPU::CellInfoArr & cell_info,
                      const CaloRecGPU::CellStateArr & cell_state_1,
                      const CaloRecGPU::CellStateArr & cell_state_2,
                      const CaloRecGPU::ClusterInfoArr & cluster_info_1,
                      const CaloRecGPU::ClusterInfoArr & cluster_info_2,
                      const CaloRecGPU::ClusterMomentsArr & cluster_moments_1,
                      const CaloRecGPU::ClusterMomentsArr & cluster_moments_2,
                      const bool match_in_energy);

};



#endif //CALORECGPU_CALOGPUDEFAULTPLOTTERHELPER_H