//
//
// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#include "CaloGPUDefaultPlotter.h"
#include "CaloRecGPU/Helpers.h"
#include "CaloRecGPU/CUDAFriendlyClasses.h"
#include "StoreGate/DataHandle.h"
#include "CaloUtils/CaloClusterCollectionProcessor.h"
#include "CaloRecGPU/CaloClusterGPUProcessor.h"

#include <map>
#include <numeric>
#include <algorithm>

#include "CaloGPUDefaultPlotterHelper.h"

using namespace CaloRecGPU;

CaloGPUDefaultPlotter::CaloGPUDefaultPlotter(const std::string & type, const std::string & name, const IInterface * parent):
  AthAlgTool(type, name, parent)
{
  declareInterface<ICaloClusterGPUPlotter> (this);
}

StatusCode CaloGPUDefaultPlotter::initialize()
{
  ATH_CHECK( m_cellsKey.value().initialize() );

  ATH_CHECK( detStore()->retrieve(m_calo_id, "CaloCell_ID") );

  const std::string this_name = this->name();

  const std::string algorithm_name_prefix = this_name.substr(0, this_name.rfind('.'));
  //This is so we take into account the fact that tools
  //are prefixed with the parent algorithm's name.

  auto final_string = [& algorithm_name_prefix](const std::string & unpref_str) -> std::string
  {
    return algorithm_name_prefix + "." + unpref_str;
  };

  for (const auto & ttp : m_toolsToPlot)
    {
      const std::string str = final_string(ttp.tool);
      if (m_toolsToCheckFor.count(str) == 0)
        {
          const size_t old_size = m_toolsToCheckFor.size();
          m_toolsToCheckFor[str] = old_size;
        }
    }

  for (const auto & ttp : m_pairsToPlot)
    {
      const std::string str_1 = final_string(ttp.tool_ref);
      const std::string str_2 = final_string(ttp.tool_test);
      if (m_toolsToCheckFor.count(str_1) == 0)
        {
          const size_t old_size = m_toolsToCheckFor.size();
          m_toolsToCheckFor[str_1] = old_size;
        }
      if (m_toolsToCheckFor.count(str_2) == 0)
        {
          const size_t old_size = m_toolsToCheckFor.size();
          m_toolsToCheckFor[str_2] = old_size;
        }
    }

  m_pimplHelper = std::make_unique<CaloPlotterHelper>(m_toolsToCheckFor.size());

  for (const auto & ttp : m_toolsToPlot)
    {
      ATH_MSG_INFO("Setting up plotting for tool '" << ttp.tool << "'.");
      const int tool_num = m_toolsToCheckFor[final_string(ttp.tool)];
      m_pimplHelper->set_tool_string(tool_num, ttp.tool);
      const std::string prefix = ttp.tool_prefix + (ttp.tool_prefix.size() > 0 ? "_" : "");
      const std::string suffix = std::string(ttp.tool_suffix.size() > 0 ? "_" : "") + ttp.tool_suffix;
      for (const auto & p : ttp.plot_definitions)
        {
          m_pimplHelper->add_to_plots(tool_num,
                                      p.name,
                                      p.x.min,
                                      p.x.max,
                                      p.x.bins,
                                      p.y.min,
                                      p.y.max,
                                      p.y.bins,
                                      prefix + p.prefix,
                                      p.suffix + suffix);
        }
    }

  for (const auto & ttp : m_pairsToPlot)
    {
      ATH_MSG_INFO("Setting up plotting for combination of '" << ttp.tool_ref << "' and '" << ttp.tool_test << "'.");
      const std::pair<int, int> tool_pair{m_toolsToCheckFor[final_string(ttp.tool_ref)], m_toolsToCheckFor[final_string(ttp.tool_test)]};
      m_pimplHelper->set_tool_string(tool_pair.first, ttp.tool_ref);
      m_pimplHelper->set_tool_string(tool_pair.second, ttp.tool_test);
      const std::string prefix = ttp.pair_prefix + (ttp.pair_prefix.size() > 0 ? "_" : "");
      const std::string suffix = std::string(ttp.pair_suffix.size() > 0 ? "_" : "") + ttp.pair_suffix;
      m_pimplHelper->add_tool_combination(tool_pair.first, tool_pair.second, ttp.match_in_energy);
      for (const auto & p : ttp.plot_definitions)
        {
          m_pimplHelper->add_to_plots(tool_pair,
                                      p.name,
                                      p.x.min,
                                      p.x.max,
                                      p.x.bins,
                                      p.y.min,
                                      p.y.max,
                                      p.y.bins,
                                      prefix + p.prefix,
                                      p.suffix + suffix);
        }
    }

  for (const auto & p : m_individualPlots)
    {
      m_pimplHelper->add_to_single_plots(p.name, p.x.min, p.x.max, p.x.bins, p.y.min, p.y.max, p.y.bins, p.prefix, p.suffix);
    }

  for (const auto & p : m_comparedPlots)
    {
      m_pimplHelper->add_to_combined_plots(p.name, p.x.min, p.x.max, p.x.bins, p.y.min, p.y.max, p.y.bins, p.prefix, p.suffix);
    }

  auto option_to_style = [](const PlotStyleDefinition & st) -> CaloPlotterHelper::PlotStyle
  {
    return CaloPlotterHelper::PlotStyle{ {st.line.color, st.line.style, st.line.alpha, st.line.size},
      {st.fill.color, st.fill.style, st.fill.alpha, st.fill.size},
      {st.marker.color, st.marker.style, st.marker.alpha, st.marker.size} };
  };

  auto option_to_style_vec = [& option_to_style](const std::vector<PlotStyleDefinition> & v) -> std::vector<CaloPlotterHelper::PlotStyle>
  {
    std::vector<CaloPlotterHelper::PlotStyle> ret;
    ret.reserve(v.size());
    for (const auto & st : v)
      {
        ret.push_back(option_to_style(st));
      }
    return ret;
  };

  for (const auto & jsp : m_joinedIndividualPlots)
    {
      std::vector<int> tool_nums;
      bool found_invalid = false;
      for (const auto & unpref_str : jsp.tools_to_join)
        {
          const std::string str = final_string(unpref_str);
          if (m_toolsToCheckFor.count(str) == 0)
            {
              ATH_MSG_WARNING("Invalid tool '" << unpref_str << "' provided to plotter (joined individual plots)!");
              found_invalid = true;
            }
          tool_nums.push_back(m_toolsToCheckFor[str]);
        }

      if (found_invalid)
        {
          continue;
        }

      m_pimplHelper->add_together_tools(tool_nums,
                                        jsp.plot_name,
                                        jsp.x.min,
                                        jsp.x.max,
                                        jsp.x.bins,
                                        jsp.y.min,
                                        jsp.y.max,
                                        jsp.y.bins,
                                        jsp.prefix,
                                        jsp.suffix,
                                        option_to_style_vec(jsp.styles));
    }

  for (const auto & jcp : m_joinedComparedPlots)
    {
      std::vector<std::pair<int, int>> tool_combs;
      bool found_invalid = false;
      for (const auto & unpref_str_pair : jcp.combinations_to_join)
        {
          const std::string str_1 = final_string(std::get<0>(unpref_str_pair));
          const std::string str_2 = final_string(std::get<1>(unpref_str_pair));
          if (m_toolsToCheckFor.count(str_1) == 0)
            {
              ATH_MSG_WARNING("Invalid tool '" << std::get<0>(unpref_str_pair) << "' provided to plotter (joined comparison plots)!");
              found_invalid = true;
            }
          if (m_toolsToCheckFor.count(str_2) == 0)
            {
              ATH_MSG_WARNING("Invalid tool '" << std::get<1>(unpref_str_pair) << "' provided to plotter (joined comparison plots)!");
              found_invalid = true;
            }
          tool_combs.emplace_back(m_toolsToCheckFor[str_1], m_toolsToCheckFor[str_2]);
        }

      if (found_invalid)
        {
          continue;
        }

      m_pimplHelper->add_together_combinations(tool_combs,
                                               jcp.plot_name,
                                               jcp.x.min,
                                               jcp.x.max,
                                               jcp.x.bins,
                                               jcp.y.min,
                                               jcp.y.max,
                                               jcp.y.bins,
                                               jcp.prefix,
                                               jcp.suffix,
                                               option_to_style_vec(jcp.styles));
    }

  for (const auto & jpt : m_joinedPlotsForTool)
    {
      if (m_toolsToCheckFor.count(final_string(jpt.tool)) == 0)
        {
          ATH_MSG_WARNING("Invalid tool '" << jpt.tool << "' provided to plotter (joined plots for single tool)!");
          continue;
        }

      m_pimplHelper->add_together_tool_plots(m_toolsToCheckFor[final_string(jpt.tool)],
                                             jpt.plot_names,
                                             jpt.plot_title,
                                             jpt.file_name,
                                             jpt.x.min,
                                             jpt.x.max,
                                             jpt.x.bins,
                                             jpt.y.min,
                                             jpt.y.max,
                                             jpt.y.bins,
                                             option_to_style_vec(jpt.styles));
    }

  for (const auto & jpp : m_joinedPlotsForPair)
    {
      std::pair<int, int> tool_combs;
      const std::string str_1 = final_string(jpp.tool_ref);
      const std::string str_2 = final_string(jpp.tool_test);

      bool found_invalid = false;

      if (m_toolsToCheckFor.count(str_1) == 0)
        {
          ATH_MSG_WARNING("Invalid tool '" << jpp.tool_ref << "' provided to plotter (joined comparison plots)!");
          found_invalid = true;
        }
      if (m_toolsToCheckFor.count(str_2) == 0)
        {
          ATH_MSG_WARNING("Invalid tool '" << jpp.tool_test << "' provided to plotter (joined comparison plots)!");
          found_invalid = true;
        }
      if (found_invalid)
        {
          continue;
        }

      m_pimplHelper->add_together_combination_plots( std::make_pair(m_toolsToCheckFor[str_1], m_toolsToCheckFor[str_2]),
                                                     jpp.plot_names,
                                                     jpp.plot_title,
                                                     jpp.file_name,
                                                     jpp.x.min,
                                                     jpp.x.max,
                                                     jpp.x.bins,
                                                     jpp.y.min,
                                                     jpp.y.max,
                                                     jpp.y.bins,
                                                     option_to_style_vec(jpp.styles)   );
    }

  m_pimplHelper->initialize(option_to_style(m_individualStyle), option_to_style(m_comparedStyle));

  const MatchingOptions matching_options = m_matchingOptions;

  m_pimplHelper->set_matching_options( m_cellThreshold, m_neighborThreshold, m_seedThreshold,
                                       matching_options.min_similarity, matching_options.term_w,
                                       matching_options.grow_w, matching_options.seed_w           );

  for (size_t i = 0; i < m_saveExtensions.size(); ++i)
    {
      if (i < m_saveOptions.size())
        {
          m_pimplHelper->add_extension(m_saveExtensions[i], m_saveOptions[i]);
        }
      else
        {
          m_pimplHelper->add_extension(m_saveExtensions[i]);
        }
    }

  m_pimplHelper->add_extension(".png");

  return StatusCode::SUCCESS;
}

StatusCode CaloGPUDefaultPlotter::do_plots() const
{
  {
    std::lock_guard<std::mutex> lock_guard(m_mutex);
    auto & plotter ATLAS_THREAD_SAFE = *m_pimplHelper;
    plotter.save_plots(m_savePath, m_filePrefix, m_fileSuffix);
  }
  return StatusCode::SUCCESS;
}

StatusCode CaloGPUDefaultPlotter::update_plots_start(const EventContext & /*ctx*/,
                                                     const ConstantDataHolder & /*constant_data*/,
                                                     const xAOD::CaloClusterContainer * /*cluster_collection_ptr*/) const
{
  /*
  Helpers::CPU_object<CellInfoArr> cell_info;
  Helpers::CPU_object<CellStateArr> ignore_1;
  Helpers::CPU_object<ClusterInfoArr> ignore_2;

  ATH_CHECK( convert_to_GPU_data_structures(ctx, constant_data, nullptr,
                                            cell_info, ignore_1, ignore_2 ) );
  */
  {
    std::lock_guard<std::mutex> lock_guard(m_mutex);
    auto & plotter ATLAS_THREAD_SAFE = *m_pimplHelper;
    plotter.setup_new_event();
  }
  return StatusCode::SUCCESS;
}

StatusCode CaloGPUDefaultPlotter::update_plots_end(const EventContext & /*ctx*/,
                                                   const ConstantDataHolder & /*constant_data*/,
                                                   const xAOD::CaloClusterContainer * /*cluster_collection_ptr*/) const
{
  /*
  Helpers::CPU_object<CellInfoArr> cell_info;
  Helpers::CPU_object<CellStateArr> ignore_1;
  Helpers::CPU_object<ClusterInfoArr> ignore_2;

  ATH_CHECK( convert_to_GPU_data_structures(ctx, constant_data, nullptr,
                                            cell_info, ignore_1, ignore_2 ) );
  */
  std::string str;
  {
    std::lock_guard<std::mutex> lock_guard(m_mutex);
    auto & plotter ATLAS_THREAD_SAFE = *m_pimplHelper;
    plotter.finish_event();
    str = plotter.get_per_event_message();
  }
  ATH_MSG_INFO("");
  std::string buffer;
  for (size_t i = 0; i < str.size(); ++i)
    {
      if (str[i] == '\n')
        {
          if (buffer.size() > 0)
            {
              ATH_MSG_INFO(buffer);
              buffer.clear();
            }
        }
      else
        {
          buffer.push_back(str[i]);
        }
    }
  if (buffer.size() > 0)
    {
      ATH_MSG_INFO(buffer);
    }
  ATH_MSG_INFO("");
  return StatusCode::SUCCESS;
}

StatusCode CaloGPUDefaultPlotter::update_plots(const EventContext & ctx,
                                               const ConstantDataHolder & constant_data,
                                               const xAOD::CaloClusterContainer * cluster_collection_ptr,
                                               const CaloClusterCollectionProcessor * tool) const
{
  if (filter_tool_by_name(tool->name()))
    {
      Helpers::CPU_object<CellInfoArr> cell_info;
      Helpers::CPU_object<CellStateArr> cell_state;
      Helpers::CPU_object<ClusterInfoArr> clusters;
      Helpers::CPU_object<ClusterMomentsArr> moments;

      ATH_CHECK( convert_to_GPU_data_structures(ctx, constant_data, cluster_collection_ptr,
                                                cell_info, cell_state, clusters, moments        )  );

      return add_data_to_plotter(ctx, constant_data, cell_info, cell_state, clusters, moments, tool->name());
    }
  else
    {
      return StatusCode::SUCCESS;
    }
}

StatusCode CaloGPUDefaultPlotter::update_plots(const EventContext & ctx,
                                               const ConstantDataHolder & constant_data,
                                               const xAOD::CaloClusterContainer * /*cluster_collection_ptr*/,
                                               const EventDataHolder & event_data,
                                               const ICaloClusterGPUInputTransformer * tool) const
{
  if (filter_tool_by_name(tool->name()))
    {
      return add_data_to_plotter(ctx, constant_data, event_data.m_cell_info_dev, event_data.m_cell_state_dev,
                                 event_data.m_clusters_dev, event_data.m_moments_dev, tool->name());
    }
  else
    {
      return StatusCode::SUCCESS;
    }
}

StatusCode CaloGPUDefaultPlotter::update_plots(const EventContext & ctx,
                                               const ConstantDataHolder & constant_data,
                                               const xAOD::CaloClusterContainer * /*cluster_collection_ptr*/,
                                               const EventDataHolder & event_data,
                                               const CaloClusterGPUProcessor * tool) const
{
  if (filter_tool_by_name(tool->name()))
    {
      Helpers::CPU_object<CellInfoArr> cell_info = event_data.m_cell_info_dev;
      Helpers::CPU_object<CellStateArr> cell_state =  event_data.m_cell_state_dev;
      Helpers::CPU_object<ClusterInfoArr> clusters = event_data.m_clusters_dev;
      Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> moments = event_data.m_moments_dev;

      ATH_CHECK( compactify_clusters(ctx, constant_data, cell_info, cell_state, clusters, moments) );

      return add_data_to_plotter(ctx, constant_data, cell_info, cell_state, clusters, moments, tool->name());
    }
  else
    {
      return StatusCode::SUCCESS;
    }
}

StatusCode CaloGPUDefaultPlotter::update_plots(const EventContext & ctx,
                                               const ConstantDataHolder & constant_data,
                                               const xAOD::CaloClusterContainer * cluster_collection_ptr,
                                               const EventDataHolder & /*event_data*/,
                                               const ICaloClusterGPUOutputTransformer * tool) const
{
  if (filter_tool_by_name(tool->name()))
    {
      Helpers::CPU_object<CellInfoArr> cell_info;
      Helpers::CPU_object<CellStateArr> cell_state;
      Helpers::CPU_object<ClusterInfoArr> clusters;
      Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> moments;

      ATH_CHECK( convert_to_GPU_data_structures(ctx, constant_data, cluster_collection_ptr,
                                                cell_info, cell_state, clusters, moments          )  );

      return add_data_to_plotter(ctx, constant_data, cell_info, cell_state, clusters, moments, tool->name());
    }
  else
    {
      return StatusCode::SUCCESS;
    }
}

StatusCode CaloGPUDefaultPlotter::add_data_to_plotter(const EventContext & /*ctx*/,
                                                      const ConstantDataHolder & constant_data,
                                                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & cell_info,
                                                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & cell_state,
                                                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & clusters,
                                                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & moments,
                                                      const std::string & tool_name) const
{
  auto it = m_toolsToCheckFor.find(tool_name);
  if (it != m_toolsToCheckFor.end())
    {
      std::lock_guard<std::mutex> lock_guard(m_mutex);
      auto & plotter ATLAS_THREAD_SAFE = *m_pimplHelper;
      plotter.add_event_data(it->second, constant_data, cell_info, cell_state, clusters, moments);
    }
  return StatusCode::SUCCESS;
}

bool CaloGPUDefaultPlotter::filter_tool_by_name(const std::string & tool_name) const
{
  ATH_MSG_DEBUG("Checking : '" << tool_name << "': " << m_toolsToCheckFor.count(tool_name));
  return m_toolsToCheckFor.count(tool_name) > 0;
}

//Note: The following is essentially copied over from two tools.
//      Maybe we could prevent the repetition?

StatusCode CaloGPUDefaultPlotter::convert_to_GPU_data_structures(const EventContext & ctx,
                                                                 const ConstantDataHolder & /*constant_data*/,
                                                                 const xAOD::CaloClusterContainer * cluster_collection,
                                                                 CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & ret_info,
                                                                 CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & ret_state,
                                                                 CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & ret_clusts,
                                                                 CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & ret_moments) const
{
  SG::ReadHandle<CaloCellContainer> cell_collection(m_cellsKey, ctx);
  if ( !cell_collection.isValid() )
    {
      ATH_MSG_ERROR( " Cannot retrieve CaloCellContainer: " << cell_collection.name()  );
      return StatusCode::RECOVERABLE;
    }

  ret_info.allocate();

  if (cluster_collection != nullptr)
    {
      ret_state.allocate();
      ret_clusts.allocate();
      ret_moments.allocate();
    }

  for (int i = 0; i < NCaloCells; ++i)
    {
      ret_info->energy[i] = 0;
      ret_info->gain[i] = GainConversion::invalid_gain();
      ret_info->time[i] = 0;
      ret_info->qualityProvenance[i] = 0;

      if (cluster_collection != nullptr)
        {
          ret_state->clusterTag[i] = ClusterTag::make_invalid_tag();
        }
    }

  for (CaloCellContainer::const_iterator iCells = cell_collection->begin(); iCells != cell_collection->end(); ++iCells)
    {
      const CaloCell * cell = (*iCells);

      const int index = m_calo_id->calo_cell_hash(cell->ID());

      const float energy = cell->energy();

      const unsigned int gain = GainConversion::from_standard_gain(cell->gain());

      ret_info->energy[index] = energy;
      ret_info->gain[index] = gain;
      ret_info->time[index] = cell->time();
      ret_info->qualityProvenance[index] = QualityProvenance{cell->quality(), cell->provenance()};

    }

  if (cluster_collection != nullptr)
    {
      const auto cluster_end = cluster_collection->end();
      auto cluster_iter = cluster_collection->begin();

      for (int cluster_number = 0; cluster_iter != cluster_end; ++cluster_iter, ++cluster_number )
        {
          const xAOD::CaloCluster * cluster = (*cluster_iter);
          const CaloClusterCellLink * cell_links = cluster->getCellLinks();
          if (!cell_links)
            {
              ATH_MSG_ERROR("Can't get valid links to CaloCells (CaloClusterCellLink)!");
              return StatusCode::FAILURE;
            }

          ret_clusts->clusterEnergy[cluster_number] = cluster->e();
          ret_clusts->clusterEt[cluster_number] = cluster->et();
          ret_clusts->clusterEta[cluster_number] = cluster->eta();
          ret_clusts->clusterPhi[cluster_number] = cluster->phi();
          ret_clusts->seedCellID[cluster_number] = m_calo_id->calo_cell_hash(cluster->cell_begin()->ID());
          for (int i = 0; i < NumSamplings; ++i)
            {
              ret_moments->energyPerSample[i][cluster_number] = cluster->eSample((CaloSampling::CaloSample) i);
              ret_moments->maxEPerSample[i][cluster_number] = cluster->energy_max((CaloSampling::CaloSample) i);
              ret_moments->maxPhiPerSample[i][cluster_number] = cluster->phimax((CaloSampling::CaloSample) i);
              ret_moments->maxEtaPerSample[i][cluster_number] = cluster->etamax((CaloSampling::CaloSample) i);
              ret_moments->etaPerSample[i][cluster_number] = cluster->etaSample((CaloSampling::CaloSample) i);
              ret_moments->phiPerSample[i][cluster_number] = cluster->phiSample((CaloSampling::CaloSample) i);
              ret_moments->nCellSampling[i][cluster_number] = cluster->numberCellsInSampling((CaloSampling::CaloSample) i);
            }
          ret_moments->time[cluster_number] = cluster->time();
          ret_moments->firstPhi[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::FIRST_PHI);
          ret_moments->firstEta[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::FIRST_ETA);
          ret_moments->secondR[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::SECOND_R);
          ret_moments->secondLambda[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::SECOND_LAMBDA);
          ret_moments->deltaPhi[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::DELTA_PHI);
          ret_moments->deltaTheta[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::DELTA_THETA);
          ret_moments->deltaAlpha[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::DELTA_ALPHA);
          ret_moments->centerX[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::CENTER_X);
          ret_moments->centerY[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::CENTER_Y);
          ret_moments->centerZ[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::CENTER_Z);
          ret_moments->centerMag[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::CENTER_MAG);
          ret_moments->centerLambda[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::CENTER_LAMBDA);
          ret_moments->lateral[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::LATERAL);
          ret_moments->longitudinal[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::LONGITUDINAL);
          ret_moments->engFracEM[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_FRAC_EM);
          ret_moments->engFracMax[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_FRAC_MAX);
          ret_moments->engFracCore[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_FRAC_CORE);
          ret_moments->firstEngDens[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::FIRST_ENG_DENS);
          ret_moments->secondEngDens[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::SECOND_ENG_DENS);
          ret_moments->isolation[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ISOLATION);
          ret_moments->engBadCells[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_BAD_CELLS);
          ret_moments->nBadCells[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::N_BAD_CELLS);
          ret_moments->nBadCellsCorr[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::N_BAD_CELLS_CORR);
          ret_moments->badCellsCorrE[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::BAD_CELLS_CORR_E);
          ret_moments->badLArQFrac[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::BADLARQ_FRAC);
          ret_moments->engPos[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_POS);
          ret_moments->significance[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::SIGNIFICANCE);
          ret_moments->cellSignificance[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::CELL_SIGNIFICANCE);
          ret_moments->cellSigSampling[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::CELL_SIG_SAMPLING);
          ret_moments->avgLArQ[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::AVG_LAR_Q);
          ret_moments->avgTileQ[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::AVG_TILE_Q);
          ret_moments->engBadHVCells[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_BAD_HV_CELLS);
          ret_moments->nBadHVCells[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::N_BAD_HV_CELLS);
          ret_moments->PTD[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::PTD);
          ret_moments->mass[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::MASS);
          ret_moments->EMProbability[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::EM_PROBABILITY);
          ret_moments->hadWeight[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::HAD_WEIGHT);
          ret_moments->OOCweight[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::OOC_WEIGHT);
          ret_moments->DMweight[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::DM_WEIGHT);
          ret_moments->tileConfidenceLevel[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::TILE_CONFIDENCE_LEVEL);
          ret_moments->secondTime[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::SECOND_TIME);
          ret_moments->nExtraCellSampling[cluster_number] = cluster->numberCellsInSampling(CaloSampling::EME2, true);
          ret_moments->numCells[cluster_number] = cluster->numberCells();
          ret_moments->vertexFraction[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::VERTEX_FRACTION);
          ret_moments->nVertexFraction[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::NVERTEX_FRACTION);
          ret_moments->etaCaloFrame[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ETACALOFRAME);
          ret_moments->phiCaloFrame[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::PHICALOFRAME);
          ret_moments->eta1CaloFrame[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ETA1CALOFRAME);
          ret_moments->phi1CaloFrame[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::PHI1CALOFRAME);
          ret_moments->eta2CaloFrame[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ETA2CALOFRAME);
          ret_moments->phi2CaloFrame[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::PHI2CALOFRAME);
          ret_moments->engCalibTot[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_TOT);
          ret_moments->engCalibOutL[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_OUT_L);
          ret_moments->engCalibOutM[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_OUT_M);
          ret_moments->engCalibOutT[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_OUT_T);
          ret_moments->engCalibDeadL[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_L);
          ret_moments->engCalibDeadM[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_M);
          ret_moments->engCalibDeadT[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_T);
          ret_moments->engCalibEMB0[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_EMB0);
          ret_moments->engCalibEME0[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_EME0);
          ret_moments->engCalibTileG3[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_TILEG3);
          ret_moments->engCalibDeadTot[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_TOT);
          ret_moments->engCalibDeadEMB0[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_EMB0);
          ret_moments->engCalibDeadTile0[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_TILE0);
          ret_moments->engCalibDeadTileG3[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_TILEG3);
          ret_moments->engCalibDeadEME0[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_EME0);
          ret_moments->engCalibDeadHEC0[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_HEC0);
          ret_moments->engCalibDeadFCAL[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_FCAL);
          ret_moments->engCalibDeadLeakage[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_LEAKAGE);
          ret_moments->engCalibDeadUnclass[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_DEAD_UNCLASS);
          ret_moments->engCalibFracEM[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_FRAC_EM);
          ret_moments->engCalibFracHad[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_FRAC_HAD);
          ret_moments->engCalibFracRest[cluster_number] = cluster->getMomentValue(xAOD::CaloCluster::ENG_CALIB_FRAC_REST);
          for (auto it = cell_links->begin(); it != cell_links->end(); ++it)
            {

              const int cell_ID = m_calo_id->calo_cell_hash(it->ID());
              const float weight = it.weight();

              uint32_t weight_as_int = 0;
              std::memcpy(&weight_as_int, &weight, sizeof(float));
              //On the platforms we expect to be running this, it should be fine.
              //Still UB.
              //With C++20, we could do that bit-cast thing.

              if (weight_as_int == 0)
                {
                  weight_as_int = 1;
                  //Subnormal,
                  //but just to distinguish from
                  //a non-shared cluster.
                }

              const ClusterTag other_tag = ret_state->clusterTag[cell_ID];

              const int other_index = other_tag.is_part_of_cluster() ? other_tag.cluster_index() : -1;

              if (other_index < 0)
                {
                  if (weight < 0.5f)
                    {
                      ret_state->clusterTag[cell_ID] = ClusterTag::make_tag(cluster_number, weight_as_int, 0);
                    }
                  else
                    {
                      ret_state->clusterTag[cell_ID] = ClusterTag::make_tag(cluster_number);
                    }
                }
              else if (weight > 0.5f)
                {
                  ret_state->clusterTag[cell_ID] = ClusterTag::make_tag(cluster_number, other_tag.secondary_cluster_weight(), other_index < 0 ? 0 : other_index);
                }
              else if (weight == 0.5f)
                //Unlikely, but...
                {
                  const int max_cluster = cluster_number > other_index ? cluster_number : other_index;
                  const int min_cluster = cluster_number > other_index ? other_index : cluster_number;
                  ret_state->clusterTag[cell_ID] = ClusterTag::make_tag(max_cluster, weight_as_int, min_cluster);
                }
              else /*if (weight < 0.5f)*/
                {
                  ret_state->clusterTag[cell_ID] = ClusterTag::make_tag(other_index, weight_as_int, cluster_number);
                }
            }
        }

      ret_clusts->number = cluster_collection->size();
    }

  return StatusCode::SUCCESS;
}

StatusCode CaloGPUDefaultPlotter::compactify_clusters(const EventContext &,
                                                      const CaloRecGPU::ConstantDataHolder &,
                                                      const CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellInfoArr> & cell_info,
                                                      CaloRecGPU::Helpers::CPU_object<CaloRecGPU::CellStateArr> & cell_state,
                                                      CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterInfoArr> & clusters,
                                                      CaloRecGPU::Helpers::CPU_object<CaloRecGPU::ClusterMomentsArr> & moments) const
{
  std::map<int, int> tag_map;

  std::vector<int> cluster_order(clusters->number);

  std::iota(cluster_order.begin(), cluster_order.end(), 0);

  std::sort(cluster_order.begin(), cluster_order.end(), [&](const int a, const int b)
  {
    if (clusters->seedCellID[a] < 0)
      {
        return false;
        //This means that clusters with no cells
        //(marked as invalid) always compare lower,
        //so they appear in the end.
      }
    else if (clusters->seedCellID[b] < 0)
      {
        return true;
      }
    return clusters->clusterEt[a] > clusters->clusterEt[b];
  } );

  int real_cluster_numbers = clusters->number;

  for (size_t i = 0; i < cluster_order.size(); ++i)
    {
      const int this_id = cluster_order[i];
      if (clusters->seedCellID[this_id] < 0)
        {
          tag_map[this_id] = -1;
          --real_cluster_numbers;
        }
      else
        {
          tag_map[this_id] = i;
        }
    }

  const Helpers::CPU_object<ClusterInfoArr> temp_clusters(clusters);
  const Helpers::CPU_object<ClusterMomentsArr> temp_moments(moments);

  clusters->number = real_cluster_numbers;

  for (int i = 0; i < temp_clusters->number; ++i)
    {
      clusters->clusterEnergy[i]      =   temp_clusters->clusterEnergy[cluster_order[i]];
      clusters->clusterEt[i]          =   temp_clusters->clusterEt[cluster_order[i]];
      clusters->clusterEta[i]         =   temp_clusters->clusterEta[cluster_order[i]];
      clusters->clusterPhi[i]         =   temp_clusters->clusterPhi[cluster_order[i]];
      clusters->seedCellID[i]         =   temp_clusters->seedCellID[cluster_order[i]];
      for (int j = 0; j < NumSamplings; ++j)
        {
          moments->energyPerSample[j][i]     =   temp_moments->energyPerSample[j][cluster_order[i]];
          moments->maxEPerSample[j][i]       =   temp_moments->maxEPerSample[j][cluster_order[i]];
          moments->maxPhiPerSample[j][i]     =   temp_moments->maxPhiPerSample[j][cluster_order[i]];
          moments->maxEtaPerSample[j][i]     =   temp_moments->maxEtaPerSample[j][cluster_order[i]];
          moments->etaPerSample[j][i]        =   temp_moments->etaPerSample[j][cluster_order[i]];
          moments->phiPerSample[j][i]        =   temp_moments->phiPerSample[j][cluster_order[i]];
        }
      moments->time[i]                =   temp_moments->time[cluster_order[i]];
      moments->firstPhi[i]            =   temp_moments->firstPhi[cluster_order[i]];
      moments->firstEta[i]            =   temp_moments->firstEta[cluster_order[i]];
      moments->secondR[i]             =   temp_moments->secondR[cluster_order[i]];
      moments->secondLambda[i]        =   temp_moments->secondLambda[cluster_order[i]];
      moments->deltaPhi[i]            =   temp_moments->deltaPhi[cluster_order[i]];
      moments->deltaTheta[i]          =   temp_moments->deltaTheta[cluster_order[i]];
      moments->deltaAlpha[i]          =   temp_moments->deltaAlpha[cluster_order[i]];
      moments->centerX[i]             =   temp_moments->centerX[cluster_order[i]];
      moments->centerY[i]             =   temp_moments->centerY[cluster_order[i]];
      moments->centerZ[i]             =   temp_moments->centerZ[cluster_order[i]];
      moments->centerMag[i]           =   temp_moments->centerMag[cluster_order[i]];
      moments->centerLambda[i]        =   temp_moments->centerLambda[cluster_order[i]];
      moments->lateral[i]             =   temp_moments->lateral[cluster_order[i]];
      moments->longitudinal[i]        =   temp_moments->longitudinal[cluster_order[i]];
      moments->engFracEM[i]           =   temp_moments->engFracEM[cluster_order[i]];
      moments->engFracMax[i]          =   temp_moments->engFracMax[cluster_order[i]];
      moments->engFracCore[i]         =   temp_moments->engFracCore[cluster_order[i]];
      moments->firstEngDens[i]        =   temp_moments->firstEngDens[cluster_order[i]];
      moments->secondEngDens[i]       =   temp_moments->secondEngDens[cluster_order[i]];
      moments->isolation[i]           =   temp_moments->isolation[cluster_order[i]];
      moments->engBadCells[i]         =   temp_moments->engBadCells[cluster_order[i]];
      moments->nBadCells[i]           =   temp_moments->nBadCells[cluster_order[i]];
      moments->nBadCellsCorr[i]       =   temp_moments->nBadCellsCorr[cluster_order[i]];
      moments->badCellsCorrE[i]       =   temp_moments->badCellsCorrE[cluster_order[i]];
      moments->badLArQFrac[i]         =   temp_moments->badLArQFrac[cluster_order[i]];
      moments->engPos[i]              =   temp_moments->engPos[cluster_order[i]];
      moments->significance[i]        =   temp_moments->significance[cluster_order[i]];
      moments->cellSignificance[i]    =   temp_moments->cellSignificance[cluster_order[i]];
      moments->cellSigSampling[i]     =   temp_moments->cellSigSampling[cluster_order[i]];
      moments->avgLArQ[i]             =   temp_moments->avgLArQ[cluster_order[i]];
      moments->avgTileQ[i]            =   temp_moments->avgTileQ[cluster_order[i]];
      moments->engBadHVCells[i]       =   temp_moments->engBadHVCells[cluster_order[i]];
      moments->nBadHVCells[i]         =   temp_moments->nBadHVCells[cluster_order[i]];
      moments->PTD[i]                 =   temp_moments->PTD[cluster_order[i]];
      moments->mass[i]                =   temp_moments->mass[cluster_order[i]];
      moments->EMProbability[i]       =   temp_moments->EMProbability[cluster_order[i]];
      moments->hadWeight[i]           =   temp_moments->hadWeight[cluster_order[i]];
      moments->OOCweight[i]           =   temp_moments->OOCweight[cluster_order[i]];
      moments->DMweight[i]            =   temp_moments->DMweight[cluster_order[i]];
      moments->tileConfidenceLevel[i] =   temp_moments->tileConfidenceLevel[cluster_order[i]];
      moments->secondTime[i]          =   temp_moments->secondTime[cluster_order[i]];
      for (int j = 0; j < NumSamplings; ++j)
        {
          moments->nCellSampling[j][i]       =   temp_moments->nCellSampling[j][cluster_order[i]];
        }
      moments->nExtraCellSampling[i]  =   temp_moments->nExtraCellSampling[cluster_order[i]];
      moments->numCells[i]            =   temp_moments->numCells[cluster_order[i]];
      moments->vertexFraction[i]      =   temp_moments->vertexFraction[cluster_order[i]];
      moments->nVertexFraction[i]     =   temp_moments->nVertexFraction[cluster_order[i]];
      moments->etaCaloFrame[i]        =   temp_moments->etaCaloFrame[cluster_order[i]];
      moments->phiCaloFrame[i]        =   temp_moments->phiCaloFrame[cluster_order[i]];
      moments->eta1CaloFrame[i]       =   temp_moments->eta1CaloFrame[cluster_order[i]];
      moments->phi1CaloFrame[i]       =   temp_moments->phi1CaloFrame[cluster_order[i]];
      moments->eta2CaloFrame[i]       =   temp_moments->eta2CaloFrame[cluster_order[i]];
      moments->phi2CaloFrame[i]       =   temp_moments->phi2CaloFrame[cluster_order[i]];
      moments->engCalibTot[i]         =   temp_moments->engCalibTot[cluster_order[i]];
      moments->engCalibOutL[i]        =   temp_moments->engCalibOutL[cluster_order[i]];
      moments->engCalibOutM[i]        =   temp_moments->engCalibOutM[cluster_order[i]];
      moments->engCalibOutT[i]        =   temp_moments->engCalibOutT[cluster_order[i]];
      moments->engCalibDeadL[i]       =   temp_moments->engCalibDeadL[cluster_order[i]];
      moments->engCalibDeadM[i]       =   temp_moments->engCalibDeadM[cluster_order[i]];
      moments->engCalibDeadT[i]       =   temp_moments->engCalibDeadT[cluster_order[i]];
      moments->engCalibEMB0[i]        =   temp_moments->engCalibEMB0[cluster_order[i]];
      moments->engCalibEME0[i]        =   temp_moments->engCalibEME0[cluster_order[i]];
      moments->engCalibTileG3[i]      =   temp_moments->engCalibTileG3[cluster_order[i]];
      moments->engCalibDeadTot[i]     =   temp_moments->engCalibDeadTot[cluster_order[i]];
      moments->engCalibDeadEMB0[i]    =   temp_moments->engCalibDeadEMB0[cluster_order[i]];
      moments->engCalibDeadTile0[i]   =   temp_moments->engCalibDeadTile0[cluster_order[i]];
      moments->engCalibDeadTileG3[i]  =   temp_moments->engCalibDeadTileG3[cluster_order[i]];
      moments->engCalibDeadEME0[i]    =   temp_moments->engCalibDeadEME0[cluster_order[i]];
      moments->engCalibDeadHEC0[i]    =   temp_moments->engCalibDeadHEC0[cluster_order[i]];
      moments->engCalibDeadFCAL[i]    =   temp_moments->engCalibDeadFCAL[cluster_order[i]];
      moments->engCalibDeadLeakage[i] =   temp_moments->engCalibDeadLeakage[cluster_order[i]];
      moments->engCalibDeadUnclass[i] =   temp_moments->engCalibDeadUnclass[cluster_order[i]];
      moments->engCalibFracEM[i]      =   temp_moments->engCalibFracEM[cluster_order[i]];
      moments->engCalibFracHad[i]     =   temp_moments->engCalibFracHad[cluster_order[i]];
      moments->engCalibFracRest[i]    =   temp_moments->engCalibFracRest[cluster_order[i]];
    }

  for (int i = 0; i < NCaloCells; ++i)
    {
      if (!cell_info->is_valid(i))
        {
          continue;
        }
      const ClusterTag this_tag = cell_state->clusterTag[i];
      if (!this_tag.is_part_of_cluster())
        {
          cell_state->clusterTag[i] = ClusterTag::make_invalid_tag();
        }
      else if (this_tag.is_part_of_cluster())
        {
          const int old_idx = this_tag.cluster_index();
          const int new_idx = tag_map[old_idx];
          const int old_idx2 = this_tag.is_shared_between_clusters() ? this_tag.secondary_cluster_index() : -1;
          const int new_idx2 = old_idx2 >= 0 ? tag_map[old_idx2] : -1;
          if (new_idx < 0 && new_idx2 < 0)
            {
              cell_state->clusterTag[i] = ClusterTag::make_invalid_tag();
            }
          else if (new_idx < 0)
            {
              cell_state->clusterTag[i] = ClusterTag::make_tag(new_idx2);
            }
          else if (new_idx2 < 0)
            {
              cell_state->clusterTag[i] = ClusterTag::make_tag(new_idx);
            }
          else
            {
              cell_state->clusterTag[i] = ClusterTag::make_tag(new_idx, this_tag.secondary_cluster_weight(), new_idx2);
            }
        }
    }

  return StatusCode::SUCCESS;
}

CaloGPUDefaultPlotter::~CaloGPUDefaultPlotter()
{
  //Nothing!
}